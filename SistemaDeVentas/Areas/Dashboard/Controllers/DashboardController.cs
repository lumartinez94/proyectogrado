﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SistemaDeVentas.Areas.Dashboard.Models;

namespace SistemaDeVentas.Areas.Dashboard.Controllers
{

    [Area("Dashboard")]
    [Authorize]
    public class DashboardController : Controller
    {
        public IActionResult graficoCerdostotales()
        {
            List<DataPoint> dataPoints = new List<DataPoint>{
                new DataPoint(10, 22),
                new DataPoint(20, 36),
                new DataPoint(30, 42),
                new DataPoint(40, 51),
                new DataPoint(50, 46),
            };

            ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);
            return View("~/Areas/Dashboard/Views/graficoCerdostotales.cshtml");
        }
    }
}