﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Cerdos.Models
{ 
    public class Cerdo
    {
        public int ID { get; set; }

        public string codigo { get; set; }

        public DateTime fecha_registro { get; set; }
        public string Peso { get; set; }
        public string Sexo { get; set; }



        public string fecha_destete { get; set; }
        public string fecha_nacimiento { get; set; }
        public int id_alimentacion { get; set; }
        public int id_venta { get; set; }
        public int id_corral { get; set; }
        public string numero_tetas { get; set; }
        public string castrado { get; set; }
        public int id_raza { get; set; }
        public string observaciones { get; set; }
        public int id_estado{ get; set; }
        public int id_lineaProduccion { get; set; }
        public string id_user { get; set; }



    }
}
