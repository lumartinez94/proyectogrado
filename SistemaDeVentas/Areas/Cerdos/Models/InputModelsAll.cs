﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SistemaDeVentas.Areas.ControlMedicamento.Models;

namespace SistemaDeVentas.Areas.Cerdos.Models
{
    public class InputModelsAll
    {

        public InputModelsCerdos cerdos { get; set; }
        public List<InputModelsGestacion> gestacion { get; set; }
        public List<InputModelsPartos> parto { get; set; }
        public List<InputModelsServicio> servicio { get; set; }
        public string porcentajeExito { get; set; }
        public string porcentajeFallido { get; set; }
        public string totalServicio { get; set; }
        public string cerdasTotales { get; set; }
        public string firstService { get; set; }
        public string lastService { get; set; }
        public string lastParto { get; set; }
        public string firstParto { get; set; }

        public string totalParto { get; set; }
        public string porcentajeHembra { get; set; }
        public string porcentajeHombre { get; set; }
        public string mortandad { get; set; }
        public string totalLechones { get; set; }
        public string totalgestacion { get; set; }
        public string pendienteGestacion { get; set; }


        public string fechaReal { get; set; }
        public string fechaAproximada { get; set; }

        public InputModelsTraslado traslado { get; set; }
        public List<InputModelsCerdosMedicamentos> medicamentos { get; set; }
        public string ErrorMessage { get; set; }




    }
}
