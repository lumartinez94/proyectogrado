﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Cerdos.Models
{
    public class InputModelsPartos
    {


        [Display(Name = "ID")]
        public string id_parto { get; set; }

        [Required(ErrorMessage = "<font color='red'> El campo numero de cerda es obligatorio</font>")]

        [Display(Name = "Identificación cerda")]
        public string id_cerdaMadre { get; set; }

        [Display(Name = "Identificación cerda")]
        public string caravana_cerda { get; set; }

        [Display(Name = "#Nacidos Vivos")]
        public string nacidos_vivos { get; set; }

        [Display(Name = "#Nacidos Muertos")]
        public string nacidos_muertos { get; set; }

        public string ErrorMessage { get; set; }

        [Display(Name ="Observación")]
        public string observacion { get; set; }


        [Required(ErrorMessage = "<font color='red'> El campo fecha de nacimiento es obligatorio</font>")]


        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        [Display(Name = "Fecha de parto")]
        public DateTime? fecha_nacimiento { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]

        [Display(Name = "Fecha del ultimo parto")]
        public DateTime? fecha_ultimoParto { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]

        [Display(Name = "Fecha del ultimo destete")]
        public DateTime? fecha_ultimoDestete { get; set; }

        [Display(Name = "# Lechones Adoptados")]

        public string numero_adoptados { get; set; }

        [Display(Name = "#Transferidos")]
        public string numero_transferidos { get; set; }

        [Display(Name = "Preso promedio")]
        public string peso_promedio { get; set; }
        [Display(Name = "#Parto")]
        public string numero_parto { get; set; }

        [Required(ErrorMessage = "<font color='red'> El numero de hembras es obligatorio</font>")]

        [Display(Name = "#Lechones Hembras")]
        public string numero_hembra { get; set; }


        [Required(ErrorMessage = "<font color='red'> El campo numero de machos es obligatorio</font>")]
        [Display(Name = "#Lechones machos")]
        public string numero_macho { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]

        [Display(Name = "Fecha de destete")]
        public DateTime? fecha_Destete { get; set; }
    }
}
