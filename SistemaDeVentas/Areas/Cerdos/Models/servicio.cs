﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Cerdos.Models
{
    public class servicio
    {
        public int id { get; set; }

        public int id_cerdoMacho { get; set; }

        public string Observacion { get; set; }
      
        public int id_cerdoHembra { get; set; }



        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]

        public DateTime? fecha{ get; set; }

        public string estado { get; set; }


        public string intentos { get; set; }



       
        public string servicio_exitoso { get; set; }
    }
}
