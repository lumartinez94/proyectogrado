﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Cerdos.Models
{
    public class Hembra
    {
        public int ID { get; set; }
        public string NumeroTetas { get; set; }
        public string IdCerdo { get; set; }

        public DateTime fecha_registro { get; set; }
    }

}
