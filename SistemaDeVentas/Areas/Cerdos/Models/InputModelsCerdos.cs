﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using SistemaDeVentas.Areas.Corrales.Models;

namespace SistemaDeVentas.Areas.Cerdos.Models
{
    public class InputModelsCerdos
    {

        public int ID { get; set; }


        [Display(Name = "Codigo")]
        public string codigo { get; set; }
        [Display(Name ="Fecha de creación")]
        public DateTime fecha_registro { get; set; }

        [Required(ErrorMessage = "<font color='red'> El campo peso es obligatorio</font>")]
        public string Peso { get; set; }
        [Required(ErrorMessage = "<font color='red'> El campo sexo es obligatorio</font>")]
        public string Sexo { get; set; }

        [Required(ErrorMessage = "<font color='red'> El campo fecha de destete es obligatorio</font>")]
        [Display(Name = "Fecha de destete")]
        public string fecha_destete { get; set; }

        [Required(ErrorMessage = "<font color='red'> El campo fecha de nacimiento es obligatorio</font>")]
        [Display(Name = "Fecha de nacimiento")]
        public string fecha_nacimiento { get; set; }



        [Display(Name = "#Corral")]
        public string numeroCorral { get; set; }

        public int id_corral { get; set; }

        [Display(Name = "Numero de tetas")]
        public string numero_tetas { get; set; }

        public string castrado { get; set; }


        public int id_raza { get; set; }

        public string observaciones { get; set; }

        [Required(ErrorMessage = "<font color='red'> El campo estado es obligatorio</font>")]
        [Display(Name = "Estado")]
        public string status { get; set; }


        public string id_alimentacion { get; set; }

        [Display(Name = "Categoria del alimento")]
        [Required(ErrorMessage = "<font color='red'> El campo tipo de alimentacion es obligatorio</font>")]
        public string Nombre_alimentacion { get; set; }



        public string linea_produccion { get; set; }

        [Display(Name = "Linea de producción")]
        [Required(ErrorMessage = "<font color='red'> El campo linea de producción es obligatorio</font>")]
        public string nombre_produccion { get; set; }

        public string Nombre { get; set; }

        [Display(Name = "Corral de destino")]
        [Required(ErrorMessage = "<font color='red'> El campo corral es obligatorio</font>")]
        public string NombreCorral { get; set; }

        [Display(Name = "Genetica")]

        [Required(ErrorMessage = "<font color='red'> El campo raza es obligatorio</font>")]
        public string NombreRaza { get; set; }

        [Display(Name = "Descripción")]




        public string Descripcion { get; set; }
        public string email { get; set; }



        public InputModelsCerdos()
        {
            this.castrado = "";
            this.numero_tetas = "";

        }
    }

}
