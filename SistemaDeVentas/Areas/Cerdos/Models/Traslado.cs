﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Cerdos.Models
{
    public class Traslado
    {
        public int id { get; set; }
        public int id_corral { get; set; }
        public string causas { get; set; }
        public int id_cerdo { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? fecha { get; set; }

       
    }
}
