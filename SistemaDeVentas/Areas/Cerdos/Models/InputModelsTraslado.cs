﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Cerdos.Models
{
    public class InputModelsTraslado
    {

        [Display(Name = "Identificación del cerdo")]
        public string codigo_cerdo { get; set; }

        public string id_traslado { get; set; }

        [Display(Name = "Corral")]
        public string id_corral { get; set; }

        [Display(Name = "Motivo del traslado")]
        public string causas { get; set; }

        public string fecha { get; set; }
        public List<SelectListItem> corrales {get;set;}

        public string ErrorMessage { get; set; }
    }
}
