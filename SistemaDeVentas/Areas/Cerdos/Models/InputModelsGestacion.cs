﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Cerdos.Models
{
    public class InputModelsGestacion
    {

        [Display(Name = "ID")]
        public string id_gestacion { get; set; }

        [Display(Name = "Identificación cerda")]
        public string caravana_hembra { get; set; }

        [Display(Name = "Identifición Cerda")]
        public string id_cerdo { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]



        [Display(Name = "Fecha Estimada")]


        public DateTime? fecha_aproximada { get; set; }

        [Required(ErrorMessage = "<font color='red'> El campo fecha de creacion es obligatorio</font>")]

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        [Display(Name = "Fecha de gestación")]
        public DateTime? fecha_inicio { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        [Display(Name = "Fecha parto")]
        public DateTime? fecha_real { get; set; }

        [Display(Name = "Estado")]
        public string estado { get; set; }

        public string ErrorMessage { get; set; }
    }
}
