﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Cerdos.Models
{
    public class Parto
    {

        public int id { get; set; }
        public int id_cerdaMadre { get; set; }
        public int nacidos_vivos { get; set; }
        public int nacidos_muertos { get; set; }


        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? fecha_nacimiento { get; set; }

        [DisplayFormat(DataFormatString= "{0:yyyy-MM-dd}")]
        public DateTime? fecha_ultimoParto { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? fecha_ultimoDestete { get; set; }

        public string observacion { get; set; }

        public int numero_adoptados { get; set; }


        public int numero_transferidos { get; set; }

        public string peso_promedio { get; set; }

        public int numero_parto { get; set; }

        public int numero_hembra { get; set; }

        public int numero_macho { get; set; }


        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? fecha_Destete { get; set; }
    }
}
