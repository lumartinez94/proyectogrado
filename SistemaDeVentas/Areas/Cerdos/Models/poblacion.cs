﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Cerdos.Models
{
    public class poblacion
    {


        [Display(Name="Población total")]
        public string poblacionTotal { get; set; }


        [Display(Name="# de lechones")]
        public string lechones {get;set;}

        [Display(Name = "# de padrote")]
        public string padrote { get; set; }


        [Display(Name = "# de reproductora")]

        public string reproductora { get; set; }

        [Display(Name = "# cuchilla")]
        public string cuchilla { get; set; }




        [Display(Name = "# en inicio")]
        public string inicio { get; set; }


        [Display(Name = "# en levante")]
        public string levante { get; set; }

        [Display(Name = "# en lactancia")]
        public string lactancia { get; set; }

        [Display(Name = "# en gestación")]
        public string gestante { get; set; }





    }
}
