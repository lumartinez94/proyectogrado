﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Cerdos.Models
{
    public class InputModelsServicio
    {
        public int id_Servicio { get; set; }


      
        [Display(Name = "Identificación Padrote")]
        public string id_macho { get; set; }

       
       
        [Display(Name = "Identificación Macho")]
        public string caravana_macho { get; set; }

        [Display(Name = "Identificación Hembra")]
        [Required(ErrorMessage = "<font color='red'> El campo identificación de la cerda es obligatorio</font>")]
        public string caravana_hembra { get; set; }
       

        [Required(ErrorMessage = "<font color='red'> El campo identificación de la cerda es obligatorio</font>")]
        [Display(Name = "Identificación Hembra")]
        public string id_hembra { get; set; }

        [Display(Name = "Observación")]
        public string Observacion { get; set; }


        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]

        [Required(ErrorMessage = "<font color='red'> El campo fecha de servicio es obligatorio</font>")]
        [Display(Name = "Fecha de servicio")]
        public DateTime? fecha_servicio{get;set;}
        

        public string intentos { get; set; }

        public List<SelectListItem> listaHembras { get; set; }

        [Required(ErrorMessage = "<font color='red'> El campo servicio exitoso es obligatorio</font>")]
        [Display(Name = "Servicio exitoso")]
        public List<SelectListItem> servicioExitoso { get; set; }



        [Required(ErrorMessage = "<font color='red'> El campo servicio exitoso es obligatorio</font>")]
        [Display(Name = "Servicio exitoso")]
        public string servicio_exitoso { get; set; }


    }
}

