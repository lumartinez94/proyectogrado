﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Cerdos.Models
{
    public class Cerdo_Razas
    {

        public int ID { get; set; }
        public string IdCerdo { get; set; }

        public string IdRaza { get; set; }

        public DateTime fecha_registro { get; set; }

        public float peso { get; set; }
        public string sexo { get; set; }

    }
}
