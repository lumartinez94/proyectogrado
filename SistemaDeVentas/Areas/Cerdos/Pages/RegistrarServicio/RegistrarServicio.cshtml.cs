using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.Cerdos.Controllers;
using SistemaDeVentas.Areas.Cerdos.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Cerdos.Pages.RegistrarServicio
{
    [Authorize]
    public class RegistrarServicioModel : PageModel
    {
        


   

            private ListObject Objeto = new ListObject();
            private static String idGet = null;
            private static List<servicio> listServicio;
            private static String user = null;
        private static string[] Servicio_exitoso = { "Si", "No" };

        public RegistrarServicioModel(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager, ApplicationDbContext context, IHostingEnvironment environment)
            {
                Objeto._roleManager = roleManager;
                Objeto._usuarios = new Usuarios();
                Objeto._context = context;
                Objeto._usersRoles = new UserRoles();
                Objeto._environment = environment;
                Objeto._image = new UploadImage();
                Objeto._userRoles = new List<SelectListItem>();
                Objeto._userManager = userManager;
            }

            [BindProperty]
            public InputModel Input { get; set; }
            public class InputModel : InputModelsServicio
            {


                [TempData]

                public string ErrorMessage { get; set; }

                public List<SelectListItem> listaMachos { get; set; }
                public List<SelectListItem> listaHembras { get; set; }
            public List<SelectListItem> servicioExitoso { get; set; }



        }

            public async Task OnGetAsync(String id)
            {


                if (id != null)
                {
                    idGet = id;
                    await setEditAsync(int.Parse(id));
                }
            else
            {
                Input = new InputModel
                {

                    listaHembras = getHembras(),
                    listaMachos = getMachos(),
                    servicioExitoso= getServicio()
                    
                };

            }


            }

            public List<SelectListItem> getHembras(string data = null)
            {
                List<SelectListItem> list = new List<SelectListItem>();
                var hembras = Objeto._context.Cerdos.Where(c => c.Sexo.Equals("H") && c.id_lineaProduccion.Equals(2) && c.id_estado.Equals(1)).ToList();


                if (data != null)
                {

                    var busqueda = Objeto._context.Cerdos.Find(int.Parse(data));

                    list.Add(new SelectListItem
                    {
                        Text = busqueda.codigo,
                        Value = busqueda.ID.ToString()
                    });

                    hembras.ForEach(item =>
                    {

                        if (!item.ID.ToString().Equals(data))
                        {
                            list.Add(new SelectListItem
                            {
                                Value = item.ID.ToString(),
                                Text = item.codigo

                            });

                        }
                    });




                }
                else
                {
                    hembras.ForEach(item =>
                    {
                        list.Add(new SelectListItem
                        {

                            Text = item.codigo,
                            Value = item.ID.ToString()
                        }

                        );

                    });
                }


                return list;
            }

            public List<SelectListItem> getMachos(string data=null)
            {
                List<SelectListItem> list = new List<SelectListItem>();
                var machos = Objeto._context.Cerdos.Where(c => c.Sexo.Equals("M") && c.id_lineaProduccion.Equals(1) && c.castrado.Equals("No") && c.id_estado.Equals(1)).ToList();
               

                if (data != null){

                    var busqueda = Objeto._context.Cerdos.Find(int.Parse(data));

                    list.Add(new SelectListItem
                    {
                        Text = busqueda.codigo,
                        Value = busqueda.ID.ToString()
                    });

                    machos.ForEach(item =>
                    {

                        if (!item.ID.ToString().Equals(data)) {
                            list.Add(new SelectListItem
                            {
                                Value = item.ID.ToString(),
                                Text = item.codigo

                            });
    
                        }
                    });


                    

                }
                else
                {
                    machos.ForEach(item =>
                    {
                    list.Add(new SelectListItem
                    {

                        Text = item.codigo,
                        Value = item.ID.ToString()
                    }

                    );

                    });
                }


                return list;
            }

        private List<SelectListItem> getServicio(string data = null)
        {
            List<SelectListItem> Listdata = new List<SelectListItem>();

            if (data != null)
            {
                if (data.Equals("Si"))
                {

                    Listdata.Add(new SelectListItem
                    {
                        Value = "0",
                        Text = data
                    });
                }
                else
                {
                    Listdata.Add(new SelectListItem
                    {
                        Value = "0",
                        Text = data
                    });
                }

                for (int i = 0; i < Servicio_exitoso.Length; i++)
                {
                    if (!Servicio_exitoso[i].Equals(data))
                    {
                        var value = Servicio_exitoso[i];

                        Listdata.Add(new SelectListItem
                        {
                            Value = i.ToString(),
                            Text = value
                        });
                    }
                }
            }
            else
            {

                Listdata.Add(new SelectListItem
                {
                    Value = "0",
                    Text = "Si"

                });

                Listdata.Add(new SelectListItem
                {
                    Value = "1",
                    Text = "No"

                });

            }

            return Listdata;

        }

        public async Task<IActionResult> OnPostAsync()
            {


                if (idGet == null)
                {


                    var valor = await Save();
                    if (valor)
                    {
                        return RedirectToAction(nameof(ServicioController.Servicio), "Servicio");
                    }
                    else
                    {
                        //return RedirectToAction("~/Areas/Cerdos/Pages/RegistrarServicio/RegistrarServicio.cshtml");
                        return Page();
                    }
                }
                else
                {
                     idGet = null;
                    var valor = await updateUser();
                    if (valor)
                    {
                        return RedirectToAction(nameof(ServicioController.Servicio), "Servicio");
                    }
                    else
                    {
                        return Page();
                    }
                }

            }

            private async Task<bool> Save()
            {
                var user = HttpContext.User.Identity.Name;

                var valor = false;
                try
                {




                    var userlist = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                    var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].Id)).ToList();



                    if (ModelState.IsValid)
                    {
                        var servicios = new servicio
                        {
                            id_cerdoHembra=int.Parse(Input.id_hembra),
                            id_cerdoMacho= int.Parse(Input.id_macho),
                            fecha= Input.fecha_servicio,
                            servicio_exitoso= Input.servicio_exitoso,
                            Observacion= Input.Observacion,
                            

                        };





                        var log_aplicacion = new Historial_Acciones
                        {
                            IdRole = userList5[0].RoleId,
                            Accion = "Guardar",
                            item = "Servicio",
                            fecha_registro = DateTime.Now,
                            IdUser = userlist[0].Id,
                        };


                        await Objeto._context.AddAsync(servicios);
                        await Objeto._context.AddAsync(log_aplicacion);
                        Objeto._context.SaveChanges();

                        valor = true;







                    }

                    else
                    {
                        Input = new InputModel
                        {
                            ErrorMessage = "Complete los campos",
                            listaHembras = getHembras(),
                            listaMachos= getMachos(),
                            servicioExitoso = getServicio()

                        };

                        valor = false;
                    }


                }
                catch (Exception ex)
                {

                    Input = new InputModel
                    {
                        ErrorMessage = ex.Message,
                        listaHembras = getHembras(),
                        listaMachos = getMachos(),
                        servicioExitoso = getServicio()

                    };
                    valor = false;
                }
                return valor;
            }

            private async Task setEditAsync(int id)
            {
                var user = HttpContext.User.Identity.Name;

                listServicio = Objeto._context.servicio.Where(s => s.id.Equals(id)).ToList();
                var userlist = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].Id)).ToList();


                Input = new InputModel
                {
                    id_Servicio = listServicio[0].id,
                    listaHembras= getHembras(listServicio[0].id_cerdoHembra.ToString()),
                    listaMachos = getMachos(listServicio[0].id_cerdoMacho.ToString()),
                    fecha_servicio = listServicio[0].fecha,
                    Observacion = listServicio[0].Observacion,
                    servicioExitoso = getServicio(listServicio[0].servicio_exitoso),
                    servicio_exitoso= listServicio[0].servicio_exitoso,




                };

            }

            private async Task<bool> updateUser()

            {
                var valor = false;

                try
                {
                idGet = null;
                var user = HttpContext.User.Identity.Name;
                var userlist = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                    var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].Id)).ToList();


                    var servicios = new servicio
                    {
                        id = listServicio[0].id,
                        id_cerdoHembra= int.Parse(Input.id_hembra),
                        id_cerdoMacho= int.Parse(Input.id_macho),
                        Observacion= Input.Observacion,
                        fecha= Input.fecha_servicio,
                        servicio_exitoso= Input.servicio_exitoso,
                        intentos= listServicio[0].intentos,
                        estado= listServicio[0].estado
                        
                        


                    };

                    Objeto._context.Update(servicios);
                    await Objeto._context.SaveChangesAsync();

                    var log_aplicaciones = new Historial_Acciones
                    {
                        Accion = "Editar",
                        item = "Servicio",
                        fecha_registro = DateTime.Now,
                        IdRole = userList5[0].RoleId,
                        IdUser = userlist[0].Id,



                    };


                    await Objeto._context.AddAsync(log_aplicaciones);
                    Objeto._context.SaveChanges();






                    valor = true;



                }
                catch (Exception ex)
                {

                    Input = new InputModel
                    {
                        ErrorMessage = ex.Message,
                        listaHembras= getHembras(listServicio[0].id_cerdoHembra.ToString()),
                        listaMachos=getMachos(listServicio[0].id_cerdoMacho.ToString()),
                        servicioExitoso= getServicio(listServicio[0].servicio_exitoso)
                    };
                    valor = false;
                }
                return valor;


            }
        
    }
}