using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SistemaDeVentas.Areas.Cerdos.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Cerdos.Pages.EliminarCerdo
{
    [Authorize]
    public class EliminarCerdoModel : PageModel
    {






        private ListObject objeto = new ListObject();
        public static InputModel model;
        public EliminarCerdoModel(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context, IHostingEnvironment environment)

        {
            objeto._context = context;
            objeto._environment = environment;
            objeto._usuarios = new Usuarios(userManager, signInManager, roleManager, context);
        }
        public async Task<IActionResult> OnGet(String id)
        {
            if (id != null)
            {
                var data = await objeto._context.Cerdos.FindAsync(int.Parse(id));
                Input = new InputModel
                {

                    ID = data.ID,
                    Sexo = data.Sexo,
                    Peso = data.Peso,
                    codigo=data.codigo,
                    fecha_destete = data.fecha_destete,
                    fecha_nacimiento = data.fecha_nacimiento,
                    linea_produccion = data.id_lineaProduccion.ToString(),



                };
                model = Input;
                return Page();
            }
            else
            {

                return Redirect("/Cerdos?area=Cerdos");
            }
        }

        private string getNameProduccion(int id_lineaProduccion)
        {
            string result = "";
            try
            {
                var listProduccion = objeto._context.Linea_Produccion.Find(id_lineaProduccion);
                result = listProduccion.nombre;


            }
            catch (Exception ex)
            {

                result = ex.Message;
            }

            return result;
        }

        public InputModel Input { get; set; }
        public class InputModel : InputModelsCerdos
        {
            [TempData]

            public string ErrorMesssage { get; set; }

        }

        public IActionResult OnPost()
        {
            try
            {

                var users = HttpContext.User.Identity.Name;
                var userList = objeto._context.Users.Where(u => u.Email.Equals(users)).ToList();
                var userList5 = objeto._context.UserRoles.Where(u => u.UserId.Equals(userList[0].Id)).ToList();
                var DataCerdo = objeto._context.Cerdos.Find(model.ID);
                

                objeto._context.Cerdos.Remove(DataCerdo);
                objeto._context.SaveChanges();


                var log_aplicaciones = new Historial_Acciones
                {
                    Accion = "Eliminar",
                    item = "Cerdos",
                    fecha_registro = DateTime.Now,
                    IdRole = userList5[0].RoleId,
                    IdUser = userList[0].Id,



                };

                objeto._context.AddAsync(log_aplicaciones);
                objeto._context.SaveChanges();

                return Redirect("/Cerdos?area=Cerdos");
            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMesssage = ex.Message
                };
                return Page();
            }
        }
    }
}


