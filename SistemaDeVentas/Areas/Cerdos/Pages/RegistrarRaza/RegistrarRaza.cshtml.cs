using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.Cerdos.Controllers;
using SistemaDeVentas.Areas.Cerdos.Models;
using SistemaDeVentas.Areas.Users.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Cerdos.Pages.RegistrarRaza
{

    [Authorize]


    public class RegistrarRazaModel : PageModel
    {

        private ListObject Objeto = new ListObject();
        private static String idGet = null;
        private static List<IdentityUser> userList1;
        private static List<TUsuarios> userList2;
        private static List<Raza> userList4;
       

        public RegistrarRazaModel(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager, ApplicationDbContext context, IHostingEnvironment environment)
        {
            Objeto._roleManager = roleManager;
            Objeto._usuarios = new Usuarios();
            Objeto._context = context;
            Objeto._usersRoles = new UserRoles();
            Objeto._environment = environment;
            Objeto._image = new UploadImage();
            Objeto._userRoles = new List<SelectListItem>();
            Objeto._userManager = userManager;
        }

        [BindProperty]
        public InputModel Input { get; set; }
        public class InputModel : InputModelsCerdos
        {


            [TempData]
           
            public string ErrorMessage { get; set; }
        


        }
   
         public async Task OnGetAsync(String id)
        {
          

            if (id != null)
            {
                idGet = id;
                await setEditAsync(int.Parse(idGet));
            }
            

        }


        public async Task<IActionResult> OnPostAsync()
        {
            if (idGet == null)
            {


                var valor = await Save();
                if (valor)
                {
                    return RedirectToAction(nameof(CerdosController.Razas), "Cerdos");
                }else
                {
                    return RedirectToAction("~/Areas/Cerdos/Pages/RegistrarRaza/RegistrarRaza.cshtml");
                }
            }
            else
            {
                var valor = await updateUser();
                if (valor)
                {
                    idGet = null;
                    return RedirectToAction(nameof(CerdosController.Razas), "Cerdos");
                }
                else
                {
                    return RedirectToAction("~/Areas/Cerdos/Pages/RegistrarRaza/RegistrarRaza.cshtml");
                }
            }

        }

        private async Task<bool> Save()
        {

            
            var valor = false;
            try
            {
                
                    

                     userList1 = Objeto._userManager.Users.Where(u => u.Email.Equals(Input.email)).ToList();
          
                   

                     userList4 = Objeto._context.Razas.Where(u => u.Nombre.Equals(Input.Nombre)).ToList();
                    var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userList1[0].Id)).ToList();


                    if (userList4.Count.Equals(0))
                    {
                       
                        var raza = new Raza
                        {
                            Nombre = Input.Nombre,
                            Descripcion = Input.Descripcion,
                            fecha_registro = DateTime.Now


                        };
                      
                       
                           
                           


                            var log_aplicacion = new Historial_Acciones
                            {
                                IdRole = userList5[0].RoleId,
                                Accion = "Guardar",
                                item = "Raza",
                                fecha_registro=DateTime.Now,
                                IdUser = userList1[0].Id,
                            };

                           
                            await Objeto._context.AddAsync(raza);
                            await Objeto._context.AddAsync(log_aplicacion);
                            Objeto._context.SaveChanges();
                          
                            valor = true;

                        }
                       


                    
                 
                  
                
                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Complete los campos",

                    };

                    valor = false;
                }


            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                  
                };
                valor = false;
            }
            return valor;
        }

        private async Task setEditAsync(int id)
        {
            var user=HttpContext.User.Identity.Name;
            userList1 = Objeto._userManager.Users.Where(u => u.Email.Equals(user)).ToList();
          
             userList4 = Objeto._context.Razas.Where(u => u.Id.Equals(id)).ToList();
              var userList5 =  Objeto._context.UserRoles.Where(u => u.UserId.Equals(userList1[0].Id)).ToList();
          


            Input = new InputModel
            {
               Nombre=userList4[0].Nombre,
               Descripcion=userList4[0].Descripcion
            };




        }

        private async Task<bool> updateUser()

        {
            var valor = false;

            try
            {
                var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userList1[0].Id)).ToList();

                
                    var raza = new Raza
                    {
                        Id=userList4[0].Id,
                        Nombre=Input.Nombre,
                        Descripcion=Input.Descripcion


                    };

                    Objeto._context.Update(raza);
                    await Objeto._context.SaveChangesAsync();

                    var log_aplicaciones = new Historial_Acciones
                    {
                        Accion ="Editar" ,
                        item = "Raza",
                        fecha_registro= DateTime.Now,
                        IdRole=userList5[0].RoleId,
                        IdUser=userList1[0].Id,
                       


                    };


                    await Objeto._context.AddAsync(log_aplicaciones);
                    Objeto._context.SaveChanges();






                    valor = true;

                
               
            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                };
                valor = false;
            }
            return valor;


        }
    }
}