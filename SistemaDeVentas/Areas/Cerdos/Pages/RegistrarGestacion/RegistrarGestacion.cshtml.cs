using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.Cerdos.Controllers;
using SistemaDeVentas.Areas.Cerdos.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Cerdos.Pages.RegistrarGestacion
{


    [Authorize]
    public class RegistrarGestacionModel : PageModel
    {

        private ListObject Objeto = new ListObject();
        private static String idGet = null;
        private static List<Gestacion> listGestacion;


        public RegistrarGestacionModel(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager, ApplicationDbContext context, IHostingEnvironment environment)
        {
            Objeto._roleManager = roleManager;
            Objeto._usuarios = new Usuarios();
            Objeto._context = context;
            Objeto._usersRoles = new UserRoles();
            Objeto._environment = environment;
            Objeto._image = new UploadImage();
            Objeto._userRoles = new List<SelectListItem>();
            Objeto._userManager = userManager;
        }

        [BindProperty]
        public InputModel Input { get; set; }
        public class InputModel : InputModelsGestacion
        {


            [TempData]

            public string ErrorMessage { get; set; }


            public List<SelectListItem> listaHembras { get; set; }




        }

        public async Task OnGetAsync(String id)
        {


            if (id != null)
            {
                idGet = id;
                await setEditAsync(int.Parse(id));
            }
            else
            {
                Input = new InputModel
                {

                    listaHembras = getHembras(),


                };

            }


        }

        public List<SelectListItem> getHembras(string data = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var hembras = Objeto._context.Cerdos.Where(c => c.Sexo.Equals("H") && c.id_lineaProduccion.Equals(2) && c.id_estado.Equals(1)).ToList();


            if (data != null)
            {

                var busqueda = Objeto._context.Cerdos.Find(int.Parse(data));

                list.Add(new SelectListItem
                {
                    Text = busqueda.codigo,
                    Value = busqueda.ID.ToString()
                });

                hembras.ForEach(item =>
                {

                    if (!item.ID.ToString().Equals(data))
                    {
                        list.Add(new SelectListItem
                        {
                            Value = item.ID.ToString(),
                            Text = item.codigo

                        });

                    }
                });




            }
            else
            {
                hembras.ForEach(item =>
                {
                    list.Add(new SelectListItem
                    {

                        Text = item.codigo,
                        Value = item.ID.ToString()
                    }

                    );

                });
            }


            return list;
        }





        public async Task<IActionResult> OnPostAsync()
        {


            if (idGet == null)
            {


                var valor = await Save();
                if (valor)
                {
                    return RedirectToAction(nameof(GestacionController.Gestacion), "Gestacion");
                }
                else
                {
                    //return RedirectToAction("~/Areas/Cerdos/Pages/RegistrarServicio/RegistrarServicio.cshtml");
                    return Page();
                }
            }
            else
            {
                var valor = await updateUser();
                if (valor)
                {
                    idGet = null;
                    return RedirectToAction(nameof(GestacionController.Gestacion), "Gestacion");
                }
                else
                {
                    return Page();
                }
            }

        }

        private async Task<bool> Save()
        {
            var user = HttpContext.User.Identity.Name;

            var valor = false;
            try
            {


                var fecha = DateTime.Parse(Input.fecha_inicio.ToString());

                var fecha1 = fecha.AddMonths(3);
                var fecha2 = fecha1.AddDays(24);





                var cerda = Objeto._context.gestacion.Where(g => g.id_cerdo.Equals(int.Parse(Input.id_cerdo)) && g.estado.Equals("En proceso")).ToList();
                var userlist = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].Id)).ToList();



                if (ModelState.IsValid)
                {

                    if (cerda.Count.Equals(0))
                    {
                        var servicios = new Gestacion
                        {
                            id_cerdo = int.Parse(Input.id_cerdo),
                            fecha_inicio = Input.fecha_inicio,
                            fecha_aproximada = fecha2,
                            estado= "En proceso"







                        };





                        var log_aplicacion = new Historial_Acciones
                        {
                            IdRole = userList5[0].RoleId,
                            Accion = "Guardar",
                            item = "Gestacion",
                            fecha_registro = DateTime.Now,
                            IdUser = userlist[0].Id,
                        };


                        await Objeto._context.AddAsync(servicios);
                        await Objeto._context.AddAsync(log_aplicacion);
                        Objeto._context.SaveChanges();

                        valor = true;




                    }
                    else
                    {
                        Input = new InputModel
                        {
                            ErrorMessage = "La cerda no ha finalizado aun el periodo de gestación",
                            listaHembras = getHembras(),


                        };

                        valor = false;
                    }


                }

                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Complete los campos",
                        listaHembras = getHembras(),


                    };

                    valor = false;
                }


            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                    listaHembras = getHembras(),


                };
                valor = false;
            }
            return valor;
        }

        private async Task setEditAsync(int id)
        {
            var user = HttpContext.User.Identity.Name;

            listGestacion = Objeto._context.gestacion.Where(s => s.id.Equals(id)).ToList();
            var userlist = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
            var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].Id)).ToList();


            Input = new InputModel
            {
                listaHembras = getHembras(listGestacion[0].id_cerdo.ToString()),
                id_gestacion = listGestacion[0].id.ToString(),
                fecha_inicio = listGestacion[0].fecha_inicio




            };

        }

        private async Task<bool> updateUser()

        {
            var valor = false;

            try
            {
                idGet = null;
                var user = HttpContext.User.Identity.Name;
                var userlist = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].Id)).ToList();
                var fecha = DateTime.Parse(Input.fecha_inicio.ToString());

                var fecha1 = fecha.AddMonths(3);
                var fecha2 = fecha1.AddDays(24);

                var servicios = new Gestacion
                {
                    id = listGestacion[0].id,
                    id_cerdo = int.Parse(Input.id_cerdo),
                    fecha_inicio = Input.fecha_inicio!=listGestacion[0].fecha_inicio ? Input.fecha_inicio : listGestacion[0].fecha_inicio,
                    fecha_aproximada = listGestacion[0].fecha_aproximada !=fecha2 ? fecha2 :listGestacion[0].fecha_aproximada,
                    estado = listGestacion[0].estado,
                    fecha_real = listGestacion[0].fecha_real




                };

                Objeto._context.Update(servicios);
                await Objeto._context.SaveChangesAsync();

                var log_aplicaciones = new Historial_Acciones
                {
                    Accion = "Editar",
                    item = "Gestacion",
                    fecha_registro = DateTime.Now,
                    IdRole = userList5[0].RoleId,
                    IdUser = userlist[0].Id,



                };


                await Objeto._context.AddAsync(log_aplicaciones);
                Objeto._context.SaveChanges();






                valor = true;



            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                    listaHembras = getHembras(listGestacion[0].id_cerdo.ToString()),


                };
                valor = false;
            }
            return valor;


        }

    }
}
