using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.Cerdos.Controllers;
using SistemaDeVentas.Areas.Cerdos.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Cerdos.Pages.RegistrarParto
{
    public class RegistrarPartoModel : PageModel
    {
        private ListObject Objeto = new ListObject();
        private static String idGet = null;
        private static List<IdentityUser> userList1;
        private static List<Parto> partoList;
        private static String user = null;

        public RegistrarPartoModel(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager, ApplicationDbContext context, IHostingEnvironment environment)
        {
            Objeto._roleManager = roleManager;
            Objeto._usuarios = new Usuarios();
            Objeto._context = context;
            Objeto._usersRoles = new UserRoles();
            Objeto._environment = environment;
            Objeto._image = new UploadImage();
            Objeto._userRoles = new List<SelectListItem>();
            Objeto._userManager = userManager;
        }

        [BindProperty]
        public InputModel Input { get; set; }
        public class InputModel : InputModelsPartos
        {


            [TempData]

            public string ErrorMessage { get; set; }
            public List<SelectListItem> listaHembras { get; set; }



        }


        public List<SelectListItem> getHembras(string data = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var hembras = Objeto._context.Cerdos.Where(c => c.Sexo.Equals("H") && c.id_lineaProduccion.Equals(2) && c.id_estado.Equals(1)).ToList();


            if (data != null)
            {

                var busqueda = Objeto._context.Cerdos.Find(int.Parse(data));

                list.Add(new SelectListItem
                {
                    Text = busqueda.codigo,
                    Value = busqueda.ID.ToString()
                });

                hembras.ForEach(item =>
                {

                    if (!item.ID.ToString().Equals(data))
                    {
                        list.Add(new SelectListItem
                        {
                            Value = item.ID.ToString(),
                            Text = item.codigo

                        });

                    }
                });




            }
            else
            {
                hembras.ForEach(item =>
                {
                    list.Add(new SelectListItem
                    {

                        Text = item.codigo,
                        Value = item.ID.ToString()
                    }

                    );

                });
            }


            return list;
        }

        public async Task OnGetAsync(String id)
        {


            if (id != null)
            {
                idGet = id;
                await setEditAsync(int.Parse(idGet));
            }
            else
            {
                Input = new InputModel
                {
                    listaHembras = getHembras()

                };


            }
        }


        public async Task<IActionResult> OnPostAsync()
        {
            if (idGet == null)
            {


                var valor = await Save();
                if (valor)
                {
                    return RedirectToAction(nameof(PartoController.Parto), "Parto");
                }
                else
                {
                    return  Page();
                }
            }
            else
            {
                var valor = await updateUser();
                if (valor)
                {
                    idGet = null;
                    return RedirectToAction(nameof(PartoController.Parto), "Parto");
                }
                else
                {
                    return Page();
                }
            }

        }

        private async Task<bool> Save()
        {


            var valor = false;
            try
            {
                var user=HttpContext.User.Identity.Name;


                userList1 = Objeto._userManager.Users.Where(u => u.Email.Equals(user)).ToList();
                var gestacion = Objeto._context.gestacion.Where(g => g.id_cerdo.Equals(int.Parse(Input.id_cerdaMadre)) && g.estado.Equals("En proceso")).ToList();
               var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userList1[0].Id)).ToList();



                if (ModelState.IsValid)
                {

                    var partos = new Parto
                    {
                        id_cerdaMadre = int.Parse(Input.id_cerdaMadre),
                        fecha_nacimiento = Input.fecha_nacimiento,
                        nacidos_muertos = int.Parse(Input.nacidos_muertos) > 0 ? int.Parse(Input.nacidos_muertos) : 0,
                        nacidos_vivos = int.Parse(Input.nacidos_vivos) > 0 ? int.Parse(Input.nacidos_vivos) : 0,
                        numero_adoptados= int.Parse(Input.numero_adoptados) > 0 ? int.Parse(Input.numero_adoptados) : 0,
                        numero_hembra= int.Parse(Input.nacidos_vivos) > 0 ? int.Parse(Input.nacidos_vivos) : 0,
                        numero_macho= int.Parse(Input.nacidos_vivos) > 0 ? int.Parse(Input.nacidos_vivos) : 0,
                        numero_transferidos= int.Parse(Input.nacidos_vivos) > 0 ? int.Parse(Input.nacidos_vivos) : 0,
                        peso_promedio= Input.peso_promedio !=null  ? Input.peso_promedio : "0",
                        fecha_Destete=Input.fecha_Destete,
                        

                       


                    };

                    

                   




                    var log_aplicacion = new Historial_Acciones
                    {
                        IdRole = userList5[0].RoleId,
                        Accion = "Guardar",
                        item = "Parto",
                        fecha_registro = DateTime.Now,
                        IdUser = userList1[0].Id,
                    };


                    await Objeto._context.AddAsync(partos);
                    await Objeto._context.AddAsync(log_aplicacion);
                    Objeto._context.SaveChanges();


                    //if (gestacion.Count >= 1)
                    //{

                    //    var updateProceso = new Gestacion
                    //    {

                    //        id = gestacion[0].id,
                    //        id_cerdo = gestacion[0].id_cerdo,
                    //        fecha_aproximada = gestacion[0].fecha_aproximada,
                    //        fecha_inicio = gestacion[0].fecha_inicio,
                    //        estado = "Finalizado",
                    //        fecha_real = Input.fecha_nacimiento


                    //    };

                    //    Objeto._context.Update(updateProceso);
                    //    Objeto._context.SaveChanges();

                    //}

                    valor = true;

                }







                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Complete los campos",
                        listaHembras = getHembras(),

                    };

                    valor = false;
                }


            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                    listaHembras = getHembras(),

                };
                valor = false;
            }
            return valor;
        }

        private async Task setEditAsync(int id)
        {
            var user = HttpContext.User.Identity.Name;
            partoList = Objeto._context.parto.Where(p => p.id.Equals(id)).ToList();
            userList1 = Objeto._userManager.Users.Where(u => u.Email.Equals(user)).ToList();
            var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userList1[0].Id)).ToList();



            Input = new InputModel
            {
                id_parto = partoList[0].id.ToString(),
                id_cerdaMadre = partoList[0].ToString(),
                nacidos_vivos = partoList[0].nacidos_vivos.ToString(),
                nacidos_muertos = partoList[0].nacidos_muertos.ToString(),
                fecha_nacimiento = partoList[0].fecha_nacimiento,
                fecha_Destete = partoList[0].fecha_Destete,
                listaHembras = getHembras(partoList[0].id_cerdaMadre.ToString()),
               numero_adoptados= partoList[0].numero_adoptados.ToString(),
               numero_transferidos= partoList[0].numero_transferidos.ToString(),
               numero_hembra= partoList[0].numero_hembra.ToString(),
               numero_macho= partoList[0].numero_macho.ToString(),
               peso_promedio= partoList[0].peso_promedio,
               


            };




        }

        private async Task<bool> updateUser()

        {
            var valor = false;

            try
            {
                partoList = Objeto._context.parto.Where(p => p.id.Equals(int.Parse(Input.id_parto))).ToList();
                var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userList1[0].Id)).ToList();


                var partos = new Parto
                {
                    id= partoList[0].id,
                    id_cerdaMadre = int.Parse(Input.id_cerdaMadre),
                    fecha_nacimiento = Input.fecha_nacimiento,
                    nacidos_muertos = int.Parse(Input.nacidos_muertos) > 0 ? int.Parse(Input.nacidos_muertos) : 0,
                    nacidos_vivos = int.Parse(Input.nacidos_vivos) > 0 ? int.Parse(Input.nacidos_vivos) : 0,
                    numero_adoptados = int.Parse(Input.numero_adoptados) > 0 ? int.Parse(Input.numero_adoptados) : 0,
                    numero_hembra = int.Parse(Input.nacidos_vivos) > 0 ? int.Parse(Input.nacidos_vivos) : 0,
                    numero_macho = int.Parse(Input.nacidos_vivos) > 0 ? int.Parse(Input.nacidos_vivos) : 0,
                    numero_transferidos = int.Parse(Input.nacidos_vivos) > 0 ? int.Parse(Input.nacidos_vivos) : 0,
                    peso_promedio = Input.peso_promedio != null ? Input.peso_promedio : "0",
                    fecha_Destete = Input.fecha_Destete,
                    numero_parto= partoList[0].numero_parto,
                    fecha_ultimoDestete= partoList[0].fecha_ultimoDestete,
                    fecha_ultimoParto= partoList[0].fecha_ultimoParto

                };

                Objeto._context.Update(partos);
                await Objeto._context.SaveChangesAsync();

                var log_aplicaciones = new Historial_Acciones
                {
                    Accion = "Editar",
                    item = "Parto",
                    fecha_registro = DateTime.Now,
                    IdRole = userList5[0].RoleId,
                    IdUser = userList1[0].Id,



                };


                await Objeto._context.AddAsync(log_aplicaciones);
                Objeto._context.SaveChanges();






                valor = true;



            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                    listaHembras = getHembras(partoList[0].id_cerdaMadre.ToString()),
                };
                valor = false;
            }
            return valor;


        }
    }
}