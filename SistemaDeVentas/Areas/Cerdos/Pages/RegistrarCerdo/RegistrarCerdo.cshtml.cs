using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.Cerdos.Controllers;
using SistemaDeVentas.Areas.Cerdos.Models;
using SistemaDeVentas.Areas.Corrales.Models;
using SistemaDeVentas.Areas.Users.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Cerdos.Pages.RegistrarCerdo
{
    [Authorize]
    public class RegistrarCerdoModel : PageModel
    {



        private ListObject Objeto = new ListObject();
        private static String idGet = null;
        private static List<IdentityUser> userList1;
        private static List<TUsuarios> userList2;
        private static List<AppUsuarios> userList3;

        private static List<IdentityRole> userList5;
        private static List<Cerdo> cerdosList;
        private static List<tipo_alimentacion> alimentList;
        private static List<linea_produccion> prodList;
        private static List<Corral> corralList;
        private static List<Raza> razaList;
        private static String user = null;
        private static string[] sexoList = { "M", "H" };
        private static string[] castradoList = { "Si", "No" };

        public RegistrarCerdoModel(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager, ApplicationDbContext context, IHostingEnvironment environment)
        {
            Objeto._roleManager = roleManager;
            Objeto._usuarios = new Usuarios();
            Objeto._context = context;
            Objeto._usersRoles = new UserRoles();
            Objeto._environment = environment;
            Objeto._image = new UploadImage();
            Objeto._userRoles = new List<SelectListItem>();
            Objeto._userManager = userManager;
        }
        public async Task OnGetAsync(String id)
        {

            if (id != null)
            {
                idGet = id;
                await setEditAsync(id);
            }
            else
            {




                Input = new InputModel
                {

                    alimentacionList = getAlimentacion(),
                    codigo = getCode(),
                    razaList = getRazas(),
                    produccionList = getProduccion(),
                    corralList = getCorrales(),
                    sexoList = getSexo(),
                    castradoList = getCastrado(),
                    estadoList = getEstado()
                };

            }
        }

        private string getCode()
        {
            string result = "";

            try
            {
                var cerdos = Objeto._context.Cerdos.ToList();
                var cantidad = cerdos.Count();
                var incremento = 0;
                var adicion = 1;
                var date = DateTime.Now.ToString("yyyy");
                var sum = 0;
                if (cantidad > 0)
                {
                    var code = cerdos[cantidad - 1].codigo;
                    var arrayData = code.Split('-');
                  
                                       
                        if (!arrayData[0].Equals(date))
                        {
                        arrayData[0] = DateTime.Now.ToString("yyyy");
                        arrayData[1] = adicion.ToString();

                        result = arrayData[0] + "-" + arrayData[1];

                    }
                    else
                    {
                        incremento = int.Parse(arrayData[1]);
                        sum = incremento + adicion;
                        arrayData[1] = sum.ToString();
                        result = arrayData[0] + "-" + arrayData[1];
                    }



                }
                else
                {
                    result = date + "-" + adicion;
                }


            }
            catch (Exception ex)
            {

                result = ex.Message;
            }

            return result;
        }
    


        [BindProperty]
        public InputModel Input { get; set; }
        public class InputModel : InputModelsCerdos
        {
            [TempData]
            public string ErrorMessage { get; set; }
            public List<SelectListItem> alimentacionList { get; set; }
            public List<SelectListItem> razaList { get; set; }
            public List<SelectListItem> produccionList { get; set; }

            public List<SelectListItem> corralList { get; set; }
            public List<SelectListItem> sexoList { get; set; }

            public List<SelectListItem> castradoList { get; set; }

            public List<SelectListItem> estadoList { get; set; }





        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (idGet == null)
            {


                var valor = await Save();
                if (valor)
                {
                    return RedirectToAction(nameof(CerdosController.Index), "Cerdos");
                }
                else
                {
                    return Page();
                }
            }
            else
            {
                var valor = await updateUser();
                if (valor)
                {
                    idGet = null;
                    return RedirectToAction(nameof(CerdosController.Index), "Cerdos");
                }
                else
                {
                    return Page();
                }
            }

        }


        private async Task<bool> Save()
        {
            var valor = false;
            try
            {
                var user = HttpContext.User.Identity.Name;
                var cerdos = new Cerdo();
                if (ModelState.IsValid)
                {



                    var TipoCorralList = Objeto._context.Corrals.Where(t => t.ID.Equals(int.Parse(Input.NombreCorral))).ToList();
                   var tipoAlimentoList = Objeto._context.tipo_Alimentacion.Where(ta => ta.nombre.Equals(Input.Nombre_alimentacion)).ToList();
                    var tipoProduccionList = Objeto._context.Linea_Produccion.Where(tp => tp.id.Equals(int.Parse(Input.nombre_produccion))).ToList();
                    var tipoRazaList = Objeto._context.Razas.Where(ra => ra.Id.Equals(int.Parse(Input.NombreRaza))).ToList();
                    var userlist = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                    var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].Id)).ToList();
                   

                   



                        if (Input.Sexo.ToLower().Equals("m"))
                        {
                            cerdos = new Cerdo
                            {
                               codigo=Input.codigo,
                                castrado = Input.castrado,
                                Sexo = Input.Sexo,

                                id_raza = tipoRazaList[0].Id,
                                id_alimentacion = tipoAlimentoList[0].id,
                                id_lineaProduccion = tipoProduccionList[0].id,
                                fecha_destete = Input.fecha_destete,
                                fecha_nacimiento = Input.fecha_nacimiento,
                                id_estado = int.Parse(Input.status),
                                observaciones = Input.observaciones,
                                fecha_registro = DateTime.Now,
                                numero_tetas = "",
                                id_corral = TipoCorralList[0].ID,
                                Peso = Input.Peso,
                                id_user = userlist[0].Id




                            };
                        }
                        else
                        {

                            cerdos = new Cerdo
                            {
                                codigo = Input.codigo,
                                numero_tetas = Input.numero_tetas,
                                Sexo = Input.Sexo,

                                id_raza = tipoRazaList[0].Id,
                                id_alimentacion = tipoAlimentoList[0].id,
                                id_lineaProduccion = tipoProduccionList[0].id,
                                fecha_destete = Input.fecha_destete,
                                fecha_nacimiento = Input.fecha_nacimiento,
                                id_estado = int.Parse(Input.status),
                                castrado = "",
                                observaciones = Input.observaciones,
                                fecha_registro = DateTime.Now,
                                id_corral = TipoCorralList[0].ID,
                                Peso = Input.Peso,



                            };


                            //var generarCodigo = await Objeto._context.Cerdos.FindAsync(cerdos);

                            //cerdos = new Cerdo
                            //{
                            //    ID = generarCodigo.ID,
                            //    castrado= generarCodigo.castrado,
                            //    caravana = generarCodigo.caravana,
                            //    numero_tetas = generarCodigo.numero_tetas,
                            //    Sexo = generarCodigo.Sexo,
                            //    color = generarCodigo.color,
                            //    id_raza = generarCodigo.id_raza,
                            //    id_alimentacion = generarCodigo.id_linea_produccion,
                            //    id_linea_produccion = generarCodigo.id_linea_produccion,
                            //    fecha_destete = generarCodigo.fecha_destete,
                            //    fecha_nacimiento = generarCodigo.fecha_nacimiento,
                            //    status = generarCodigo.status,
                            //    observaciones = generarCodigo.observaciones,
                            //    fecha_registro = generarCodigo.fecha_registro,
                            //    origen = generarCodigo.origen,
                            //    id_corral = generarCodigo.id_corral,
                            //    Peso = generarCodigo.Peso,
                            //    codigo = generarCodigo.ID.ToString() + "" + Input.Sexo + "" + Input.NombreRaza + "" + DateTime.Now.ToString("yyyyMMdd")
                            //};

                            //Objeto._context.Update(cerdos);
                            //await Objeto._context.SaveChangesAsync();


                        }


                        await Objeto._context.AddAsync(cerdos);
                        Objeto._context.SaveChanges();

                        var log_aplicaciones = new Historial_Acciones
                        {
                            Accion = "Guardar",
                            item = "Cerdos",
                            fecha_registro = DateTime.Now,
                            IdRole = userList5[0].RoleId,
                            IdUser = userlist[0].Id,



                        };

                        await Objeto._context.AddAsync(log_aplicaciones);
                        Objeto._context.SaveChanges();
                        valor = true;
                    
                  

                }


                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Faltan campos por seleccionar",
                        alimentacionList = getAlimentacion(),
                        razaList = getRazas(),
                        produccionList = getProduccion(),
                        corralList = getCorrales(),
                        sexoList = getSexo(),
                        castradoList = getCastrado(),
                        estadoList = getEstado()
                    };
                }


            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                    alimentacionList = getAlimentacion(),
                    razaList = getRazas(),
                    codigo = getCode(),
                    produccionList = getProduccion(),
                    corralList = getCorrales(),
                    sexoList = getSexo(),
                    castradoList = getCastrado(),
                    estadoList = getEstado()
                };
                valor = false;
            }
            return valor;
        }


        private async Task<bool> updateUser()

        {
            var valor = false;
            var user = HttpContext.User.Identity.Name;

            var TipoCorralList = Objeto._context.Corrals.Where(t => t.ID.Equals(int.Parse(Input.NombreCorral))).ToList();
            var tipoAlimentoList = Objeto._context.tipo_Alimentacion.Where(ta => ta.nombre.Equals(Input.Nombre_alimentacion)).ToList();
            var tipoProduccionList = Objeto._context.Linea_Produccion.Where(tp => tp.id.Equals(int.Parse(Input.nombre_produccion))).ToList();
            var tipoRazaList = Objeto._context.Razas.Where(ra => ra.Id.Equals(int.Parse(Input.NombreRaza))).ToList();
            var userList = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
            var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userList[0].Id)).ToList();
            var cerdos = new Cerdo();

            try
            {


                if (ModelState.IsValid)
                {
                    if (Input.Sexo.ToLower().Equals("m"))
                    {
                        cerdos = new Cerdo
                        {
                            ID = cerdosList[0].ID,
                            codigo=Input.codigo,
                            castrado = Input.castrado,
                            Sexo = Input.Sexo,

                            id_raza = tipoRazaList[0].Id,
                            id_alimentacion = tipoAlimentoList[0].id,
                            id_lineaProduccion = tipoProduccionList[0].id,
                            fecha_destete = Input.fecha_destete,
                            fecha_nacimiento = Input.fecha_nacimiento,
                            id_estado = int.Parse(Input.status),
                            observaciones = Input.observaciones,
                            fecha_registro = cerdosList[0].fecha_registro,
                            id_corral = TipoCorralList[0].ID,
                            Peso = Input.Peso,
                            id_user = cerdosList[0].id_user,
                            numero_tetas = ""





                        };
                    }
                    else
                    {

                        cerdos = new Cerdo
                        {
                            ID = cerdosList[0].ID,
                            codigo = Input.codigo,
                            numero_tetas = Input.numero_tetas,
                            Sexo = Input.Sexo,

                            id_raza = tipoRazaList[0].Id,
                            id_alimentacion = tipoAlimentoList[0].id,
                            id_lineaProduccion = tipoProduccionList[0].id,
                            fecha_destete = Input.fecha_destete,
                            fecha_nacimiento = Input.fecha_nacimiento,
                            id_estado = int.Parse(Input.status),
                            observaciones = Input.observaciones,
                            fecha_registro = DateTime.Now,
                            id_corral = TipoCorralList[0].ID,
                            Peso = Input.Peso,
                            castrado = ""



                        };
                    }

                    Objeto._context.Update(cerdos);
                    await Objeto._context.SaveChangesAsync();





                    var log_aplicaciones = new Historial_Acciones
                    {
                        Accion = "Editar",
                        item = "Cerdos",
                        fecha_registro = DateTime.Now,
                        IdRole = userList5[0].RoleId,
                        IdUser = userList[0].Id,



                    };
                    await Objeto._context.AddAsync(log_aplicaciones);
                    Objeto._context.SaveChanges();


                    valor = true;


                }
                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Seleccione un tipo",
                        alimentacionList = getAlimentacion(),
                        razaList = getRazas(),
                        produccionList = getProduccion(),
                        corralList = getCorrales(),
                        sexoList = getSexo(),
                        castradoList = getCastrado(),
                        estadoList = getEstado()

                    };
                    valor = false;
                }
            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                    alimentacionList = getAlimentacion(),
                    razaList = getRazas(),
                    produccionList = getProduccion(),
                    corralList = getCorrales(),
                    sexoList = getSexo(),
                    castradoList = getCastrado(),
                    estadoList = getEstado()
                };
                valor = false;
            }
            return valor;


        }

        private async Task setEditAsync(string id)
        {

            cerdosList = Objeto._context.Cerdos.Where(c => c.ID.Equals(int.Parse(id))).ToList();

            corralList = Objeto._context.Corrals.Where(c => c.ID.Equals(cerdosList[0].id_corral)).ToList();
            alimentList = Objeto._context.tipo_Alimentacion.Where(a => a.id.Equals(cerdosList[0].id_alimentacion)).ToList();
            prodList = Objeto._context.Linea_Produccion.Where(prod => prod.id.Equals(cerdosList[0].id_lineaProduccion)).ToList();
            razaList = Objeto._context.Razas.Where(raz => raz.Id.Equals(cerdosList[0].id_raza)).ToList();


            if (cerdosList[0].Sexo.ToLower().Equals("m"))
            {
                Input = new InputModel
                {
                    ID = cerdosList[0].ID,
                    codigo=cerdosList[0].codigo,
                   
                    observaciones = cerdosList[0].observaciones,
                    castradoList = getCastrado(cerdosList[0].castrado),
                    fecha_destete = cerdosList[0].fecha_destete,
                    fecha_nacimiento = cerdosList[0].fecha_nacimiento,
                    estadoList = getEstado(cerdosList[0].id_estado),
                    sexoList = getSexo(cerdosList[0].Sexo),
                    corralList = getCorrales(cerdosList[0].id_corral),
                    razaList = getRazas(cerdosList[0].id_raza),
                    Peso = cerdosList[0].Peso,
                    produccionList = getProduccion(cerdosList[0].id_lineaProduccion),
                    alimentacionList = getAlimentacion(cerdosList[0].id_alimentacion),







                };
            }
            else
            {


                Input = new InputModel
                {
                    ID = cerdosList[0].ID,

                    codigo = cerdosList[0].codigo,
                    observaciones = cerdosList[0].observaciones,
                    numero_tetas = cerdosList[0].numero_tetas,
                    fecha_destete = cerdosList[0].fecha_destete,
                    fecha_nacimiento = cerdosList[0].fecha_nacimiento,
                    Peso = cerdosList[0].Peso,
                    castradoList = getCastrado(cerdosList[0].castrado),
                    estadoList = getEstado(cerdosList[0].id_estado),
                    sexoList = getSexo(cerdosList[0].Sexo),
                    corralList = getCorrales(cerdosList[0].id_corral),
                    razaList = getRazas(cerdosList[0].id_raza),
                    produccionList = getProduccion(cerdosList[0].id_lineaProduccion),
                    alimentacionList = getAlimentacion(cerdosList[0].id_alimentacion),





                };
            }


        }

        private List<SelectListItem> getRazas(int id = 0)
        {
            List<SelectListItem> ListRazas = new List<SelectListItem>();
            var fillRaza = Objeto._context.Razas.ToList();

            if (id > 0)
            {
                var searchRaza = Objeto._context.Razas.Find(id);

                ListRazas.Add(new SelectListItem
                {
                    Value = searchRaza.Id.ToString(),
                    Text = searchRaza.Nombre


                });


                fillRaza.ForEach(item =>
                {
                    if (item.Id != id)
                    {
                        ListRazas.Add(new SelectListItem
                        {
                            Text = item.Nombre,
                            Value = item.Id.ToString()
                        })
;
                    }

                });

            }
            else
            {


                foreach (var item in fillRaza)
                {

                    ListRazas.Add(new SelectListItem
                    {

                        Value = item.Id.ToString(),
                        Text = item.Nombre
                    });

                }
            }

            return ListRazas;

        }

        private List<SelectListItem> getAlimentacion(int id = 0)
        {
            List<SelectListItem> ListAlimento = new List<SelectListItem>();

            var fillAlimento = Objeto._context.tipo_Alimentacion.ToList();

            if (id > 0)
            {
                var searchAlimento = Objeto._context.tipo_Alimentacion.Where(al => al.id.Equals(id)).ToList();

                ListAlimento.Add(new SelectListItem
                {
                    Value = searchAlimento[0].id.ToString(),
                    Text = searchAlimento[0].nombre,
                });

                fillAlimento.ForEach(item =>
                {
                    if (item.id != id)
                    {
                        ListAlimento.Add(new SelectListItem
                        {
                            Value = item.id.ToString(),
                            Text = item.nombre
                        });
                    }

                });
            }
            else
            {


                foreach (var item in fillAlimento)
                {

                    ListAlimento.Add(new SelectListItem
                    {

                        Value = item.id.ToString(),
                        Text = item.nombre
                    });

                }
            }

            return ListAlimento;

        }

        private List<SelectListItem> getProduccion(int id = 0)
        {
            List<SelectListItem> ListProduccion = new List<SelectListItem>();

            var fillAlimento = Objeto._context.Linea_Produccion.ToList();

            if (id > 0)
            {
                var searchProduccion = Objeto._context.Linea_Produccion.Where(al => al.id.Equals(id)).ToList();

                ListProduccion.Add(new SelectListItem
                {
                    Value = searchProduccion[0].id.ToString(),
                    Text = searchProduccion[0].nombre,
                });

                fillAlimento.ForEach(item =>
                {
                    if (item.id != id)
                    {
                        ListProduccion.Add(new SelectListItem
                        {
                            Value = item.id.ToString(),
                            Text = item.nombre
                        });
                    }

                });
            }
            else
            {
                foreach (var item in fillAlimento)
                {

                    ListProduccion.Add(new SelectListItem
                    {

                        Value = item.id.ToString(),
                        Text = item.nombre
                    });

                }

            }
            return ListProduccion;


        }

        private List<SelectListItem> getCorrales(int id = 0)
        {
            List<SelectListItem> ListCorral = new List<SelectListItem>();

            var fillRaza = Objeto._context.Corrals.ToList();


            if (id > 0)
            {

                var searchCorral = Objeto._context.Corrals.Find(id);

                ListCorral.Add(new SelectListItem
                {
                    Value = searchCorral.ID.ToString(),
                    Text = searchCorral.numeroCorral,
                });

                fillRaza.ForEach(item =>
                {
                    if (item.ID != id)
                    {
                        ListCorral.Add(new SelectListItem
                        {
                            Value = item.ID.ToString(),
                            Text = item.numeroCorral

                        });
                    }
                });
            }
            else
            {


                fillRaza = Objeto._context.Corrals.ToList();

                foreach (var item in fillRaza)
                {

                    ListCorral.Add(new SelectListItem
                    {

                        Value = item.ID.ToString(),
                        Text = item.numeroCorral
                    });

                }
            }

            return ListCorral;

        }


        private List<SelectListItem> getSexo(string sex = null)
        {
            List<SelectListItem> ListSex = new List<SelectListItem>();


            if (sex != null)
            {
                if (sex.ToLower().Equals("m"))
                {

                    ListSex.Add(new SelectListItem
                    {
                        Value = "1",
                        Text = sex.ToUpper()
                    });
                }
                else
                {
                    ListSex.Add(new SelectListItem
                    {
                        Value = "1",
                        Text = sex.ToUpper()
                    });

                }

                for (int i = 0; i < sexoList.Length; i++)
                {
                    if (!sexoList[i].ToLower().Equals(sex.ToLower()))
                    {
                        ListSex.Add(new SelectListItem
                        {
                            Value = i.ToString(),
                            Text = sexoList[i].ToUpper()
                        });
                    }
                }
            }
            else
            {

                ListSex.Add(new SelectListItem
                {
                    Value = "1",
                    Text = "M"

                });

                ListSex.Add(new SelectListItem
                {
                    Value = "2",
                    Text = "H"

                });
            }
            return ListSex;

        }

        private List<SelectListItem> getEstado(int id = 0)
        {
            List<SelectListItem> ListEstado = new List<SelectListItem>();


            var fillEstado = Objeto._context.Estados.ToList();

            if (id > 0)
            {
                var searchEstados = Objeto._context.Estados.Where(al => al.ID.Equals(id)).ToList();

                ListEstado.Add(new SelectListItem
                {
                    Value = searchEstados[0].ID.ToString(),
                    Text = searchEstados[0].Nombre,
                });

                fillEstado.ForEach(item =>
                {
                    if (item.ID != id)
                    {
                        ListEstado.Add(new SelectListItem
                        {
                            Value = item.ID.ToString(),
                            Text = item.Nombre
                        });
                    }

                });
            }
            else
            {
                foreach (var item in fillEstado)
                {

                    ListEstado.Add(new SelectListItem
                    {

                        Value = item.ID.ToString(),
                        Text = item.Nombre
                    });

                }
            }


                return ListEstado;

        }


        

        private List<SelectListItem> getCastrado(string castrado = null)
        {
            List<SelectListItem> ListCastrado = new List<SelectListItem>();

            if (castrado != null)
            {
                if (castrado.Equals("Si"))
                {

                    ListCastrado.Add(new SelectListItem
                    {
                        Value = "0",
                        Text = castrado
                    });
                }
                else
                {

                    ListCastrado.Add(new SelectListItem
                    {
                        Value = "0",
                        Text = castrado
                    });
                }

                for (int i = 0; i < castradoList.Length; i++)
                {
                    if (!castradoList[i].Equals(castrado))
                    {
                        var data = castradoList[i];

                        ListCastrado.Add(new SelectListItem
                        {
                            Value = i.ToString(),
                            Text = data
                        });
                    }
                }
            }
            else
            {

                ListCastrado.Add(new SelectListItem
                {
                    Value = "0",
                    Text = "Si"

                });

                ListCastrado.Add(new SelectListItem
                {
                    Value = "1",
                    Text = "No"

                });

            }

            return ListCastrado;

        }


    }
}