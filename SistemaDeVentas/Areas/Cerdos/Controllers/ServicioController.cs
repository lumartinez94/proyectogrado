﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SistemaDeVentas.Areas.Cerdos.Models;
using SistemaDeVentas.Controllers;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Cerdos.Controllers
{

    [Area("Cerdos")]
    [Authorize]
    public class ServicioController : Controller
    {

       
        private ListObject Objeto = new ListObject();


        public ServicioController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context)
        {
            Objeto._signInManager = signInManager;
            Objeto._usuarios = new Usuarios(userManager, signInManager, roleManager, context);
            Objeto._procCerdos = new ProcCerdos(userManager, signInManager, roleManager, context);
        }
        public async Task<IActionResult> Servicio(int id, String Search)
        {
            if (Objeto._signInManager.IsSignedIn(User))
            {
                var url = Request.Scheme + "://" + Request.Host.Value;
                var objects = new Paginador<InputModelsServicio>().paginador(await Objeto._procCerdos.getServicioAsync(Search),
                    id, "Cerdos", "Servicio", "Servicio", url);
                var models = new DataPaginador<InputModelsServicio>
                {
                    List = (List<InputModelsServicio>)objects[2],
                    Pagi_info = (String)objects[0],
                    Pagi_navegacion = (String)objects[1],
                    Input = new InputModelsServicio()

                };


                return View(models);
            }
            else
            {

                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }
    }
}