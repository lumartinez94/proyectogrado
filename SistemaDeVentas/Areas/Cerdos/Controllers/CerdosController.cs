﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SistemaDeVentas.Areas.Cerdos.DaoCerdos;
using SistemaDeVentas.Areas.Cerdos.Models;
using SistemaDeVentas.Areas.Users.Models;
using SistemaDeVentas.Controllers;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Cerdos.Controllers
{
    [Area("Cerdos")]
    [Authorize]
    public class CerdosController : Controller
    {

        private ListObject Objeto = new ListObject();


        public CerdosController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context)
        {
            Objeto._signInManager = signInManager;
            Objeto._context = context;
            Objeto._usuarios = new Usuarios(userManager, signInManager, roleManager, context);
            Objeto._procCerdos = new ProcCerdos(userManager, signInManager, roleManager, context);
        }
        public async Task<IActionResult> Index(int id, String Search)
        {
            if (Objeto._signInManager.IsSignedIn(User))
            {
                var url = Request.Scheme + "://" + Request.Host.Value;
                var objects = new Paginador<InputModelsCerdos>().paginador(await Objeto._procCerdos.getCerdosAsync(Search),
                    id, "Cerdos", "Cerdos", "Index", url);
                var models = new DataPaginador<InputModelsCerdos>
                {
                    List = (List<InputModelsCerdos>)objects[2],
                    Pagi_info = (String)objects[0],
                    Pagi_navegacion = (String)objects[1],
                    Input = new InputModelsCerdos()

                };


                return View(models);

            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }




        public async Task<IActionResult> Razas(int id, String Search)
        {
            if (Objeto._signInManager.IsSignedIn(User))
            {
                var url = Request.Scheme + "://" + Request.Host.Value;
                var objects = new Paginador<InputModelsCerdos>().paginador(await Objeto._procCerdos.getRazasAsync(Search),
                    id, "Cerdos", "Cerdos", "Razas", url);
                var models = new DataPaginador<InputModelsCerdos>
                {
                    List = (List<InputModelsCerdos>)objects[2],
                    Pagi_info = (String)objects[0],
                    Pagi_navegacion = (String)objects[1],
                    Input = new InputModelsCerdos()

                };


                return View("~/Areas/Cerdos/Views/Razas/Razas.cshtml", models);
            }
            else
            {

                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }


        public IActionResult AddServicio(int id)
        {


            var Service = new ServiceModel(Objeto._context);
            ViewBag.Result = "";



            return PartialView("~/Areas/Cerdos/Views/AddServicio/AddServicio.cshtml", Service.GetDatos(id));



        }


        public IActionResult PeticionServicio(String codigo, string caravana_hembra, String fecha, String servicioExitoso, String observacion)
        {

            var Service = new ServiceModel(Objeto._context);
            var user = HttpContext.User.Identity.Name;
            var result = Service.Save(codigo, caravana_hembra, fecha, servicioExitoso, observacion, user);


            if (result)
            {

                ViewBag.Result = "ok";

            }
            else
            {

                ViewBag.Result = "error";


            }



            return RedirectToAction("Index", "Cerdos");


        }


        public IActionResult addParto(string id, string fecha_nacimiento, string numero_hembra, string numero_macho, int nacidos_muertos, string fecha_destete, string peso_promedio, string observacion)
        {
            var parto = new CerdapartoModel(Objeto._context);

            if (fecha_nacimiento != null)
            {
                var user = HttpContext.User.Identity.Name;
                var result = parto.Save(id, fecha_nacimiento, numero_hembra, numero_macho, nacidos_muertos, fecha_destete, peso_promedio, observacion, user);

                if (result)
                {

                    return RedirectToAction("Index", "cerdos");
                }


            }




            return PartialView("~/Areas/Cerdos/Views/addParto/addParto.cshtml", parto.getParto(id));


        }



        public IActionResult addGestacion(string id, string fecha_inicio)
        {
            var gestacion = new CerdaGestacionModel(Objeto._context);


            if (fecha_inicio != null)
            {
                var user = HttpContext.User.Identity.Name;
                var result = gestacion.Save(id, fecha_inicio, user);

                if (result)
                {

                    return RedirectToAction("Index", "cerdos");
                }


            }




            return PartialView("~/Areas/Cerdos/Views/addGestacion/addGestacion.cshtml", gestacion.getParto(id));


        }



        public IActionResult cerdoInformacion(string id)
        {
            var model = new CerdoInformationAll(Objeto._context);
   

            return PartialView("~/Areas/Cerdos/Views/cerdoInformacion/cerdoInformacion.cshtml",model.getModel(id));
        }


        public IActionResult addTraslado(string id)
        {
            var traslado = new CerdoTrasladoModel(Objeto._context);





            return PartialView("~/Areas/Cerdos/Views/addTraslado/addTraslado.cshtml", traslado.getTraslado(id));


        }

        [HttpPost]
        public IActionResult saveTraslado(string id, string corral, string causas)
        {


            var traslado = new CerdoTrasladoModel(Objeto._context);


            var user = HttpContext.User.Identity.Name;
            var result = traslado.Save(id, corral, causas, user);

             if (result)
            {


                return RedirectToAction("Index", "Cerdos");

            }
            else
            {

                return Json(string.Format("'Success':'false'"));
            }






        }


        public IActionResult addMedicamento(string id)
        {
            var cerdo_medicamento = new CerdoMedicamentoModels(Objeto._context);


            return PartialView("~/Areas/Cerdos/Views/addMedicamento/addMedicamento.cshtml", cerdo_medicamento.getModel(id));


        }


        [HttpPost]
        public JsonResult saveMedicamento(string id, string medicamento,string dosis,string fecha,string observacion)
        {


            var cerdo_medicamento = new CerdoMedicamentoModels(Objeto._context);


            var user = HttpContext.User.Identity.Name;
            var result = cerdo_medicamento.Save(id,medicamento,dosis,fecha,observacion,user);

            if (result)
            {


                return Json("'Success:'true'");

            }
            else
            {

                return Json(string.Format("'Success':'false'"));
            }






        }

    }


}