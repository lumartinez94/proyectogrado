﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SistemaDeVentas.Areas.Cerdos.Models;
using SistemaDeVentas.Controllers;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Cerdos.Controllers
{

    [Area("Cerdos")]
    [Authorize]
    public class GestacionController : Controller
    {

        private ListObject Objeto = new ListObject();


        public GestacionController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context)
        {
            Objeto._signInManager = signInManager;
            Objeto._usuarios = new Usuarios(userManager, signInManager, roleManager, context);
            Objeto._procCerdos = new ProcCerdos(userManager, signInManager, roleManager, context);
        }
        public async Task<IActionResult> Gestacion(int id, String Search)
        {
            if (Objeto._signInManager.IsSignedIn(User))
            {
                var url = Request.Scheme + "://" + Request.Host.Value;
                var objects = new Paginador<InputModelsGestacion>().paginador(await Objeto._procCerdos.getGestacionAsync(Search),
                    id, "Cerdos", "Gestacion", "Gestacion", url);
                var models = new DataPaginador<InputModelsGestacion>
                {
                    List = (List<InputModelsGestacion>)objects[2],
                    Pagi_info = (String)objects[0],
                    Pagi_navegacion = (String)objects[1],
                    Input = new InputModelsGestacion()

                };


                return View(models);
            }
            else
            {

                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }
    }
}