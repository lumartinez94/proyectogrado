﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.Cerdos.Models;
using SistemaDeVentas.Areas.ControlMedicamento.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Cerdos.DaoCerdos
{
    public class CerdoInformationAll
    {

        private ListObject Objeto = new ListObject();




        public CerdoInformationAll(ApplicationDbContext context)
        {

            Objeto._usuarios = new Usuarios();
            Objeto._context = context;
            Objeto._usersRoles = new UserRoles();
            Objeto._image = new UploadImage();
            Objeto._userRoles = new List<SelectListItem>();

        }



        [BindProperty]

        public InputModel input { get; set; }
        public class InputModel : InputModelsAll
        {




        }



        public InputModelsAll getModel(string id)
        {

            try
            {
                var cerdo = cerdoModel(int.Parse(id));
                var medicamento = cerdoMedicamento(cerdo.ID);
                var traslado = cerdoTraslado(cerdo.ID);
                int totalHembras = 0;
                double nExitos = 0;
                double value = 100;
                double total = 0;
                double porcentajeExito = 0;
                double porcentajeFallido = 0;
                string firstService="";
                string lastService="";

                double totalParto = 0;
                double totalMacho = 0;
                double totalHembra = 0;
                double totalMuerto = 0;
                var lasParto = "";
                var firsParto = "";
                double totalVivos = 0;
                double mortandad = 0;
                double totalCerdo = 0;
                double porcentajeMacho = 0;
                double porcentajeHembra = 0;
                List<Gestacion> gestacion= new List<Gestacion>();
                int pendiente = 0;
                List<Gestacion> fechaprx = new List<Gestacion>();



                if (cerdo.Sexo.Equals("M"))
                {


                    if (cerdo.nombre_produccion.Equals("padrote"))
                    {


                        var servicio = Objeto._context.servicio.Where(u => u.id_cerdoMacho.Equals(cerdo.ID)).ToList();

                        if (servicio.Count() > 0)
                        {
                            totalHembras = servicio.Select(m => m.id_cerdoHembra).Distinct().Count();
                            nExitos = Objeto._context.servicio.Where(u => u.id_cerdoMacho.Equals(cerdo.ID)).Where(u => u.servicio_exitoso.Equals("Si")).Count();

                            double nFallidos = Objeto._context.servicio.Where(u => u.id_cerdoMacho.Equals(cerdo.ID)).Where(u => u.servicio_exitoso.Equals("No")).Count();

                            value = 100;
                            total = servicio.Count() > 0 ? servicio.Count() : 0;
                            porcentajeExito = (nExitos / total) * value;
                            porcentajeExito = porcentajeExito > 0 ? porcentajeExito : 0;
                            porcentajeFallido = (nFallidos / total) * value;
                            porcentajeFallido = porcentajeFallido > 0 ? porcentajeFallido : 0;
                            firstService = servicio[0].fecha.ToString().Replace("12:00:00 AM", "");
                            lastService = servicio[(int)total - 1].fecha.ToString().Replace("12:00:00 AM", "");
                        }


                        input = new InputModel
                        {

                            cerdos = cerdo,
                            totalServicio = total.ToString().Length > 0 ? total.ToString() : "N/a",
                            porcentajeExito = porcentajeExito.ToString().Length > 0 ? porcentajeExito.ToString() : "N/a",
                            porcentajeFallido = porcentajeFallido.ToString().Length > 0 ? porcentajeFallido.ToString() : "N/a",
                            firstService = firstService.ToString().Length > 0 ? firstService.ToString() : "N/a",
                            lastService = lastService.ToString().Length > 0 ? lastService.ToString() : "N/a",
                            cerdasTotales = totalHembras.ToString().Length > 0 ? totalHembras.ToString() : "N/a",
                            medicamentos = medicamento,
                            traslado = traslado,



                        };


                    }
                    else
                    {


                        input = new InputModel
                        {

                            cerdos = cerdo,
                            medicamentos = medicamento,
                            traslado = traslado,



                        };
                    }





                }
                else
                {
                    var parto = "";
                    var inicio = "";

                    if (cerdo.nombre_produccion.Equals("reproductora"))
                    {
                        var Parto = Objeto._context.parto.Where(u => u.id_cerdaMadre.Equals(cerdo.ID)).ToList();
                        if (Parto.Count() > 0)
                        {
                            totalParto = Parto.Count();
                            totalMacho = Parto.Sum(u => u.numero_macho);
                            totalHembra = Parto.Sum(u => u.numero_hembra);
                            totalMuerto = Parto.Sum(u => u.nacidos_muertos);
                            lasParto = Parto.Select(u => u.fecha_nacimiento).Last().ToString();
                            firsParto = Parto.Select(u => u.fecha_nacimiento).First().ToString();
                            double data = 100;
                            totalVivos = (totalHembra + totalMacho) - totalMuerto;
                            mortandad = (totalMuerto / totalVivos) * data;
                            totalCerdo = totalHembra + totalMacho;
                            porcentajeMacho = (totalMacho / totalCerdo) * data;
                            porcentajeHembra = (totalHembra / totalCerdo) * data;
                            gestacion = Objeto._context.gestacion.Where(u => u.id_cerdo.Equals(cerdo.ID)).ToList();
                            pendiente = gestacion.Where(u => u.estado.Equals("En proceso")).Distinct().Count();
                            fechaprx = gestacion.Where(u => u.estado.Equals("En proceso")).ToList();


                            if (fechaprx.Count() > 0)
                            {
                                parto = fechaprx[0].fecha_aproximada.ToString().Replace("12:00:00 AM", ""); ;
                                inicio = fechaprx[0].fecha_inicio.ToString().Replace("12:00:00 AM", ""); ;

                            }
                            total = gestacion.Count();

                        }



                        input = new InputModel
                        {

                            cerdos = cerdo,
                            medicamentos = medicamento,
                            traslado = traslado,
                            mortandad = mortandad > 0 ? mortandad.ToString() : "0",
                            porcentajeHembra = porcentajeHembra > 0 ? porcentajeHembra.ToString() : "0",
                            porcentajeHombre = porcentajeMacho > 0 ? porcentajeMacho.ToString() : "0",
                            firstParto = firsParto.ToString().Length > 0 ? firsParto.ToString().Replace("12:00:00 AM", "") : "N/a",
                            lastParto = lasParto.ToString().Length > 0 ? lasParto.ToString().Replace("12:00:00 AM", "") : "N/a",
                            totalLechones = totalCerdo > 0 ? totalCerdo.ToString() : "0",
                            totalParto = totalParto > 0 ? totalParto.ToString() : "0",
                            pendienteGestacion = pendiente > 0 ? pendiente.ToString() : "0",
                            fechaAproximada = parto.ToString().Length > 0 ? parto.ToString() : "N/a",
                            totalgestacion = total > 0 ? total.ToString() : "0",
                            fechaReal = inicio.ToString().Length > 0 ? inicio.ToString() : "N/a"






                        };




                    }
                    else
                    {

                        input = new InputModel
                        {

                            cerdos = cerdo,
                            medicamentos = medicamento,
                            traslado = traslado,




                        };
                    }

                }



            }
            catch (Exception ex)
            {

                input = new InputModel
                {
                    ErrorMessage = ex.Message,


                };
            }

            return input;
        }

        public InputModelsCerdos cerdoModel(int id)
        {
            InputModelsCerdos model = null;
            try
            {
                var cerdo = Objeto._context.Cerdos.Find(id);
                var corral = Objeto._context.Corrals.Find(cerdo.id_corral);
                var alimento = Objeto._context.tipo_Alimentacion.Find(cerdo.id_alimentacion);
                var produccion = Objeto._context.Linea_Produccion.Find(cerdo.id_lineaProduccion);
                var estado = Objeto._context.Estados.Find(cerdo.id_estado);
                var raza = Objeto._context.Razas.Find(cerdo.id_raza);


                model = new InputModelsCerdos
                {
                    ID = cerdo.ID,
                    codigo = cerdo.codigo,
                    Peso = cerdo.Peso,
                    numeroCorral = corral.numeroCorral,
                    Sexo = cerdo.Sexo,
                    castrado = cerdo.castrado,
                    numero_tetas = cerdo.numero_tetas,
                    fecha_destete = cerdo.fecha_destete,
                    status = estado.Nombre,
                    fecha_nacimiento = cerdo.fecha_nacimiento,
                    fecha_registro = cerdo.fecha_registro,
                    nombre_produccion = produccion.nombre,
                    NombreRaza = raza.Nombre,
                    Nombre_alimentacion = alimento.nombre



                };




            }
            catch (Exception)
            {

                throw;
            }



            return model;

        }


        public List<InputModelsCerdosMedicamentos> cerdoMedicamento(int id)
        {
            List<InputModelsCerdosMedicamentos> model = new List<InputModelsCerdosMedicamentos>();
            try
            {




                var list = Objeto._context.cerdo_Medicamento.Join(
                    Objeto._context.medicamento,
                    medic => medic.id_medicamento,
                    m => m.ID,
                    (medic, m) => new
                    {
                        m,
                        medic

                    }).Where(u => u.medic.id_cerdo.Equals(id)).ToList();


                foreach (var item in list)
                {

                    model.Add(


                     new InputModelsCerdosMedicamentos
                     {

                         id_medicamento = item.m.ID.ToString(),
                         nombre_medicamento = item.m.Nombre,
                         date =  item.medic.fecha.ToString().Replace("12:00:00 AM", ""),
                         cantidad_suministrada = item.medic.cantidad_suministrada


                     }

                    );










                }



            }
            catch (Exception ex)
            {

                model.Add(new InputModelsCerdosMedicamentos
                {

                    ErrorMessage = "Ha ocurrido un error",
                });
            }


            return model;
        }


        public InputModelsTraslado cerdoTraslado(int id)
        {
            InputModelsTraslado model = null;
            try
            {

                var traslado = Objeto._context.traslado.Join(
                    Objeto._context.Corrals,
                    trasla => trasla.id_corral,
                    corral => corral.ID,
                    (trasla, corral) => new
                    {

                        trasla,
                        corral
                    }
                    ).Where(u => u.trasla.id_cerdo.Equals(id)).ToList();

                if (traslado.Count() > 0)
                {

                    model = new InputModelsTraslado
                    {
                        id_corral = traslado[traslado.Count() - 1].corral.numeroCorral,
                        causas = traslado[traslado.Count() - 1].trasla.causas,
                        fecha = traslado[traslado.Count() - 1].trasla.fecha.ToString(),




                    };
                }
                else
                {
                    model = new InputModelsTraslado
                    {
                        ErrorMessage = "Ha ocurrido un error"
                    };

                }





            }
            catch (Exception ex)
            {

                model = new InputModelsTraslado
                {
                    ErrorMessage = "Ha ocurrido un error"
                };
            }


            return model;
        }

    }
}
