﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.Cerdos.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Cerdos.DaoCerdos
{
    public class CerdoTrasladoModel
    {


        private ListObject Objeto = new ListObject();
        private static List<Cerdo> cerdosList;



        public CerdoTrasladoModel(ApplicationDbContext context)
        {

            Objeto._usuarios = new Usuarios();
            Objeto._context = context;
            Objeto._usersRoles = new UserRoles();
            Objeto._image = new UploadImage();
            Objeto._userRoles = new List<SelectListItem>();

        }



        [BindProperty]

        public InputModel input { get; set; }
        public class InputModel : InputModelsTraslado
        {

        


        }



        public InputModelsTraslado getTraslado(string id)
        {

            try
            {
                var cerdo = Objeto._context.Cerdos.Where(u => u.ID.Equals(int.Parse(id)) || u.codigo.Equals(id)).ToList();
                var corral = Objeto._context.Corrals.Where(u => u.ID.Equals(cerdo[0].id_corral)).ToList();
                input = new InputModel
                {



                   codigo_cerdo= cerdo[0].codigo,
                   id_corral=corral[0].numeroCorral,
                   corrales= getCorrales(cerdo[0].id_corral),
                   


                };
            }
            catch (Exception ex)
            {

                input = new InputModel
                {
                    ErrorMessage = ex.Message,


                };
            }

            return input;
        }


        public bool Save(string id, string corral, string causas, string user)
        {
            Boolean result = false;
            try
            {
                cerdosList = Objeto._context.Cerdos.Where(u => u.codigo.Equals(id)).ToList();
                var userlist = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].Id)).ToList();



                var traslado = new Traslado
                {

                    id_cerdo = cerdosList[0].ID,
                    id_corral =int.Parse(corral),
                    causas=causas,
                    fecha = DateTime.Now,
                    


                };


                var log_aplicacion = new Historial_Acciones
                {
                    IdRole = userList5[0].RoleId,
                    Accion = "Guardar",
                    item = "Traslado",
                    fecha_registro = DateTime.Now,
                    IdUser = userlist[0].Id,
                };


                Objeto._context.Add(traslado);
                Objeto._context.Add(log_aplicacion);
                Objeto._context.SaveChanges();
               
                updateCerdo(id, corral);
            


                result = true;


            }
            catch (Exception ex)
            {
                result = false;

            }

            return result;


        }
        public void updateCerdo(string id,string corral)
        {

            try
            {


                var cerdos = Objeto._context.Cerdos.First(g => g.ID==cerdosList[0].ID);
                var cerdoss = new Cerdo
                {
                    ID = cerdosList[0].ID,
                    codigo = cerdosList[0].codigo,
                    castrado = cerdosList[0].castrado,
                    Sexo = cerdosList[0].Sexo,
                    id_raza = cerdosList[0].id_raza,
                    id_alimentacion = cerdosList[0].id_alimentacion,
                    id_lineaProduccion = cerdosList[0].id_lineaProduccion,
                    fecha_destete = cerdosList[0].fecha_destete,
                    fecha_nacimiento = cerdosList[0].fecha_nacimiento,
                    id_estado = cerdosList[0].id_estado,
                    observaciones = cerdosList[0].observaciones,
                    fecha_registro = cerdosList[0].fecha_registro,
                    id_corral = int.Parse(corral),
                    Peso = cerdosList[0].Peso,
                    id_user = cerdosList[0].id_user,
                    numero_tetas = cerdosList[0].numero_tetas





                };
            
                Objeto._context.Entry(cerdos).CurrentValues.SetValues(cerdoss);
                Objeto._context.Update(cerdos);

                Objeto._context.SaveChanges();

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private List<SelectListItem> getCorrales(int id = 0)
        {
            List<SelectListItem> ListCorral = new List<SelectListItem>();

            var fillRaza = Objeto._context.Corrals.ToList();
         

            if (id > 0)
            {
                var searchCorrales = Objeto._context.Corrals.Find(id);

                ListCorral.Add(new SelectListItem
                {
                    Value = searchCorrales.ID.ToString(),
                    Text = searchCorrales.numeroCorral
                });

                fillRaza.ForEach(item =>
                {
                    if (item.ID != id)
                    {
                        ListCorral.Add(new SelectListItem
                        {
                            Value = item.ID.ToString(),
                            Text = item.numeroCorral

                        });
                    }
                });
            }
            else
            {


                fillRaza = Objeto._context.Corrals.ToList();

                foreach (var item in fillRaza)
                {

                    ListCorral.Add(new SelectListItem
                    {

                        Value = item.ID.ToString(),
                        Text = item.numeroCorral
                    });

                }
            }

            return ListCorral;

        }
    }
}

