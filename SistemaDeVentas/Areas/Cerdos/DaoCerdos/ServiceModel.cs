﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.Cerdos.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Cerdos.DaoCerdos
{
    public class ServiceModel
    {
        private ListObject Objeto = new ListObject();
        private static String idGet = null;
        private static List<servicio> listServicio;
        private static String user = null;
        private static string[] Servicio_exitoso = { "Si", "No" };




        public ServiceModel(ApplicationDbContext context)
        {

            Objeto._usuarios = new Usuarios();
            Objeto._context = context;
            Objeto._usersRoles = new UserRoles();
            Objeto._image = new UploadImage();
            Objeto._userRoles = new List<SelectListItem>();

        }

        public ServiceModel()
        {
        }

        [BindProperty]

        public InputModel input { get; set; }
        public class InputModel : InputModelsServicio
        {


            [TempData]

            public string ErrorMessage { get; set; }





        }

        public bool Save(string codeMale, string codeFemale, string fecha, string service, string observacion, string user)
        {
            Boolean result = false;
            try
            {
                var id_male = Objeto._context.Cerdos.Where(u => u.codigo.Equals(codeMale)).ToList();
                var userlist = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].Id)).ToList();



                var services = new servicio
                {
                    id_cerdoMacho = id_male[0].ID,
                    fecha = DateTime.Parse(fecha),
                    id_cerdoHembra = int.Parse(codeFemale),
                    Observacion = observacion,
                    servicio_exitoso = service,



                };


                var log_aplicacion = new Historial_Acciones
                {
                    IdRole = userList5[0].RoleId,
                    Accion = "Guardar",
                    item = "Servicio",
                    fecha_registro = DateTime.Now,
                    IdUser = userlist[0].Id,
                };


                Objeto._context.Add(services);
                Objeto._context.Add(log_aplicacion);
                Objeto._context.SaveChanges();


                result = true;


            }
            catch (Exception ex)
            {
                result = false;

            }

            return result;


        }

        public InputModelsServicio GetDatos(int codigo)
        {
            InputModelsServicio data = null;


            try
            {
                data = new InputModel
                {
                    caravana_macho = getCode(codigo),
                    listaHembras = getHembras(),
                    servicioExitoso = getServicio()

                };


            }
            catch (Exception ex)
            {

                data = new InputModel
                {
                    ErrorMessage = ex.Message,
                    listaHembras = getHembras(),
                    caravana_macho = getCode(codigo),
                    servicioExitoso = getServicio(),




                };
            }
            return data;

        }

        public string getCode(int code)
        {
            string result = "";
            try
            {

                var pork = Objeto._context.Cerdos.Find(code);
                result = pork.codigo;

            }
            catch (Exception ex)
            {

                result = ex.Message;
            }

            return result;


        }


        public List<SelectListItem> getHembras(string data = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var hembras = Objeto._context.Cerdos.Where(c => c.Sexo.Equals("H") && c.id_lineaProduccion.Equals(2) && (c.id_estado.Equals(1) || c.id_estado==5)).ToList();


            if (data != null)
            {

                var busqueda = Objeto._context.Cerdos.Find(int.Parse(data));

                list.Add(new SelectListItem
                {
                    Text = busqueda.codigo,
                    Value = busqueda.ID.ToString()
                });

                hembras.ForEach(item =>
                {

                    if (!item.ID.ToString().Equals(data))
                    {
                        list.Add(new SelectListItem
                        {
                            Value = item.ID.ToString(),
                            Text = item.codigo

                        });

                    }
                });




            }
            else
            {
                hembras.ForEach(item =>
                {
                    list.Add(new SelectListItem
                    {

                        Text = item.codigo,
                        Value = item.ID.ToString()
                    }

                    );

                });
            }


            return list;
        }

        private List<SelectListItem> getServicio(string data = null)
        {
            List<SelectListItem> Listdata = new List<SelectListItem>();

            if (data != null)
            {
                if (data.Equals("Si"))
                {

                    Listdata.Add(new SelectListItem
                    {
                        Value = "0",
                        Text = data
                    });
                }
                else
                {
                    Listdata.Add(new SelectListItem
                    {
                        Value = "0",
                        Text = data
                    });
                }

                for (int i = 0; i < Servicio_exitoso.Length; i++)
                {
                    if (!Servicio_exitoso[i].Equals(data))
                    {
                        var value = Servicio_exitoso[i];

                        Listdata.Add(new SelectListItem
                        {
                            Value = i.ToString(),
                            Text = value
                        });
                    }
                }
            }
            else
            {

                Listdata.Add(new SelectListItem
                {
                    Value = "0",
                    Text = "Si"

                });

                Listdata.Add(new SelectListItem
                {
                    Value = "1",
                    Text = "No"

                });

            }

            return Listdata;

        }





    }



}
