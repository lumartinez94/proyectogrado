﻿using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.Cerdos.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Cerdos.DaoCerdos
{
    public class DaoPoblacion
    {


        private SqlConnection con;
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["GanadoPorcino"].ToString();
            con = new SqlConnection(constring);
        }


        public poblacion getPoblation(string f1,string f2)
        {

            poblacion model = null;
            try
            {
                model = new poblacion
                {

                    lactancia = lactancia(f1, f2),
                    gestante = gestacion(f1, f2),
                    lechones = lechones(f1, f2),
                    levante = levante(f1, f2),
                    inicio = inicio(f1, f2),
                    poblacionTotal = (int.Parse(lechones(f1, f2)) + int.Parse(cerdos(f1, f2))).ToString(),
                    cuchilla = cuchilla(f1, f2),
                    padrote = padrote(f1, f2),
                    reproductora = reproductora(f1, f2),


                };

                


            }
            catch (Exception ex)
            {

                throw ex;
            }



            return model;

        }
        public string cuchilla(string f1, string f2)
        {
            string result = "";
            if(f1!=null && f2!=null){


                connection();
                SqlCommand cmd = new SqlCommand("select count(*) cuchilla from Cerdos  c inner join  Estados on estados.ID= c.id_estado inner join linea_Produccion l on c.id_lineaProduccion= l.id where  estados.Nombre='En granja' and l.nombre='cuchilla' and  (cast (fecha_registro as date) >=  '" + f1 + "'  and cast(fecha_registro as date) <= '" + f2 + "' )", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {

                    result = dr["cuchilla"].ToString() != "" ? dr["cuchilla"].ToString() : "0";


                }
            }
            else
            {

                connection();
                SqlCommand cmd = new SqlCommand("select count(*) cuchilla from Cerdos  c inner join  Estados on estados.ID= c.id_estado inner join linea_Produccion l on c.id_lineaProduccion= l.id where  estados.Nombre='En granja' and l.nombre='cuchilla' and fecha_registro<=GETDATE()", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {

                    result = dr["cuchilla"].ToString() != "" ? dr["cuchilla"].ToString() : "0";


                }



            }
           

            return result;

        }

        public string reproductora(string f1, string f2)
        {
            string result = "";
            if(f1!=null && f2!=null){

                connection();
                SqlCommand cmd = new SqlCommand("select count(*) reproductora from Cerdos  c inner join  Estados on estados.ID= c.id_estado inner join linea_Produccion l on c.id_lineaProduccion= l.id where  estados.Nombre='En granja' and l.nombre='reproductora' and  (cast (fecha_registro as date) >=  '" + f1 + "'  and cast(fecha_registro as date) <= '" + f2 + "' )", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {

                    result = dr["reproductora"].ToString() != "" ? dr["reproductora"].ToString() : "0";


                }
            }
            else
            {

                connection();
                SqlCommand cmd = new SqlCommand("select count(*) reproductora from Cerdos  c inner join  Estados on estados.ID= c.id_estado inner join linea_Produccion l on c.id_lineaProduccion= l.id where  estados.Nombre='En granja' and l.nombre='reproductora' and  fecha_registro <=GETDATE()", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {

                    result = dr["reproductora"].ToString() != "" ? dr["reproductora"].ToString() : "0";


                }
            }
            

            return result;

        }

        public string padrote (string f1, string f2)
        {
            string result = "";
            if(f1!=null && f2!=null){

                connection();
                SqlCommand cmd = new SqlCommand("select count(*) padrote from Cerdos  c inner join  Estados on estados.ID= c.id_estado inner join linea_Produccion l on c.id_lineaProduccion= l.id where  estados.Nombre='En granja' and l.nombre='padrote' and  (cast (fecha_registro as date) >=  '" + f1 + "'  and cast(fecha_registro as date) <= '" + f2 + "' )", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {

                    result = dr["padrote"].ToString() != "" ? dr["padrote"].ToString() : "0";


                }
            }
            else
            {
                connection();
                SqlCommand cmd = new SqlCommand("select count(*) padrote from Cerdos  c inner join  Estados on estados.ID= c.id_estado inner join linea_Produccion l on c.id_lineaProduccion= l.id where  estados.Nombre='En granja' and l.nombre='padrote' and fecha_registro<=GETDATE() ", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {

                    result = dr["padrote"].ToString() != "" ? dr["padrote"].ToString() : "0";


                }


            }
           

            return result;

        }
        public string cerdos(string f1, string f2)
        {
            string result = "";
            if(f1!=null && f2!=null){
                connection();
                SqlCommand cmd = new SqlCommand("select count(*) cerdos from Cerdos c inner join  Estados on estados.ID= c.id_estado inner join linea_Produccion on linea_Produccion.id= c.id_lineaProduccion where estados.Nombre='En granja' and  (cast (fecha_registro as date) >=  '" + f1 + "'  and cast(fecha_registro as date) <= '" + f2 + "' )", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {

                    result = dr["cerdos"].ToString() != "" ? dr["cerdos"].ToString() : "0";


                }
            }
            else
            {
                connection();
                SqlCommand cmd = new SqlCommand("select count(*) cerdos from Cerdos c inner join  Estados on estados.ID= c.id_estado inner join linea_Produccion on linea_Produccion.id= c.id_lineaProduccion where estados.Nombre='En granja' and fecha_registro<=GETDATE() ", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {

                    result = dr["cerdos"].ToString() != "" ? dr["cerdos"].ToString() : "0";


                }


            }
           

            return result;

        }
        public string lechones(string f1, string f2)
        {
            string result = "";
            if(f1!=null && f2!=null){


                connection();
                SqlCommand cmd = new SqlCommand("select sum(numero_macho+numero_hembra)-sum(nacidos_muertos) as lechones  from Parto where fecha_nacimiento	 >=  '" + f1 + "'  and fecha_nacimiento	 <= '" + f2 + "'", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {

                    result = dr["lechones"].ToString() != "" ? dr["lechones"].ToString() : "0";


                }
            }
            else
            {
                connection();
                SqlCommand cmd = new SqlCommand("select sum(numero_macho+numero_hembra)-sum(nacidos_muertos) as lechones  from Parto where fecha_nacimiento	 <= cast(GETDATE() as date)", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {

                    result = dr["lechones"].ToString() != "" ? dr["lechones"].ToString() : "0";


                }

            }
            

            return result;

        }

        public string gestacion(string f1, string f2)
        {
            string result = "";
            if(f1!=null && f2!=null){


                connection();
                SqlCommand cmd = new SqlCommand("select count(*) as gestacion from tipo_Alimentacion t inner join cerdos on Cerdos.id_alimentacion = t.id inner join Estados e on e.ID = cerdos.id_estado where e.nombre = 'gestacion' and e.Nombre = 'En granja' and (cast (fecha_registro as date) >=  '" + f1 + "'  and cast(fecha_registro as date) <= '" + f2 + "' )", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {

                    result = dr["gestacion"].ToString() != "" ? dr["gestacion"].ToString() : "0";


                }

            }
            else
            {
                connection();
                SqlCommand cmd = new SqlCommand("select count(*) as gestacion from tipo_Alimentacion t inner join cerdos on Cerdos.id_alimentacion = t.id inner join Estados e on e.ID = cerdos.id_estado where e.nombre = 'gestacion' and e.Nombre = 'En granja' and fecha_registro <=GETDATE() ", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {

                    result = dr["gestacion"].ToString() != "" ? dr["gestacion"].ToString() : "0";


                }


            }
           

            return result;

        }


        public string lactancia(string f1,string f2)
        {
            string result = "";

            if (f1 != null && f2 != null)
            {
                connection();
                SqlCommand cmd = new SqlCommand("select count(*) as lactancia from tipo_Alimentacion t inner join cerdos on Cerdos.id_alimentacion = t.id inner join Estados e on e.ID = cerdos.id_estado where e.nombre = 'lactancia' and e.Nombre = 'En granja' and (cast (fecha_registro as date) >=  '" + f1 + "'  and cast(fecha_registro as date) <= '" + f2 + "' )", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {

                    result = dr["lactancia"].ToString() != "" ? dr["lactancia"].ToString() : "0";


                }

            }
            else
            {
                connection();
                SqlCommand cmd = new SqlCommand("select count(*) as lactancia from tipo_Alimentacion t inner join cerdos on Cerdos.id_alimentacion = t.id inner join Estados e on e.ID = cerdos.id_estado where e.nombre = 'lactancia' and e.Nombre = 'En granja' and fecha_registro<=GETDATE() ", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {

                    result = dr["lactancia"].ToString() != "" ? dr["lactancia"].ToString() : "0";


                }

            }

            return result;


        }

        public string levante(string f1, string f2)
        {
            string result = "";
            if(f1!=null && f2!=null){

                connection();
                SqlCommand cmd = new SqlCommand("select count(*) as levante from tipo_Alimentacion t inner join cerdos on Cerdos.id_alimentacion = t.id inner join Estados e on e.ID = cerdos.id_estado where e.nombre = 'levante' and e.Nombre = 'En granja' and  (cast (fecha_registro as date) >=  '" + f1 + "'  and cast(fecha_registro as date) <= '" + f2 + "' )", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {

                    result = dr["levante"].ToString() != "" ? dr["levante"].ToString() : "0";


                }

            }
            else
            {
                connection();
                SqlCommand cmd = new SqlCommand("select count(*) as levante from tipo_Alimentacion t inner join cerdos on Cerdos.id_alimentacion = t.id inner join Estados e on e.ID = cerdos.id_estado where e.nombre = 'levante' and e.Nombre = 'En granja' and fecha_registro<=GETDATE()", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {

                    result = dr["levante"].ToString() != "" ? dr["levante"].ToString() : "0";


                }

            }

            

            return result;


        }

        public string inicio(string f1, string f2)
        {
            string result = "";
            if (f1 != null && f2 != null)
            {
                connection();
                SqlCommand cmd = new SqlCommand("select count(*) as inicio from tipo_Alimentacion t inner join cerdos on Cerdos.id_alimentacion = t.id inner join Estados e on e.ID = cerdos.id_estado where e.nombre = 'inicio' and e.Nombre = 'En granja' and (cast (fecha_registro as date) >=  '" + f1 + "'  and cast(fecha_registro as date) <= '" + f2 + "' )", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {

                    result = dr["inicio"].ToString() != "" ? dr["inicio"].ToString() : "0";


                }
            }
            else
            {

                connection();
                SqlCommand cmd = new SqlCommand("select count(*) as inicio from tipo_Alimentacion t inner join cerdos on Cerdos.id_alimentacion = t.id inner join Estados e on e.ID = cerdos.id_estado where e.nombre = 'inicio' and e.Nombre = 'En granja' and fecha_registro <= GETDATE()", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {

                    result = dr["inicio"].ToString() != "" ? dr["inicio"].ToString() : "0";


                }
            }
           

            return result;


        }


    }
}
