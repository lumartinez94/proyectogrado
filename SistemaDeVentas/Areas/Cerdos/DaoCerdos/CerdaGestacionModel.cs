﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Areas.Cerdos.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SistemaDeVentas.Models;
using System;

namespace SistemaDeVentas.Areas.Cerdos.DaoCerdos
{
    public class CerdaGestacionModel
    {


        private ListObject Objeto = new ListObject();
     




        public CerdaGestacionModel(ApplicationDbContext context)
        {

            Objeto._usuarios = new Usuarios();
            Objeto._context = context;
            Objeto._usersRoles = new UserRoles();
            Objeto._image = new UploadImage();
            Objeto._userRoles = new List<SelectListItem>();

        }



        [BindProperty]

        public InputModel input { get; set; }
        public class InputModel : InputModelsGestacion

        {





        }



        public InputModelsGestacion getParto(string id)
        {

            try
            {
                var cerdo = Objeto._context.Cerdos.Where(u => u.ID.Equals(int.Parse(id)) || u.codigo.Equals(id)).ToList();
                input = new InputModel
                {



                    caravana_hembra = cerdo[0].codigo


                };
            }
            catch (Exception ex)
            {

                input = new InputModel
                {
                    ErrorMessage = ex.Message,

                };
            }

            return input;
        }


        public bool Save(string id, string fecha_inicio, string user)
        {
            Boolean result = false;
            try
            {
                var id_female = Objeto._context.Cerdos.Where(u => u.codigo.Equals(id) || u.ID.Equals(int.Parse(id))).ToList();
                var userlist = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].Id)).ToList();

                var fecha = DateTime.Parse(fecha_inicio);

                var fecha1 = fecha.AddMonths(3);
                var fecha_aproximada = fecha1.AddDays(24);


                var gestacion = new Gestacion
                {
                    fecha_inicio=DateTime.Parse(fecha_inicio),
                    fecha_aproximada=fecha_aproximada,
                    estado="En proceso",
                    id_cerdo=id_female[0].ID
                    


                };
              

                var log_aplicacion = new Historial_Acciones
                {
                    IdRole = userList5[0].RoleId,
                    Accion = "Guardar",
                    item = "Gestacion",
                    fecha_registro = DateTime.Now,
                    IdUser = userlist[0].Id,
                };


                Objeto._context.Add(gestacion);
                Objeto._context.Add(log_aplicacion);
                Objeto._context.SaveChanges();


                result = true;


            }
            catch (Exception ex)
            {
                result = false;

            }

            return result;


        }
    }
}
