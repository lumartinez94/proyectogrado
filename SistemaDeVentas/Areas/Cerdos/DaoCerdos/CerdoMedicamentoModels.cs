﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.ControlMedicamento.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Cerdos.DaoCerdos
{
    public class CerdoMedicamentoModels
    {

        private ListObject Objeto = new ListObject();





        public CerdoMedicamentoModels (ApplicationDbContext context)
        {

            Objeto._usuarios = new Usuarios();
            Objeto._context = context;
            Objeto._usersRoles = new UserRoles();
            Objeto._image = new UploadImage();
            Objeto._userRoles = new List<SelectListItem>();

        }



        [BindProperty]

        public InputModel input { get; set; }
        public class InputModel : InputModelsCerdosMedicamentos

        {





        }



        public InputModelsCerdosMedicamentos getModel(string id)
        {

            try
            {
                var cerdo = Objeto._context.Cerdos.Where(u => u.ID.Equals(int.Parse(id)) || u.codigo.Equals(id)).ToList();
                input = new InputModel
                {



                    id_cerdo = cerdo[0].codigo,
                    MedicamentoList = getMedicamentos(),


                };
            }
            catch (Exception ex)
            {

                input = new InputModel
                {
                    ErrorMessage = ex.Message,

                };
            }

            return input;
        }


        public bool Save(string id, string medicamento,string dosis, string fecha, string observacion,string user)
        {
            Boolean result = false;
            try
            {
                var cerdo = Objeto._context.Cerdos.Where(u => u.codigo.Equals(id)).ToList();
                var userlist = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].Id)).ToList();



                var cerdo_Medicamento = new Cerdo_Medicamento
                {
                    id_cerdo = cerdo[0].ID,
                    id_medicamento = int.Parse(medicamento),
                    cantidad_suministrada = dosis != null ? dosis : "0",
                    fecha = DateTime.Parse(fecha),
                    id_usuario = userlist[0].Id,
                    observacion=observacion




                };


                var log_aplicacion = new Historial_Acciones
                {
                    IdRole = userList5[0].RoleId,
                    Accion = "Guardar",
                    item = "Cerdo_Medicamentos",
                    fecha_registro = DateTime.Now,
                    IdUser = userlist[0].Id,
                };


                Objeto._context.Add(cerdo_Medicamento);
                Objeto._context.Add(log_aplicacion);
                Objeto._context.SaveChanges();


                result = true;


            }
            catch (Exception ex)
            {
                result = false;

            }

            return result;


        }

        public List<SelectListItem> getMedicamentos(string data = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var medicamentos = Objeto._context.medicamento.ToList();


            if (data != null)
            {

                var busqueda = Objeto._context.medicamento.Find(int.Parse(data));

                list.Add(new SelectListItem
                {
                    Value = busqueda.ID.ToString(),
                    Text = busqueda.Nombre,

                });

                medicamentos.ForEach(item =>
                {

                    if (!item.ID.ToString().Equals(data))
                    {
                        list.Add(new SelectListItem
                        {
                            Value = item.ID.ToString(),
                            Text = item.Nombre,


                        });

                    }
                });




            }
            else
            {
                medicamentos.ForEach(item =>
                {
                    list.Add(new SelectListItem
                    {

                        Value = item.ID.ToString(),
                        Text = item.Nombre,

                    }

                    );

                });
            }


            return list;
        }
    }
}

