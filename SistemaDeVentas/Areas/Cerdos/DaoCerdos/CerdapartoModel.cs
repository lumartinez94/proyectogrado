﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.Cerdos.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Cerdos.DaoCerdos
{
    public class CerdapartoModel
    {


        private ListObject Objeto = new ListObject();
        private static String idGet = null;
        private static List<Parto> listParto;
        private static String user = null;
        private static string[] Servicio_exitoso = { "Si", "No" };




        public CerdapartoModel(ApplicationDbContext context)
        {

            Objeto._usuarios = new Usuarios();
            Objeto._context = context;
            Objeto._usersRoles = new UserRoles();
            Objeto._image = new UploadImage();
            Objeto._userRoles = new List<SelectListItem>();

        }



        [BindProperty]

        public InputModel input { get; set; }
        public class InputModel : InputModelsPartos
        {





        }



        public InputModelsPartos getParto(string id)
        {

            try
            {
                var cerdo = Objeto._context.Cerdos.Where(u => u.ID.Equals(int.Parse(id)) || u.codigo.Equals(id)).ToList();
                input = new InputModel
                {



                    caravana_cerda = cerdo[0].codigo


                };
            }
            catch (Exception ex)
            {

                input = new InputModel
                {
                    ErrorMessage = ex.Message,

                };
            }

            return input;
        }


        public bool Save(string id, string fecha_nacimiento, string numero_hembra, string numero_macho, int muerto, string fecha_destete, string peso_promedio, string observacion, string user)
        {
            Boolean result = false;
            try
            {
                var id_female = Objeto._context.Cerdos.Where(u => u.codigo.Equals(id) || u.ID.Equals(int.Parse(id))).ToList();
                var userlist = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].Id)).ToList();



                var parto = new Parto
                {

                    id_cerdaMadre = id_female[0].ID,
                    fecha_Destete = DateTime.Parse(fecha_destete),
                    fecha_nacimiento = DateTime.Parse(fecha_nacimiento),
                    numero_hembra = int.Parse(numero_hembra) > 0 ? int.Parse(numero_hembra) : 0,
                    numero_macho= int.Parse(numero_macho) > 0 ? int.Parse(numero_macho) : 0,
                    nacidos_muertos= muerto > 0 ? muerto : 0,
                    peso_promedio= peso_promedio,
                    observacion=observacion!=""? observacion : ""


                };

                var gestacion = Objeto._context.gestacion.Where(u => u.id_cerdo.Equals(id_female[0].ID)).ToList();
               

                foreach (var item in gestacion) {

        
                    var gest = new Gestacion
                    {
                        id = item.id,
                        id_cerdo = item.id_cerdo,
                        fecha_aproximada = item.fecha_aproximada,
                        fecha_inicio = item.fecha_inicio,
                        fecha_real = DateTime.Parse(fecha_nacimiento),
                        estado = "terminado",
                    };

                    var data = Objeto._context.gestacion.Find(item.id);
                    Objeto._context.Entry(data).CurrentValues.SetValues(gest);
                    Objeto._context.Update(data);




                }










                var log_aplicacion = new Historial_Acciones
                {
                    IdRole = userList5[0].RoleId,
                    Accion = "Guardar",
                    item = "parto",
                    fecha_registro = DateTime.Now,
                    IdUser = userlist[0].Id,
                };


                Objeto._context.Add(parto);
                Objeto._context.Add(log_aplicacion);
                Objeto._context.SaveChanges();


                result = true;


            }
            catch (Exception ex)
            {
                result = false;

            }

            return result;


        }
    }
}
