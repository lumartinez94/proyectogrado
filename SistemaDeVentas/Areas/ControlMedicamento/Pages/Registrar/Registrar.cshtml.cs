using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.ControlAlimentos.Models;
using SistemaDeVentas.Areas.ControlMedicamento.Controllers;
using SistemaDeVentas.Areas.ControlMedicamento.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.ControlMedicamento.Pages.Registrar
{
    public class RegistrarModel : PageModel
    {


        private ListObject Objeto = new ListObject();
        private static String idGet = null;
        private static List<Cerdo_Medicamento> listControlCerdoMedicamentos;



        public RegistrarModel(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager, ApplicationDbContext context, IHostingEnvironment environment)
        {
            Objeto._roleManager = roleManager;
            Objeto._usuarios = new Usuarios();
            Objeto._context = context;
            Objeto._usersRoles = new UserRoles();
            Objeto._environment = environment;
            Objeto._image = new UploadImage();
            Objeto._userRoles = new List<SelectListItem>();
            Objeto._userManager = userManager;
        }
        public async Task OnGet(string id)
        {
            if (id != null)
            {
                idGet = id;
                await setEditAsync(id);
            }
            else
            {

                Input = new InputModel
                {

                    cerdosList = getCerdos(),
                   MedicamentoList = getMedicamentos(),

                };
            }


        }


        [BindProperty]
        public InputModel Input { get; set; }
        public class InputModel : InputModelsCerdosMedicamentos
        {

            [TempData]

            public string ErrorMessage { get; set; }

            public List<SelectListItem> MedicamentoList { get; set; }

            public List<SelectListItem> cerdosList { get; set; }


        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (idGet == null)
            {


                var valor = await Save();
                if (valor)
                {
                    return RedirectToAction(nameof(ControlMedicamentoController.Index), "ControlMedicamento");
                }
                {
                    return Page();
                }
            }
            else
            {
                var valor = await updateUser();
                if (valor)
                {
                    idGet = null;
                    return RedirectToAction(nameof(ControlMedicamentoController.Index), "ControlMedicamento");
                }
                else
                {
                    return Page();
                }
            }

        }


        private async Task<bool> Save()
        {
            var valor = false;
            try
            {
                var user = HttpContext.User.Identity.Name;

                var userAsp = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                var userT = Objeto._context.Tusuarios.Where(u => u.IdUser.Equals(userAsp[0].Id)).ToList();
                var userAPP = Objeto._context.AppUsuarios.Where(u => u.IdUser.Equals(userAsp[0].Id)).ToList();


                if (ModelState.IsValid)
                {


                    var Control = new Cerdo_Medicamento
                    {

                        id_cerdo = int.Parse(Input.id_cerdo),
                        id_medicamento = int.Parse(Input.id_medicamento),
                        id_usuario = userAsp[0].Id,
                        cantidad_suministrada = Input.cantidad_suministrada,
                        fecha= Input.fecha,
                        observacion= Input.observacion





                    };


                    await Objeto._context.AddAsync(Control);
                    Objeto._context.SaveChanges();





                    var userlist = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                    var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].Id)).ToList();


                    var log_aplicaciones = new Historial_Acciones
                    {
                        Accion = "Guardar",
                        item = "ControlMedicamentos",
                        fecha_registro = DateTime.Now,
                        IdRole = userList5[0].RoleId,
                        IdUser = userlist[0].Id,



                    };

                    await Objeto._context.AddAsync(log_aplicaciones);
                    Objeto._context.SaveChanges();
                    valor = true;




                }
                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Debe completar todos los campos",
                       MedicamentoList = getMedicamentos(),
                        cerdosList = getCerdos(),


                    };
                }





            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                   MedicamentoList = getMedicamentos(),
                    cerdosList = getCerdos(),



                };
                valor = false;
            }
            return valor;
        }


        public List<SelectListItem> getCerdos(string data = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var hembras = Objeto._context.Cerdos.Where(c => c.id_estado.Equals(1)).ToList();


            if (data != null)
            {

                var busqueda = Objeto._context.Cerdos.Find(int.Parse(data));

                list.Add(new SelectListItem
                {
                    Text = busqueda.codigo,
                    Value = busqueda.ID.ToString()
                });

                hembras.ForEach(item =>
                {

                    if (!item.ID.ToString().Equals(data))
                    {
                        list.Add(new SelectListItem
                        {
                            Value = item.ID.ToString(),
                            Text = item.codigo

                        });

                    }
                });




            }
            else
            {
                hembras.ForEach(item =>
                {
                    list.Add(new SelectListItem
                    {

                        Text = item.codigo,
                        Value = item.ID.ToString()
                    }

                    );

                });
            }


            return list;
        }


        public List<SelectListItem> getMedicamentos(string data = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var medicamentos = Objeto._context.medicamento.ToList();


            if (data != null)
            {

                var busqueda = Objeto._context.medicamento.Find(int.Parse(data));

                list.Add(new SelectListItem
                {
                    Value = busqueda.ID.ToString(),
                    Text = busqueda.Nombre,

                });

                medicamentos.ForEach(item =>
                {

                    if (!item.ID.ToString().Equals(data))
                    {
                        list.Add(new SelectListItem
                        {
                            Value = item.ID.ToString(),
                            Text = item.Nombre,


                        });

                    }
                });




            }
            else
            {
                medicamentos.ForEach(item =>
                {
                    list.Add(new SelectListItem
                    {

                        Value = item.ID.ToString(),
                        Text = item.Nombre,

                    }

                    );

                });
            }


            return list;
        }



        private async Task<bool> updateUser()

        {
            var valor = false;
            var user = HttpContext.User.Identity.Name;
            var userAsp = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
            var userList = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
            var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userList[0].Id)).ToList();



            try
            {


                if (ModelState.IsValid)
                {
                    var Control = new Cerdo_Medicamento
                    {
                        id= int.Parse(Input.id_cerdo_medicamento),
                        id_cerdo= int.Parse(Input.id_cerdo),
                        id_medicamento= int.Parse(Input.id_medicamento),
                        id_usuario= userAsp[0].Id,
                        cantidad_suministrada = Input.cantidad_suministrada,
                        fecha = Input.fecha,
                        id_corral= listControlCerdoMedicamentos[0].id_corral,
                        id_lote= listControlCerdoMedicamentos[0].id_lote,
                        observacion= Input.observacion
                        






                    };
                    Objeto._context.Update(Control);
                    await Objeto._context.SaveChangesAsync();





                    var log_aplicaciones = new Historial_Acciones
                    {
                        Accion = "Editar",
                        item = "ControlMedicamentos",
                        fecha_registro = DateTime.Now,
                        IdRole = userList5[0].RoleId,
                        IdUser = userList[0].Id,



                    };
                    await Objeto._context.AddAsync(log_aplicaciones);
                    Objeto._context.SaveChanges();


                    valor = true;

                }
                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Debe llenar todos los campos",

                       MedicamentoList = getMedicamentos(listControlCerdoMedicamentos[0].id_medicamento.ToString()),
                        cerdosList = getCerdos(listControlCerdoMedicamentos[0].id_cerdo.ToString())

                    };
                    valor = false;
                }
            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                   MedicamentoList = getMedicamentos(listControlCerdoMedicamentos[0].id_medicamento.ToString()),


                    cerdosList = getCerdos(listControlCerdoMedicamentos[0].id_cerdo.ToString())

                };
                valor = false;
            }
            return valor;


        }

        private async Task setEditAsync(string id)
        {
            listControlCerdoMedicamentos = Objeto._context.cerdo_Medicamento.Where(c => c.id.Equals(int.Parse(id))).ToList();

            Input = new InputModel
            {
                id_cerdo_medicamento = listControlCerdoMedicamentos[0].id.ToString(),
                id_medicamento = listControlCerdoMedicamentos[0].id_medicamento.ToString(),
                id_cerdo = listControlCerdoMedicamentos[0].id_cerdo.ToString(),
                cantidad_suministrada = listControlCerdoMedicamentos[0].cantidad_suministrada,
                MedicamentoList=getMedicamentos(listControlCerdoMedicamentos[0].id_medicamento.ToString()),
                cerdosList= getCerdos(listControlCerdoMedicamentos[0].id_cerdo.ToString()),
                fecha = listControlCerdoMedicamentos[0].fecha,
                observacion= listControlCerdoMedicamentos[0].observacion








            };
        }







    }
}