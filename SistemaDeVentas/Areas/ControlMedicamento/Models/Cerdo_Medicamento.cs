﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.ControlMedicamento.Models
{
    public class Cerdo_Medicamento
    {

        public int id { get; set; }

        public int id_cerdo { get; set; }
        public int id_medicamento { get; set; }
        public string cantidad_suministrada { get; set; }
        public string observacion { get; set; }
        public string id_usuario { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? fecha { get; set; }

        public int id_corral { get; set; }
        public int id_lote { get; set; }
        
    }
}
