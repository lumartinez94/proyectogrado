﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.ControlMedicamento.Models
{
    public class InputModelsCerdosMedicamentos
    {

        [Display(Name = "#ID")]
        public string id_cerdo_medicamento { get; set; }


        [Display(Name = "Numero del cerdo")]
        public string id_cerdo { get; set; }

        [Display(Name = "Numero del Cerdo")]
        public string numero_caravana { get; set; }

        [Display(Name = "#Numero corral")]
        public string numero_corral { get; set; }

        [Display(Name = "Nombre Medicamento")]
        public string nombre_medicamento { get; set; }

        [Display(Name = "Nombre medicamento")]
        public string id_medicamento { get; set; }

        [Display(Name = "Nombre medicamento")]
        public List<SelectListItem> MedicamentoList { get; set; }
        public string ErrorMessage { get; set; }

        [Display(Name = "Dosis")]
        public string cantidad_suministrada { get; set; }

        [Display(Name = "Observaciones")]
        public string observacion { get; set; }

        [Display(Name = "#Responsable")]
        public string correo_user { get; set; }

        public string id_usuario { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? fecha { get; set; }

        public string date { get; set; }
        public string id_corral { get; set; }
        public string id_lote { get; set; }
    }
}
