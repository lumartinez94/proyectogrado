﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SistemaDeVentas.Areas.ControlMedicamento.Models;
using SistemaDeVentas.Controllers;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.ControlMedicamento.Controllers
{
    [Area("ControlMedicamento")]
    [Authorize]
    public class ControlMedicamentoController : Controller
    {
       

        private ListObject Objeto = new ListObject();


        public ControlMedicamentoController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context)
        {
            Objeto._signInManager = signInManager;
            Objeto._usuarios = new Usuarios(userManager, signInManager, roleManager, context);
        }
        public async Task<IActionResult> Index (int id, String Search)
        {



            if (Objeto._signInManager.IsSignedIn(User))
            {
                var url = Request.Scheme + "://" + Request.Host.Value;
                var objects = new Paginador<InputModelsCerdosMedicamentos>().paginador(await Objeto._usuarios.getControlMedicamentosCerdosAsync(Search),
                    id, "ControlMedicamento", "ControlMedicamento", "Index", url);
                var models = new DataPaginador<InputModelsCerdosMedicamentos>
                {
                    List = (List<InputModelsCerdosMedicamentos>)objects[2],
                    Pagi_info = (String)objects[0],
                    Pagi_navegacion = (String)objects[1],
                    Input = new InputModelsCerdosMedicamentos()

                };


                return View(models);
            }
            else
            {

                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        

    }
}
}