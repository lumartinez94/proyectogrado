﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Users.Models
{
    public class InputModelRegistrarcs
    {
        [Required(ErrorMessage = "<font color='red'>El campo correo electronico es obligatorio.</font>")]
        [EmailAddress(ErrorMessage = "<font color='red'>El correo electronico no es una dirección de correo electronico valida.</font>")]
        public string Email { get; set; } 
        [Required(ErrorMessage = "<font color='red'>El campo Nombre es obligatorio.</font>")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "<font color='red'>El campo Apellido es obligatorio.</font>")]
        public string Apellido { get; set; }

        [Required(ErrorMessage = "<font color='red'>El campo cedula es obligatorio.</font>")]
        public string Cedula { get; set; }

        [Required(ErrorMessage = "<font color='red'>El campo telefono es obligatorio.</font>")]
        [DataType(DataType.PhoneNumber)]
       
        public string Telefono { get; set; }
        
        [Display(Name="Password")]
        [Required(ErrorMessage = "<font color='red'>El campo contraseña es obligatorio.</font>")]
        //[DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "<font color='red'>El numero de caracteres de {0} debe ser al menos {2}.</font>", MinimumLength = 6)]
        public string Password { get; set; }

        [Required]
        public string Role { get; set; }
        public string ID { get; set; }
        public int id { get; set; }
    }
}
