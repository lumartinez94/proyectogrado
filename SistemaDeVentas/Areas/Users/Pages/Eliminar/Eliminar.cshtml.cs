using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SistemaDeVentas.Areas.Users.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Users.Pages.Eliminar
{
    [Authorize(Roles ="Admin")]
    public class EliminarModel : PageModel
    {

        private ListObject objeto = new ListObject();
        public static InputModel model;
        public EliminarModel (UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager,RoleManager<IdentityRole>roleManager,ApplicationDbContext context,IHostingEnvironment environment)

        {
            objeto._context = context;
            objeto._environment = environment;
            objeto._image = new UploadImage();
            objeto._usuarios = new Usuarios(userManager, signInManager, roleManager, context);
        }
        public async Task <IActionResult> OnGet(String id)
        {
            if (id!=null){
                var data = await objeto._usuarios.getTUsuariosAsync(id);
                Input = new InputModel
                {
                    Cedula = data[0].Cedula,
                    Nombre = data[0].Nombre,
                    Apellido = data[0].Apellido,
                    Email=data[0].Email,
                    ID= data[0].ID,
                    id=data[0].id
                    
                };
                model = Input;
                return Page();
            }
            else
            {
                
                return Redirect("/Users?area=Users");
            }
        }
        public InputModel Input { get; set; }
        public class InputModel: InputModelRegistrarcs
        {
            [TempData]

            public string ErrorMesssage { get; set; }
 
        }

        public IActionResult OnPost()
        {
            try
            {

                var users = HttpContext.User.Identity.Name;
                var userList = objeto._context.Users.Where(u => u.Email.Equals(users)).ToList();
                var userList5 = objeto._context.UserRoles.Where(u => u.UserId.Equals(userList[0].Id)).ToList();

                var Appuser = new AppUsuarios
                {
                    IdTUsuario = model.id
                };

                var usuarios = new TUsuarios
                {
                    ID = model.id
                };


                objeto._context.AppUsuarios.Remove(Appuser);
                objeto._context.SaveChanges();
                objeto._context.Tusuarios.Remove(usuarios);
                objeto._context.SaveChanges();
                var user = objeto._context.Users.SingleOrDefault(m => m.Id.Equals(model.ID));
                objeto._context.Users.Remove(user);
                objeto._context.SaveChanges();
                objeto._image.deleteImage(objeto._environment, "Users", model.Email);


                var log_aplicaciones = new Historial_Acciones
                {
                    Accion = "Eliminar",
                    item = "User",
                    fecha_registro = DateTime.Now,
                    IdRole = userList5[0].RoleId,
                    IdUser = userList[0].Id,



                };

               objeto._context.AddAsync(log_aplicaciones);
                objeto._context.SaveChanges();

                return Redirect("/Users?area=Users");
            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMesssage = ex.Message
                };
                return Page();
            }
        }
    }
}