using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.Users.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Models;
using SistemaDeVentas.Library;

namespace SistemaDeVentas.Areas.Users.Pages.MiPerfil
{
    [Authorize]
    public class MIPerfilModel : PageModel
    {

        private ListObject Objeto = new ListObject();
        public static InputModel model;
        private static String idGet = null;
        private static List<IdentityUser> userList1;
        private static List<TUsuarios> userList2;
        private static List<AppUsuarios> userList3;
        public MIPerfilModel(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager, ApplicationDbContext context, IHostingEnvironment environment)
        {
            Objeto._roleManager = roleManager;
            Objeto._usuarios = new Usuarios();
            Objeto._context = context;
            Objeto._usersRoles = new UserRoles();
            Objeto._environment = environment;
            Objeto._image = new UploadImage();
            Objeto._userRoles = new List<SelectListItem>();
            Objeto._userManager = userManager;
        }
        public async Task OnGet(string id)
        {
            if (id != null)
            {
                idGet = id;
                await setEditAsync(id);

            }
            

        }

        [BindProperty]
        public InputModel Input { get; set; }
        public class InputModel : InputModelRegistrarcs
        {

            [TempData]

            public string ErrorMessage { get; set; }
            public IFormFile AvatarImage { get; set; }

            public List<SelectListItem> rolesLista { get; set; }

        }


        public async Task<IActionResult> OnPostAsync()
        {
            if (idGet != null)
            {

                var valor = await updateUser();
                if (valor)
                {

                    Input = new InputModel
                    {
                        Password = "**********",
                        rolesLista = Objeto._usersRoles.getRoles(Objeto._roleManager)
                        
                    };

                    return Page();
                   
                }
                else
                {
                    Input = new InputModel
                    {
                        Password = "**********",
                        rolesLista = Objeto._usersRoles.getRoles(Objeto._roleManager)
                    };

                    return Page();
                }
            }
            Input = new InputModel
            {
                Password = "**********",
                rolesLista = Objeto._usersRoles.getRoles(Objeto._roleManager)
            };

            return Page();

        }


        private async Task setEditAsync(string email)
        {
            userList1 = Objeto._userManager.Users.Where(u => u.Email.Equals(email)).ToList();
            userList2 = Objeto._context.Tusuarios.Where(u => u.IdUser.Equals(userList1[0].Id)).ToList();
            userList3 = Objeto._context.AppUsuarios.Where(u => u.IdUser.Equals(userList1[0].Id)).ToList();
            var userRoles = await Objeto._usersRoles.getRole(Objeto._userManager, Objeto._roleManager, userList1[0].Id);

            Input = new InputModel
            {
                Nombre = userList2[0].Nombre,
                Apellido = userList2[0].Apellido,
                Cedula = userList2[0].Cedula,
                Email = userList1[0].Email,
                Telefono = userList1[0].PhoneNumber,
                Password = "**********",
                rolesLista = getRoles(userRoles[0].Text)
            };




        }

       

        private async Task<bool> updateUser()

        {
            var valor = false;
            var newPassword="";
            var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userList1[0].Id)).ToList();
            try
            {


              

               


                        var identityUser = new IdentityUser
                        {
                            Id = userList1[0].Id,
                            UserName = Input.Email,
                            Email = Input.Email,
                            PhoneNumber = Input.Telefono,
                            EmailConfirmed = userList1[0].EmailConfirmed,
                            LockoutEnabled = userList1[0].LockoutEnabled,
                            LockoutEnd = userList1[0].LockoutEnd,
                            NormalizedEmail = userList1[0].NormalizedEmail,
                            NormalizedUserName = userList1[0].NormalizedUserName,
                            PasswordHash = userList1[0].PasswordHash,
                            PhoneNumberConfirmed = userList1[0].PhoneNumberConfirmed,
                            SecurityStamp = userList1[0].SecurityStamp,
                            TwoFactorEnabled = userList1[0].TwoFactorEnabled,
                            AccessFailedCount = userList1[0].AccessFailedCount,
                            ConcurrencyStamp = userList1[0].ConcurrencyStamp


                        };

                if (Input.Password.Equals("**********"))
                {
                    Objeto._context.Update(identityUser);
                    await Objeto._context.SaveChangesAsync();
                    var appUser = new AppUsuarios
                    {
                        Email = Input.Email,
                        password = userList3[0].password,
                        telefono = Input.Telefono,
                        IdTUsuario = userList2[0].ID,
                        ID = userList3[0].ID,
                        IdUser = userList1[0].Id

                    };

                    Objeto._context.Update(appUser);
                    await Objeto._context.SaveChangesAsync();
                }
                else
                {
                    newPassword = Objeto._userManager.PasswordHasher.HashPassword(identityUser, Input.Password);
                    identityUser.PasswordHash = newPassword;
                    Objeto._context.Update(identityUser);
                    await Objeto._context.SaveChangesAsync();

                    var appUser = new AppUsuarios
                    {
                        Email = Input.Email,
                        password = Input.Password,
                        telefono = Input.Telefono,
                        IdTUsuario = userList2[0].ID,
                        ID = userList3[0].ID,
                        IdUser = userList1[0].Id

                    };

                    Objeto._context.Update(appUser);
                    await Objeto._context.SaveChangesAsync();
                }
               
                        var usuarios = new TUsuarios
                        {
                            ID = userList2[0].ID,
                            Nombre = Input.Nombre,
                            Apellido = Input.Apellido,
                            Cedula = Input.Cedula,
                            Image = Input.Email,
                            IdUser = userList1[0].Id

                        };
                        Objeto._context.Update(usuarios);
                        await Objeto._context.SaveChangesAsync();


                var log_aplicaciones = new Historial_Acciones
                {
                    Accion = "Editar perfil",
                    item = "usuario",
                    fecha_registro = DateTime.Now,
                    IdRole = userList5[0].RoleId,
                    IdUser = userList1[0].Id,



                };
                await Objeto._context.AddAsync(log_aplicaciones);
                Objeto._context.SaveChanges();




                var imageName = Input.Email + ".png";
                        await Objeto._image.CopyImageAsync(Input.AvatarImage, imageName, Objeto._environment, "Users", idGet);
                        valor = true;
                    
                   
                
            }





            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                    rolesLista = getRoles(Input.Role)
                };
                valor = false;
            }
            return valor;


        }
        private List<SelectListItem> getRoles(string role)
        {
            Objeto._userRoles.Add(new SelectListItem
            {
                Text = role
            });
            var roles = Objeto._usersRoles.getRoles(Objeto._roleManager);
            roles.ForEach(item =>
            {
                if (item.Text != role)
                {
                    Objeto._userRoles.Add(new SelectListItem
                    {
                        Text = item.Text
                    });
                }

            });
            return Objeto._userRoles;
        }


    }
}
