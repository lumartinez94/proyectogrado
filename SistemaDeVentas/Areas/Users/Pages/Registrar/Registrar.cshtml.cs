using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SistemaDeVentas.Areas.Users.Models;
using SistemaDeVentas.Library;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using SistemaDeVentas.Data;
using SistemaDeVentas.Areas.Users.Controllers;
using Microsoft.AspNetCore.Authorization;
using SistemaDeVentas.Areas.Cerdos.Models;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Users.Pages.Registrar
{
    [Authorize (Roles = "Admin")]
    public class RegistrarModel : PageModel
    {
        private ListObject Objeto = new ListObject();
        private static String idGet = null;
        private static List<IdentityUser> userList1;
        private static List<TUsuarios> userList2;
        private static List<AppUsuarios> userList3;
        private static List<Raza> userList4;
        private static List<IdentityRole> userList5;
        

        public RegistrarModel(RoleManager <IdentityRole> roleManager,UserManager<IdentityUser> userManager,ApplicationDbContext context,IHostingEnvironment environment)
        {
            Objeto._roleManager = roleManager;
            Objeto._usuarios = new Usuarios();
            Objeto._context = context;
            Objeto._usersRoles = new UserRoles();
            Objeto._environment = environment;
            Objeto._image = new UploadImage();
            Objeto._userRoles = new List<SelectListItem>();
            Objeto._userManager = userManager;
        }
        public async Task OnGetAsync(String id)
        {

            if (id != null)
            {
                idGet = id;
                await setEditAsync(id);
            }
            else
            {
                Input = new InputModel
                {
                    
                    rolesLista = Objeto._usersRoles.getRoles(Objeto._roleManager)
                };

            }

        }

        [BindProperty]
        public InputModel Input { get; set; }
        public class InputModel:InputModelRegistrarcs
        {       
      

            [TempData]
            public IFormFile AvatarImage { get; set; }
      
            public string ErrorMessage { get; set; }
            public List<SelectListItem> rolesLista { get; set; }


        }
        [Authorize(Roles ="Admin")]
        public async Task<IActionResult> OnPostAsync()
        {
            if (idGet == null)
            {


                var valor = await Save();
                if (valor)
                {
                    return RedirectToAction(nameof(UsersController.Index), "Users");
                }
                {
                    return Page();
                }
            }
            else
            {
                var valor = await updateUser();
                if (valor)
                {
                    return RedirectToAction(nameof(UsersController.Index), "Users");
                }
                else
                {
                    return Page();
                }
            }
      
        }

        private async Task<bool> Save()
        {
            var valor = false;
            try
            {
                if (ModelState.IsValid)
                {
                    Objeto._userRoles.Add(new SelectListItem
                    {
                        Text = Input.Role
                    });

                    var userList = Objeto._userManager.Users.Where(u => u.Email.Equals(Input.Email)).ToList();
                   

                    if (userList.Count.Equals(0))
                    {
                        var ImageName = Input.Email + ".png";
                        var user = new IdentityUser
                        {
                            UserName = Input.Email,
                            Email = Input.Email,
                            PhoneNumber = Input.Telefono,


                        };
                        var result = await Objeto._userManager.CreateAsync(user, Input.Password);
                        if (result.Succeeded)
                        {
                            await Objeto._userManager.AddToRoleAsync(user, Input.Role);
                            var listUser = Objeto._userManager.Users.Where(u => u.Email.Equals(Input.Email)).ToList();


                            var usuarios = new TUsuarios
                            {
                                Nombre = Input.Nombre,
                                Apellido = Input.Apellido,
                                Cedula = Input.Cedula,
                                Image = Input.Email,
                                IdUser = listUser[0].Id,
                            };
                            await Objeto._context.AddAsync(usuarios);
                            Objeto._context.SaveChanges();

                            var userlist = Objeto._context.Tusuarios.Where(u => u.IdUser.Equals(listUser[0].Id)).ToList();
                            var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].IdUser)).ToList();
                            var AppUser = new AppUsuarios
                            {
                                Email = Input.Email,
                                telefono = Input.Telefono,
                                password = Input.Password,
                                IdTUsuario = userlist[0].ID,
                                IdUser = listUser[0].Id,
                                fecha_registro = DateTime.Now
                            };

                            var log_aplicaciones = new Historial_Acciones
                            {
                                Accion = "Guardar",
                                item = "usuario",
                                fecha_registro = DateTime.Now,
                                IdRole = userList5[0].RoleId,
                                IdUser = userlist[0].IdUser,



                            };
                            await Objeto._context.AddAsync(log_aplicaciones);
                            await Objeto._context.AddAsync(AppUser);
                            Objeto._context.SaveChanges();


                            await Objeto._image.CopyImageAsync(Input.AvatarImage, ImageName, Objeto._environment, "Users",null);
                            valor = true;

                        }
                        else
                        {

                            foreach (var item in result.Errors)
                            {
                                Input = new InputModel
                                {
                                    ErrorMessage = item.Description,
                                    rolesLista = Objeto._userRoles
                                };
                            }
                            valor = false;
                        }


                    }
                    else
                    {
                        Input = new InputModel
                        {
                            ErrorMessage = "El " + Input.Email + " ya esta registrado",
                            rolesLista = Objeto._userRoles
                        };

                        valor = false;
                    }
                }
                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Seleccione un role",
                        rolesLista = Objeto._usersRoles.getRoles(Objeto._roleManager)
                };
            }
                
               
            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                    rolesLista = Objeto._userRoles
                };
                valor = false;
            }
            return valor;
        }

        private async Task setEditAsync (string email)
        {
            userList1 = Objeto._userManager.Users.Where(u => u.Email.Equals(email)).ToList();
            userList2 = Objeto._context.Tusuarios.Where(u => u.IdUser.Equals(userList1[0].Id) ).ToList();
            userList3 = Objeto._context.AppUsuarios.Where(u => u.IdUser.Equals(userList1[0].Id)).ToList();
            var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userList1[0].Id)).ToList();


            var userRoles = await Objeto._usersRoles.getRole(Objeto._userManager, Objeto._roleManager, userList1[0].Id);


            Input = new InputModel
            {
                Nombre = userList2[0].Nombre,
                Apellido = userList2[0].Apellido,
                Cedula = userList2[0].Cedula,
                Email = userList1[0].Email,
                Telefono = userList1[0].PhoneNumber,
                Password = "********",
                rolesLista = getRoles(userRoles[0].Text)
        };




        }

        private async Task<bool> updateUser()

        {
            var valor = false;
            var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userList1[0].Id)).ToList();

            try
            {
                

                if (ModelState.IsValid)
                {
                    var identityUser = new IdentityUser
                    {   Id=userList1[0].Id,
                        UserName=Input.Email,
                        Email = Input.Email,
                        PhoneNumber = Input.Telefono,
                        EmailConfirmed= userList1[0].EmailConfirmed,
                        LockoutEnabled = userList1[0].LockoutEnabled,
                        LockoutEnd= userList1[0].LockoutEnd,
                        NormalizedEmail= userList1[0].NormalizedEmail,
                        NormalizedUserName=userList1[0].NormalizedUserName,
                        PasswordHash= userList1[0].PasswordHash,
                        PhoneNumberConfirmed=userList1[0].PhoneNumberConfirmed,
                        SecurityStamp= userList1[0].SecurityStamp,
                        TwoFactorEnabled=userList1[0].TwoFactorEnabled,
                        AccessFailedCount =userList1[0].AccessFailedCount,
                        ConcurrencyStamp=userList1[0].ConcurrencyStamp
                        

                    };
                    Objeto._context.Update(identityUser);
                    await Objeto._context.SaveChangesAsync();
                    var usuarios = new TUsuarios
                    {
                        ID = userList2[0].ID,
                        Nombre = Input.Nombre,
                        Apellido = Input.Apellido,
                        Cedula = Input.Cedula,
                        Image = Input.Email,
                        IdUser = userList1[0].Id

                    };
                    Objeto._context.Update(usuarios);
                    await Objeto._context.SaveChangesAsync();
                    var appUser = new AppUsuarios
                    {
                        ID= userList3[0].ID,
                        IdUser= userList1[0].Id,
                        Email= Input.Email,
                        telefono= Input.Telefono,
                        password= userList3[0].password
                    };

                    Objeto._context.Update(appUser);
                    await Objeto._context.SaveChangesAsync();

                    var log_aplicaciones = new Historial_Acciones
                    {
                        Accion = "Editar",
                        item = "usuario",
                        fecha_registro = DateTime.Now,
                        IdRole = userList5[0].RoleId,
                        IdUser = userList1[0].Id,



                    };
                    await Objeto._context.AddAsync(log_aplicaciones);
                    Objeto._context.SaveChanges();

                    var imageName = Input.Email + ".png";
                    await Objeto._image.CopyImageAsync(Input.AvatarImage, imageName, Objeto._environment, "Users",idGet);
                    valor = true;

                }
                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Seleccione un role",
                        rolesLista = Objeto._usersRoles.getRoles(Objeto._roleManager)
                    };
                    valor = false;
                }
            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                    rolesLista = getRoles(Input.Role)
                };
                valor = false;
            }
            return valor;

            
        }
        private List<SelectListItem> getRoles(string role)
        {
            Objeto._userRoles.Add(new SelectListItem
            {
                Text=role
            });
            var roles = Objeto._usersRoles.getRoles(Objeto._roleManager);
            roles.ForEach(item =>
            {
                if (item.Text != role)
                {
                    Objeto._userRoles.Add(new SelectListItem
                    {
                        Text = item.Text 
                    });
                }

            });
            return Objeto._userRoles;
        }


    }

}