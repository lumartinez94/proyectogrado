﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SistemaDeVentas.Controllers;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Areas.Users.Models;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Users.Controllers
{
    [Area("Users")]
    [Authorize]
    public class UsersController : Controller
    {
        private ListObject Objeto = new ListObject();

       
        public UsersController(UserManager <IdentityUser> userManager,SignInManager<IdentityUser> signInManager,RoleManager<IdentityRole> roleManager, ApplicationDbContext context)
        {
            Objeto._signInManager = signInManager;
            Objeto._usuarios = new Usuarios(userManager,signInManager,roleManager,context);
        }
       
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index(int id, String Search)
        {
            if (Objeto._signInManager.IsSignedIn(User))
            {
                var url = Request.Scheme + "://" + Request.Host.Value;
                var objects = new Paginador<InputModelRegistrarcs>().paginador(await Objeto._usuarios.getTUsuariosAsync(Search),
                    id,"Users","Users","Index",url);
                var models = new DataPaginador<InputModelRegistrarcs>
                {
                    List = (List < InputModelRegistrarcs >) objects[2],
                    Pagi_info = (String)objects[0],
                    Pagi_navegacion = (String)objects[1],
                    Input = new InputModelRegistrarcs()
                
                };
                
               
                return View(models);
            }
            else
            {

                return RedirectToAction(nameof(HomeController.Index), "Home");
            } 
        }

        
        public async Task<IActionResult> SessionClose()
        {
            HttpContext.Session.Remove("User");
            await Objeto._signInManager.SignOutAsync();
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }
    }
}