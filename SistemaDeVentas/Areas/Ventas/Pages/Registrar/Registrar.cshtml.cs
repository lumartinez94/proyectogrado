using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.Ventas.Controllers;
using SistemaDeVentas.Areas.Ventas.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Ventas.Pages.Registrar
{
    public class RegistrarModel : PageModel
    {
        private ListObject Objeto = new ListObject();
        private static String idGet = null;
        private static List<IdentityUser> userList1;
        private static List<VentasU> listVentas;
        private static List<IdentityRole> userList5;
        public static string[] tipoVenta = { "En canal", "En pie", "Otro" };

        public RegistrarModel(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager, ApplicationDbContext context, IHostingEnvironment environment)
        {
            Objeto._roleManager = roleManager;
            Objeto._usuarios = new Usuarios();
            Objeto._context = context;
            Objeto._usersRoles = new UserRoles();
            Objeto._environment = environment;
            Objeto._image = new UploadImage();
            Objeto._userRoles = new List<SelectListItem>();
            Objeto._userManager = userManager;
        }
        public async Task OnGet(string id)
        {
            if (id != null)
            {
                idGet = id;
                await setEditAsync(id);
            }
            else
            {

                Input = new InputModel
                {
                    tipoVentaList = getTipoVentas(),
                    cerdosList = getCerdos(),
                    clientesList = getClientes(),
                    
                };
            }


        }


        [BindProperty]
        public InputModel Input { get; set; }
        public class InputModel : InputModelVentas
        {

            [TempData]

            public string ErrorMessage { get; set; }

            public List<SelectListItem> tipoVentaList { get; set; }

            public List<SelectListItem> cerdosList { get; set; }

            public List<SelectListItem> clientesList { get; set; }
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (idGet == null)
            {


                var valor = await Save();
                if (valor)
                {
                    return RedirectToAction(nameof(VentasController.Index), "Ventas");
                }
                {
                    return Page();
                }
            }
            else
            {
                var valor = await updateUser();
                if (valor)
                {
                    idGet = null;
                    return RedirectToAction(nameof(VentasController.Index), "Ventas");
                }
                else
                {
                    return Page();
                }
            }

        }


        private async Task<bool> Save()
        {
            var valor = false;
            try
            {
                var user = HttpContext.User.Identity.Name;

                var userAsp = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                var userT = Objeto._context.Tusuarios.Where(u => u.IdUser.Equals(userAsp[0].Id)).ToList();
                var userAPP = Objeto._context.AppUsuarios.Where(u => u.IdUser.Equals(userAsp[0].Id)).ToList();
              

                if (ModelState.IsValid)
                {


                   



                    if (Input.TipoVenta.Equals("Otro"))
                    {

                        var ventas = new VentasU
                        {
                            tipo_venta = Input.TipoVenta,
                            precio_total = Input.precio_total,
                            Observacion = Input.Observacion,
                            id_cerdo = int.Parse(Input.id_cerdo),
                            id_user = userAsp[0].Id,
                            idTusuario = userT[0].ID,
                            idAppUser = userAPP[0].ID,
                            peso = Input.Peso,
                            precioKg= Input.PrecioKg,
                            id_cliente= int.Parse(Input.id_cliente),
                            fecha = Input.Fecha




                        };


                        await Objeto._context.AddAsync(ventas);
                        Objeto._context.SaveChanges();

                    }
                    else
                    {


                        var ventas = new VentasU
                        {

                            tipo_venta = Input.TipoVenta,
                            precio_total = Input.precio_total,
                            Observacion = Input.Observacion,
                            id_cerdo = int.Parse(Input.id_cerdo),
                            id_user = userAsp[0].Id,
                            idTusuario = userT[0].ID >0 ?userT[0].ID : 0,
                            idAppUser = userAPP[0].ID > 0 ? userAPP[0].ID : 0,
                            peso = Input.Peso,
                            precioKg = Input.PrecioKg,
                            id_cliente = int.Parse(Input.id_cliente),
                            fecha = Input.Fecha




                        };


                        await Objeto._context.AddAsync(ventas);
                        Objeto._context.SaveChanges();
                    }


                  

                    var userlist = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                    var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].Id)).ToList();


                    var log_aplicaciones = new Historial_Acciones
                    {
                        Accion = "Guardar",
                        item = "Ventas",
                        fecha_registro = DateTime.Now,
                        IdRole = userList5[0].RoleId,
                        IdUser = userlist[0].Id,



                    };

                    await Objeto._context.AddAsync(log_aplicaciones);
                    Objeto._context.SaveChanges();
                    valor = true;

                }


                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Debe completar todos los campos",
                        tipoVentaList = getTipoVentas(),
                        cerdosList = getCerdos(),
                        clientesList = getClientes(),

                    };
                }


            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                    tipoVentaList = getTipoVentas(),
                    cerdosList = getCerdos(),
                    clientesList = getClientes(),


                };
                valor = false;
            }
            return valor;
        }


        public List<SelectListItem> getCerdos(string data = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var hembras = Objeto._context.Cerdos.Where(c => c.id_estado.Equals(1)).ToList();


            if (data != null)
            {

                var busqueda = Objeto._context.Cerdos.Find(int.Parse(data));

                list.Add(new SelectListItem
                {
                    Text = busqueda.codigo,
                    Value = busqueda.ID.ToString()
                });

                hembras.ForEach(item =>
                {

                    if (!item.ID.ToString().Equals(data))
                    {
                        list.Add(new SelectListItem
                        {
                            Value = item.ID.ToString(),
                            Text = item.codigo

                        });

                    }
                });




            }
            else
            {
                hembras.ForEach(item =>
                {
                    list.Add(new SelectListItem
                    {

                        Text = item.codigo,
                        Value = item.ID.ToString()
                    }

                    );

                });
            }


            return list;
        }


        public List<SelectListItem> getClientes(string data = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var clientes = Objeto._context.cliente.ToList();


            if (data != null)
            {

                var busqueda = Objeto._context.cliente.Find(int.Parse(data));

                list.Add(new SelectListItem
                {
                    Value = busqueda.id.ToString(),
                    Text = busqueda.nombre,
                  
                });

                clientes.ForEach(item =>
                {

                    if (!item.id.ToString().Equals(data))
                    {
                        list.Add(new SelectListItem
                        {
                            Value = item.id.ToString(),
                            Text = item.nombre
                            

                        });

                    }
                });




            }
            else
            {
                clientes.ForEach(item =>
                {
                    list.Add(new SelectListItem
                    {

                        Value = item.id.ToString(),
                        Text = item.nombre,
                       
                    }

                    );

                });
            }


            return list;
        }

        private List<SelectListItem> getTipoVentas(string data = null)
        {
            List<SelectListItem> ListTipo = new List<SelectListItem>();


            if (data != null)
            {
                if (data.Equals("Otro"))
                {

                    ListTipo.Add(new SelectListItem
                    {
                        Value = "0",
                        Text = data
                    });
                }
                else
                {

                    ListTipo.Add(new SelectListItem
                    {
                        Value = "0",
                        Text = data
                    });
                }

                for (int i = 0; i < tipoVenta.Length; i++)
                {
                    if (!tipoVenta[i].Equals(data))
                    {
                       ListTipo.Add(new SelectListItem
                        {
                            Value = i.ToString(),
                            Text = tipoVenta[i]
                        });
                    }
                }
            }
            else
            {


                for (int i = 0; i < tipoVenta.Length; i++)
                {
                    
                    
                        ListTipo.Add(new SelectListItem
                        {
                            Value = i.ToString(),
                            Text = tipoVenta[i]
                        });
                    
                }

            }

            return ListTipo;

        }

        private async Task<bool> updateUser()

        {
            var valor = false;
            var user = HttpContext.User.Identity.Name;
            var userAsp = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
            var userT = Objeto._context.Tusuarios.Where(u => u.IdUser.Equals(userAsp[0].Id)).ToList();
            var userAPP = Objeto._context.Tusuarios.Where(u => u.IdUser.Equals(userAsp[0].Id)).ToList();
            var userList = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
            var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userList[0].Id)).ToList();
            var cliente = Objeto._context.cliente.Where(c => c.id.Equals(int.Parse(Input.id_cliente))).ToList();

            try
            {


                if (ModelState.IsValid)
                {
                    var Ventas = new VentasU
                    {
                        ID = int.Parse(Input.id_venta),
                        id_user= userAsp[0].Id,
                        idAppUser= userAPP[0].ID,
                        idTusuario= userT[0].ID,
                        fecha= Input.Fecha,
                        precioKg= Input.PrecioKg,
                        Observacion= Input.Observacion,
                        id_cerdo= int.Parse(Input.id_cerdo),
                        id_cliente= cliente[0].id,
                        precio= Input.Precio,
                        peso= Input.Peso,
                        precio_total= Input.precio_total,
                        tipo_venta= Input.TipoVenta
                        





                    };
                    Objeto._context.Update(Ventas);
                    await Objeto._context.SaveChangesAsync();





                    var log_aplicaciones = new Historial_Acciones
                    {
                        Accion = "Editar",
                        item = "Ventas",
                        fecha_registro = DateTime.Now,
                        IdRole = userList5[0].RoleId,
                        IdUser = userList[0].Id,



                    };
                    await Objeto._context.AddAsync(log_aplicaciones);
                    Objeto._context.SaveChanges();


                    valor = true;

                }
                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Debe llenar todos los campos",

                        tipoVentaList = getTipoVentas(listVentas[0].tipo_venta),
                          clientesList= getClientes(cliente[0].id.ToString()),
                          cerdosList= getCerdos(Input.id_cerdo.ToString())

                    };
                    valor = false;
                }
            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                    tipoVentaList = getTipoVentas(listVentas[0].tipo_venta),
                
                    clientesList = getClientes(cliente[0].id.ToString()),
                    cerdosList = getCerdos(Input.id_cerdo.ToString())

                };
                valor = false;
            }
            return valor;


        }

        private async Task setEditAsync(string id)
        {
            listVentas = Objeto._context.Ventas.Where(c => c.ID.Equals(int.Parse(id))).ToList();

            Input = new InputModel
            {
               tipoVentaList= getTipoVentas(listVentas[0].tipo_venta),
               Fecha= listVentas[0].fecha,
               precio_total= listVentas[0].precio_total,
               Peso= listVentas[0].peso,
               PrecioKg= listVentas[0].precioKg,
               Observacion= listVentas[0].Observacion,
               id_cliente= listVentas[0].id_cliente.ToString(),
               cerdosList= getCerdos( listVentas[0].id_cerdo.ToString()),
               clientesList= getClientes(listVentas[0].id_cliente.ToString()),
               id_venta= listVentas[0].ID.ToString(),
               Precio= listVentas[0].precio
               
               
               





            };
        }






    }
}