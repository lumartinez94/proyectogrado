﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Ventas.Models
{
    public class VentasU
    {

        public int ID { get; set; }
        public string precioKg { get; set; }
        public string peso { get; set; }

        public  string tipo_venta {get;set;}

        public string precio { get; set; }

       public int id_cerdo { get; set; }

        public int id_cliente { get; set; }

        public int idTusuario { get; set; }

        public int idAppUser { get; set; }

        public string precio_total { get; set; }
        public string id_user { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? fecha { get; set; }

        public string Observacion { get; set; }
    }
}
