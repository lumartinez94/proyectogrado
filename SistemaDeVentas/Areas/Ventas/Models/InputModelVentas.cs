﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Ventas.Models
{
    public class InputModelVentas
    {

        [Required(ErrorMessage = "<font color='red'> Debe seleccionar un tipo de venta</font>")]

        [Display(Name ="Tipo de venta")]
        public string TipoVenta { get; set; }


        [Required(ErrorMessage = "<font color='red'>El campo Precio es obligatorio.</font>")]

        [Display(Name = "Precio X kg")]
        public string PrecioKg { get; set; }

        [Required(ErrorMessage = "<font color='red'>El campo fecha es obligatorio.</font>")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? Fecha { get; set; }

      
        [Display(Name = "Precio")]
        public string Precio { get; set; }

        public string id_venta { get; set; }


        [Display(Name = "Atendido Por")]
        public string correo_empleado { get; set; }

        [Display(Name = "#Caravana")]
        public string caravana_cerdo { get; set; }



        public string id_user {get;set;}

        [Display(Name = "Observación")]
        public string Observacion { get; set; }
        public string Peso{ get; set; }


        [Display(Name = "ID")]
        public string id_cerdo { get; set; }

        public string id_cliente { get; set; }

        public string Nombre_Cliente { get;set; }

        public string idTusuario { get; set; }

        public string idAppUser { get; set; }

        public string precio_total { get; set; }


    }
}
