using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SistemaDeVentas.Areas.Corrales.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Corrales.Pages.Eliminar
{
    [Authorize]
    public class EliminarModel : PageModel
    {
       

        private ListObject objeto = new ListObject();
        public static InputModel model;
        public EliminarModel(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context, IHostingEnvironment environment)

        {
            objeto._context = context;
            objeto._environment = environment;
            objeto._usuarios = new Usuarios(userManager, signInManager, roleManager, context);
        }
        public async Task<IActionResult> OnGet(String id)
        {
            if (id != null)
            {
                var data =await objeto._context.Corrals.FindAsync(int.Parse(id));
                Input = new InputModel
                {
                    
                    ID = data.ID,
                    Comentario= data.comentario,
                    numeroCorral= data.numeroCorral
                    
               

                };
                model = Input;
                return Page();
            }
            else
            {

                return Redirect("/Corrales?area=Corrales");
            }
        }
        public InputModel Input { get; set; }
        public class InputModel : InputModelCorral
        {
            [TempData]

            public string ErrorMesssage { get; set; }

        }

        public IActionResult OnPost()
        {
            try
            {

                var users = HttpContext.User.Identity.Name;
                var userList = objeto._context.Users.Where(u => u.Email.Equals(users)).ToList();
                var userList5 = objeto._context.UserRoles.Where(u => u.UserId.Equals(userList[0].Id)).ToList();
                var corrales= new Corral
                {
                    ID = model.ID,
                    comentario= model.Comentario,
                    numeroCorral= model.numeroCorral
                    
                };

                objeto._context.Corrals.Remove(corrales);
                objeto._context.SaveChanges();


                var log_aplicaciones = new Historial_Acciones
                {
                    Accion = "Eliminar",
                    item = "Corral",
                    fecha_registro = DateTime.Now,
                    IdRole = userList5[0].RoleId,
                    IdUser = userList[0].Id,



                };

                objeto._context.AddAsync(log_aplicaciones);
                objeto._context.SaveChanges();

                return Redirect("/Corrales?area=Corrales");
            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMesssage = ex.Message
                };
                return Page();
            }
        }
    }
}