using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.Corrales.Controllers;
using SistemaDeVentas.Areas.Corrales.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Corrales.Pages.Registrar
{
    [Authorize]
    public class RegistrarModel : PageModel
    {

        private ListObject Objeto = new ListObject();
        private static String idGet = null;
        private static List<IdentityUser> userList1;
        private static List<Corral> CorralList;
        private static List<IdentityRole> userList5;

        public RegistrarModel(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager, ApplicationDbContext context, IHostingEnvironment environment)
        {
            Objeto._roleManager = roleManager;
            Objeto._usuarios = new Usuarios();
            Objeto._context = context;
            Objeto._usersRoles = new UserRoles();
            Objeto._environment = environment;
            Objeto._image = new UploadImage();
            Objeto._userRoles = new List<SelectListItem>();
            Objeto._userManager = userManager;
        }
        public async Task OnGetAsync(String id)
        {

            if (id != null)
            {
                idGet = id;
                await setEditAsync(id);
            }
         
           

        }



        [BindProperty]
        public InputModel Input { get; set; }
        public class InputModel : InputModelCorral
        {

            [TempData]
            public string ErrorMessage { get; set; }
            public List<SelectListItem> TipoLista { get; set; }

        }


    



        public async Task<IActionResult> OnPostAsync()
        {
            if (idGet == null)
            {


                var valor = await Save();
                if (valor)
                {
                    return RedirectToAction(nameof(CorralesController.Index), "Corrales");
                }
                {
                    return Page();
                }
            }
            else
            {
                var valor = await updateUser();
                if (valor)
                {
                    return RedirectToAction(nameof(CorralesController.Index), "Corrales");
                }
                else
                {
                    return Page();
                }
            }

        }

        private async Task<bool> Save()
        {
            var valor = false;
            try
            {
                var user = HttpContext.User.Identity.Name;

                if (ModelState.IsValid)
                {


                   

                    var corrales = new Corral
                    {
                      
                        comentario = Input.Comentario,
                        numeroCorral= Input.numeroCorral
                        

                    };


                    await Objeto._context.AddAsync(corrales);
                    Objeto._context.SaveChanges();

                    var userlist = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                    var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].Id)).ToList();


                    var log_aplicaciones = new Historial_Acciones
                    {
                        Accion = "Guardar",
                        item = "Corral",
                        fecha_registro = DateTime.Now,
                        IdRole = userList5[0].RoleId,
                        IdUser = userlist[0].Id,



                    };

                    await Objeto._context.AddAsync(log_aplicaciones);
                    Objeto._context.SaveChanges();
                    valor = true;

                }


                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Debe completar los campos obligatorios",

                    };
                }


            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                   

                };
                valor = false;
            }
            return valor;
        }


        private async Task<bool> updateUser()

        {
            var valor = false;
            var user = HttpContext.User.Identity.Name;
            var userList = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
            var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userList[0].Id)).ToList();

            try
            {


                if (ModelState.IsValid)
                {
                    var corral = new Corral
                    {
                       ID=Input.ID,
                       comentario= Input.Comentario,
                       numeroCorral= Input.numeroCorral
                       
                       


                    };
                    Objeto._context.Update(corral);
                    await Objeto._context.SaveChangesAsync();
                    
                   

                    

                    var log_aplicaciones = new Historial_Acciones
                    {
                        Accion = "Editar",
                        item = "Corral",
                        fecha_registro = DateTime.Now,
                        IdRole = userList5[0].RoleId,
                        IdUser = userList1[0].Id,



                    };
                    await Objeto._context.AddAsync(log_aplicaciones);
                    Objeto._context.SaveChanges();

                    
                    valor = true;

                }
                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Debe completar los campos obligatorios",
                        
                    };
                    valor = false;
                }
            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                  
                };
                valor = false;
            }
            return valor;


        }

        private async Task setEditAsync(string id)
        {
            CorralList = Objeto._context.Corrals.Where(c => c.ID.Equals(int.Parse(id))).ToList();

            Input = new InputModel
            {
                ID = CorralList[0].ID,
                Comentario = CorralList[0].comentario,
                numeroCorral= CorralList[0].numeroCorral
                



            };
        }


    }

}







