﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SistemaDeVentas.Areas.Corrales.Models;
using SistemaDeVentas.Controllers;
using SistemaDeVentas.Data;
using SistemaDeVentas.Areas.Corrales.DaoCorral;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Corrales.Controllers
{
    [Area("Corrales")]
    [Authorize]
    public class CorralesController : Controller
    {

        private ListObject Objeto = new ListObject();


        public CorralesController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context)
        {
            Objeto._signInManager = signInManager;
            Objeto._context = context;
            Objeto._usuarios = new Usuarios(userManager, signInManager, roleManager, context);
        }

        public async Task<IActionResult> Index(int id, String Search)
        {


            if (Objeto._signInManager.IsSignedIn(User))
            {
                var url = Request.Scheme + "://" + Request.Host.Value;
                var objects = new Paginador<InputModelCorral>().paginador(await Objeto._usuarios.getCorralAsync(Search),
                    id, "Corrales", "Corrales", "Index", url);
                var models = new DataPaginador<InputModelCorral>
                {
                    List = (List<InputModelCorral>)objects[2],
                    Pagi_info = (String)objects[0],
                    Pagi_navegacion = (String)objects[1],
                    Input = new InputModelCorral()

                };


                return View(models);
            }
            else
            {

                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }


        [HttpPost]
        public IActionResult getModal()
        {








            return PartialView("~/Areas/Corrales/Views/addCorral/addCorral.cshtml");



        }

        [HttpPost]
        public IActionResult editModal(string id)
        {



           

                var corral = new CorralModel(Objeto._context);


                return PartialView("~/Areas/Corrales/Views/editCorral/editCorral.cshtml", corral.getCorral(int.Parse(id)));

            


        }


        public IActionResult saveCorral(string numeroCorral, string comentario)
        {

            var corral = new CorralModel(Objeto._context);

            var user = HttpContext.User.Identity.Name;
            var result = corral.Save(numeroCorral, comentario, user);



            return RedirectToAction(nameof(CorralesController.Index), "Corrales");
        }

        [HttpPost]
        public IActionResult updateCorral(string id, string numeroCorral, string comentario)
        {

            var corral = new CorralModel(Objeto._context);


            corral.updateCorral(int.Parse(id), numeroCorral, comentario);



            return RedirectToAction("Index", "Corrales");
        }




    }

}
