﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.Corrales.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Corrales.DaoCorral
{
    public class CorralModel
    {

        private ListObject Objeto = new ListObject();




        public CorralModel(ApplicationDbContext context)
        {

            Objeto._usuarios = new Usuarios();
            Objeto._context = context;
            Objeto._usersRoles = new UserRoles();
            Objeto._image = new UploadImage();
            Objeto._userRoles = new List<SelectListItem>();

        }



        [BindProperty]

        public InputModel input { get; set; }
        public class InputModel : InputModelCorral
        {




        }



        public InputModelCorral getCorral(int id)
        {

            try
            {
                var corral = Objeto._context.Corrals.Where(u => u.ID.Equals(id)).ToList();

                input = new InputModel
                {


                    id= corral[0].ID.ToString(),
                    numeroCorral = corral[0].numeroCorral,
                    Comentario = corral[0].comentario



                };
            }
            catch (Exception ex)
            {

                input = new InputModel
                {
                    numeroCorral = ex.Message,


                };
            }

            return input;
        }


        public bool Save(string numeroCorral, string comentario, string user)
        {
            Boolean result = false;
            try
            {
                var corralList = Objeto._context.Corrals.Where(u => u.numeroCorral.Equals(numeroCorral)).ToList();
                var userlist = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].Id)).ToList();

                if (corralList.Count() <= 0)
                {

                    var corral = new Corral
                    {

                        numeroCorral = numeroCorral,
                        comentario = comentario,




                    };


                    var log_aplicacion = new Historial_Acciones
                    {
                        IdRole = userList5[0].RoleId,
                        Accion = "Guardar",
                        item = "Corral",
                        fecha_registro = DateTime.Now,
                        IdUser = userlist[0].Id,
                    };


                    Objeto._context.Add(corral);
                    Objeto._context.Add(log_aplicacion);
                    Objeto._context.SaveChanges();





                    result = true;

                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;

            }

            return result;


        }
        public bool updateCorral(int id, string numeroCorral, string comentario)
        {
            var result = false;
            try
            {


                var Corral = Objeto._context.Corrals.Where(g => g.numeroCorral.Equals(numeroCorral)).ToList();

                if (Corral.Count() > 0)
                {

                    if (Corral[0].ID != id)
                    {

                        result = false;

                    }
                    else
                    {
                        var data = Objeto._context.Corrals.Find(Corral[0].ID);
                        var corral = new Corral
                        {

                            ID = id,
                            numeroCorral = numeroCorral,
                            comentario = comentario





                        };


                       Objeto._context.Entry(data).CurrentValues.SetValues(corral);
                        Objeto._context.Update(data);
                        Objeto._context.SaveChanges();
                        result = true;

                    }

                }
                else
                {
                    var corral = new Corral
                    {

                        ID = id,
                        numeroCorral = numeroCorral,
                        comentario = comentario





                    };

                    result = true;
                    Objeto._context.Update(corral);

                    Objeto._context.SaveChanges();

                }









            }
            catch (Exception ex)
            {

                throw ex;
            }

            return result;
        }

    }
}
