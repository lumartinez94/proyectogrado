﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Corrales.Models
{
    public class Corral
    {
        public int ID { get; set; }

        public string comentario { get; set; }

        public string numeroCorral { get; set; }
    }
    
}
