﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Corrales.Models
{
    public class InputModelCorral
    {
       

        [Required(ErrorMessage = "<font color='red'>El campo Comentario es obligatorio.</font>")]

        public string Comentario { get; set; }
        public int ID { get; set; }

        [Required(ErrorMessage = "<font color='red'>El campo numero de corral es obligatorio.</font>")]
        [Display(Name ="Numero de corral")]
        public string numeroCorral { get; set; }

        public string total { get; set; }
        public string id { get; set; }



    }
}
