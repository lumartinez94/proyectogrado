﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Corrales.Models
{
    public class TipoCorral
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
    }
}
