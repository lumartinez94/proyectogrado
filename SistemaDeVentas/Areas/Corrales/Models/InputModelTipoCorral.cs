﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace SistemaDeVentas.Areas.Corrales.Models
{
    public class InputModelTipoCorral
    {
        public int IDCorral { get; set; }

        [Display(Name="Linea de producción")]
        public string NombreTipoCorral { get; set; }
    }
}
