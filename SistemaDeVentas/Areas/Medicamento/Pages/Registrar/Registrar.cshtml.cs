using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.Medicamento.Controllers;
using SistemaDeVentas.Areas.Medicamento.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Medicamento.Pages.Registrar
{
    public class RegistrarModel : PageModel
    {

        private ListObject Objeto = new ListObject();
        private static String idGet = null;
        private static List<IdentityUser> userList1;
        private static List<medicamento> medicamento;
        private static List<IdentityRole> userList5;

        public RegistrarModel(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager, ApplicationDbContext context, IHostingEnvironment environment)
        {
            Objeto._roleManager = roleManager;
            Objeto._usuarios = new Usuarios();
            Objeto._context = context;
            Objeto._usersRoles = new UserRoles();
            Objeto._environment = environment;
            Objeto._image = new UploadImage();
            Objeto._userRoles = new List<SelectListItem>();
            Objeto._userManager = userManager;
        }
        public async Task  OnGet(string id)
        {
            if (id != null)
            {
                idGet = id;
                await setEditAsync(id);
            }


        }


        [BindProperty]
        public InputModel Input { get; set; }
        public class InputModel : InputModelMedicamentos
        {

            [TempData]

            public string ErrorMessage { get; set; }


        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (idGet == null)
            {


                var valor = await Save();
                if (valor)
                {
                    return RedirectToAction(nameof(MedicamentoController.Index), "Medicamento");
                }
                {
                    return Page();
                }
            }
            else
            {
                var valor = await updateUser();
                if (valor)
                {
                    return RedirectToAction(nameof(MedicamentoController.Index), "Medicamento");
                }
                else
                {
                    return Page();
                }
            }

        }


        private async Task<bool> Save()
        {
            var valor = false;
            try
            {
                var user = HttpContext.User.Identity.Name;

                if (ModelState.IsValid)
                {


                  


                    var medicamentos = new medicamento
                    {
                        
                        Nombre= Input.Nombre,
                        Marca= Input.Marca,
                        descripcion= Input.descripcion,
                        fecha_creacion= Input.Fecha,
                        cantidad= Input.cantidad,
                        Precio_unidad= Input.Precio_unidad


                    };


                    await Objeto._context.AddAsync(medicamentos);
                    Objeto._context.SaveChanges();

                    var userlist = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                    var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].Id)).ToList();


                    var log_aplicaciones = new Historial_Acciones
                    {
                        Accion = "Guardar",
                        item = "Medicamento",
                        fecha_registro = DateTime.Now,
                        IdRole = userList5[0].RoleId,
                        IdUser = userlist[0].Id,



                    };

                    await Objeto._context.AddAsync(log_aplicaciones);
                    Objeto._context.SaveChanges();
                    valor = true;

                }


                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Debe completar todos los campos",
                       
                    };
                }


            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                   

                };
                valor = false;
            }
            return valor;
        }


        private async Task<bool> updateUser()

        {
            var valor = false;
            var user = HttpContext.User.Identity.Name;
            var userList = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
            var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userList[0].Id)).ToList();

            try
            {


                if (ModelState.IsValid)
                {
                    var medicamentos = new medicamento
                    {
                        ID = int.Parse(Input.ID),
                       Marca= Input.Marca,
                       Nombre= Input.Nombre,
                       descripcion= Input.descripcion,
                       Precio_unidad= Input.Precio_unidad,
                       cantidad= Input.cantidad,
                       fecha_creacion= Input.Fecha




                    };
                    Objeto._context.Update(medicamentos);
                    await Objeto._context.SaveChangesAsync();





                    var log_aplicaciones = new Historial_Acciones
                    {
                        Accion = "Editar",
                        item = "Medicamento",
                        fecha_registro = DateTime.Now,
                        IdRole = userList5[0].RoleId,
                        IdUser = userList[0].Id,



                    };
                    await Objeto._context.AddAsync(log_aplicaciones);
                    Objeto._context.SaveChanges();


                    valor = true;

                }
                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Debe llenar todos los campos",
                        
                    };
                    valor = false;
                }
            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                    
                };
                valor = false;
            }
            return valor;


        }

        private async Task setEditAsync(string id)
        {
            medicamento = Objeto._context.medicamento.Where(c => c.ID.Equals(int.Parse(id))).ToList();

            Input = new InputModel
            {
                ID = medicamento[0].ID.ToString(),
                cantidad = medicamento[0].cantidad,
                Nombre = medicamento[0].Nombre,
                Fecha = medicamento[0].fecha_creacion,
                Precio_unidad = medicamento[0].Precio_unidad,
                Marca=medicamento[0].Marca,
                descripcion=medicamento[0].descripcion
                




            };
        }





    }
}