using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SistemaDeVentas.Areas.Medicamento.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Medicamento.Pages.Eliminar
{
    [Authorize(Roles = "Admin")]
    public class EliminarModel : PageModel
    {

        private ListObject objeto = new ListObject();
        public static InputModel model;
        public EliminarModel(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context, IHostingEnvironment environment)

        {
            objeto._context = context;
            objeto._environment = environment;
            objeto._image = new UploadImage();
            objeto._usuarios = new Usuarios(userManager, signInManager, roleManager, context);
        }
        public async Task<IActionResult> OnGet(String id)
        {
            if (id != null)
            {
                var data =  objeto._context.medicamento.Find(int.Parse(id));
                Input = new InputModel
                {
                    Marca = data.Marca,
                    Nombre = data.Nombre,
                    descripcion = data.descripcion,
                    cantidad = data.cantidad,
                    Precio_unidad = data.Precio_unidad,
                    Fecha= data.fecha_creacion,
                    ID=data.ID.ToString()

                };
                model = Input;
                return Page();
            }
            else
            {

                return Redirect("/Medicamento?area=Medicamento");
            }
        }
        public InputModel Input { get; set; }
        public class InputModel : InputModelMedicamentos
        {
            [TempData]

            public string ErrorMesssage { get; set; }

        }

        public IActionResult OnPost()
        {
            try
            {

                var users = HttpContext.User.Identity.Name;
                var userList = objeto._context.Users.Where(u => u.Email.Equals(users)).ToList();
                var userList5 = objeto._context.UserRoles.Where(u => u.UserId.Equals(userList[0].Id)).ToList();
                var medicamentos = new medicamento
                {
                    ID = int.Parse(model.ID)
                };

                objeto._context.medicamento.Remove(medicamentos);
                objeto._context.SaveChanges();
               
                


                var log_aplicaciones = new Historial_Acciones
                {
                    Accion = "Eliminar",
                    item = "Medicamentos",
                    fecha_registro = DateTime.Now,
                    IdRole = userList5[0].RoleId,
                    IdUser = userList[0].Id,



                };

                objeto._context.AddAsync(log_aplicaciones);
                objeto._context.SaveChanges();

                return Redirect("/Medicamento?area=Medicamento");
            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMesssage = ex.Message
                };
                return Page();
            }
        }
    }
}