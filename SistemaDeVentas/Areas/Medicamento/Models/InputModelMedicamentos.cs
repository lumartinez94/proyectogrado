﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Medicamento.Models
{
    public class InputModelMedicamentos
    {
        
        

        [Required(ErrorMessage = "<font color='red'> El campo Nombre es obligatorio</font>")]
        [Display(Name = "Nombre del medicamento")]
        public string Nombre { get; set; }

        [Display(Name = "Marca")]
        public string Marca { get; set; }

        [Required(ErrorMessage = "<font color='red'> El campo fecha  es obligatorio</font>")]

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? Fecha { get; set; }

        [Required(ErrorMessage = "<font color='red'> El campo Precio es obligatorio</font>")]
        [Display(Name = "Precio x Unidad")]
        public float Precio_unidad { get; set; }

        [Display(Name = "Descripción")]
        public string descripcion { get; set; }

        [Display(Name = "Cantidad")]
        public int cantidad { get; set; }

        public string Search { get; set; }
        public string ID { get; set; }


       
    }
}
