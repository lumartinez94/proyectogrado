﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Medicamento.Models
{
    public class medicamento
    {
        public int ID { get; set; }

        public string Nombre { get; set; }

        public string Marca { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? fecha_creacion { get; set; }
        
        public float Precio_unidad { get; set; }

        public string descripcion { get; set; }

        public int cantidad { get; set; }

    }
}
