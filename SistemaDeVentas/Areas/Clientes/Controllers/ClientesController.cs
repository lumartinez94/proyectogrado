﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SistemaDeVentas.Areas.Clientes.Models;
using SistemaDeVentas.Controllers;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Clientes.Controllers
{

    [Area("Clientes")]
    [Authorize]
    public class ClientesController : Controller
    {

        private ListObject Objeto = new ListObject();


        public ClientesController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context)
        {
            Objeto._signInManager = signInManager;
            Objeto._usuarios = new Usuarios(userManager, signInManager, roleManager, context);
        }

        public async Task<IActionResult> Index(int id, String Search)
        {


            if (Objeto._signInManager.IsSignedIn(User))
            {
                var url = Request.Scheme + "://" + Request.Host.Value;
                var objects = new Paginador<InputModelsClientes>().paginador(await Objeto._usuarios.getClientesAsync(Search),
                    id, "Clientes", "Clientes", "Index", url);
                var models = new DataPaginador<InputModelsClientes>
                {
                    List = (List<InputModelsClientes>)objects[2],
                    Pagi_info = (String)objects[0],
                    Pagi_navegacion = (String)objects[1],
                    Input = new InputModelsClientes()

                };


                return View(models);
            }
            else
            {

                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

    }

}
    