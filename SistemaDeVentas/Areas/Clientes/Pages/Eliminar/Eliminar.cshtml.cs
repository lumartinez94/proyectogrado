using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SistemaDeVentas.Areas.Clientes.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Clientes.Pages.Eliminar
{


    [Authorize]
    public class EliminarModel : PageModel
    {

        private ListObject objeto = new ListObject();
        public static InputModel model;
        public EliminarModel(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context, IHostingEnvironment environment)

        {
            objeto._context = context;
            objeto._environment = environment;
            objeto._usuarios = new Usuarios(userManager, signInManager, roleManager, context);
        }
        public async Task<IActionResult> OnGet(String id)
        {
            if (id != null)
            {
                var data = await objeto._context.cliente.FindAsync(int.Parse(id));
                Input = new InputModel
                {

                   id_clientes= data.id.ToString(),
                    comentario= data.comentario,
                    nombre= data.nombre,
                    telefono= data.telefono.ToString(),
                    direccion= data.direccion,
                    cedula= data.cedula,
                    fecha= data.fecha
                   



                };
                model = Input;
                return Page();
            }
            else
            {

                return Redirect("/Clientes?area=Clientes");
            }
        }
        public InputModel Input { get; set; }
        public class InputModel : InputModelsClientes
        {
            [TempData]

            public string ErrorMesssage { get; set; }

        }

        public IActionResult OnPost()
        {
            try
            {

                var users = HttpContext.User.Identity.Name;
                var userList = objeto._context.Users.Where(u => u.Email.Equals(users)).ToList();
                var userList5 = objeto._context.UserRoles.Where(u => u.UserId.Equals(userList[0].Id)).ToList();
                var clientes = objeto._context.cliente.Find(int.Parse(model.id_clientes));


                objeto._context.cliente.Remove(clientes);
                objeto._context.SaveChanges();


                var log_aplicaciones = new Historial_Acciones
                {
                    Accion = "Eliminar",
                    item = "Cliente",
                    fecha_registro = DateTime.Now,
                    IdRole = userList5[0].RoleId,
                    IdUser = userList[0].Id,



                };

                objeto._context.AddAsync(log_aplicaciones);
                objeto._context.SaveChanges();

                return Redirect("/Clientes?area=Clientes");
            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMesssage = ex.Message
                };
                return Page();
            }
        }
    }
}
