using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.Clientes.Controllers;
using SistemaDeVentas.Areas.Clientes.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Clientes.Pages.Registrar
{
    public class RegistrarModel : PageModel
    {
        private ListObject Objeto = new ListObject();
        private static String idGet = null;
        private static List<Clientess> listcliente;
     

        public RegistrarModel(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager, ApplicationDbContext context, IHostingEnvironment environment)
        {
            Objeto._roleManager = roleManager;
            Objeto._usuarios = new Usuarios();
            Objeto._context = context;
            Objeto._usersRoles = new UserRoles();
            Objeto._environment = environment;
            Objeto._image = new UploadImage();
            Objeto._userRoles = new List<SelectListItem>();
            Objeto._userManager = userManager;
        }
        public async Task OnGet(string id)
        {
            if (id != null)
            {
                idGet = id;
                await setEditAsync(id);
            }


        }


        [BindProperty]
        public InputModel Input { get; set; }
        public class InputModel : InputModelsClientes
        {

            [TempData]

            public string ErrorMessage { get; set; }


        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (idGet == null)
            {


                var valor = await Save();
                if (valor)
                {
                    return RedirectToAction(nameof(ClientesController.Index), "Clientes");
                }
                {
                    return Page();
                }
            }
            else
            {
                var valor = await updateUser();
                if (valor)
                {
                    idGet = null;
                    return RedirectToAction(nameof(ClientesController.Index), "Clientes");
                }
                else
                {
                    return Page();
                }
            }

        }


        private async Task<bool> Save()
        {
            var valor = false;
            try
            {
                var user = HttpContext.User.Identity.Name;

                var comparar = Objeto._context.cliente.Where(c => c.cedula.Equals(Input.cedula)).ToList();

                if (ModelState.IsValid)
                {

                    if (comparar.Count.Equals(0))
                    {



                        var cliente = new Clientess
                        {

                            nombre = Input.nombre,
                            cedula = Input.cedula,
                            comentario = Input.comentario,
                            fecha = Input.fecha,
                            ciudad = "",
                            correo=Input.correo,
                            pais = "",
                            direccion = Input.direccion,
                            telefono = Int64.Parse(Input.telefono)


                        };


                        await Objeto._context.AddAsync(cliente);
                        Objeto._context.SaveChanges();

                        var userlist = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                        var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].Id)).ToList();





                        var log_aplicaciones = new Historial_Acciones
                        {
                            Accion = "Guardar",
                            item = "Cliente",
                            fecha_registro = DateTime.Now,
                            IdRole = userList5[0].RoleId,
                            IdUser = userlist[0].Id,



                        };

                        await Objeto._context.AddAsync(log_aplicaciones);
                        Objeto._context.SaveChanges();
                        valor = true;

                    }
                    else
                    {
                        Input = new InputModel
                        {
                            ErrorMessage = "El cliente ya se encuentra registrado",

                        };
                        valor = false;
                    }

                }


                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Debe completar todos los campos",

                    };
                }


            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,


                };
                valor = false;
            }
            return valor;
        }


        private async Task<bool> updateUser()

        {
            var valor = false;
            var user = HttpContext.User.Identity.Name;
            var userList = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
            var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userList[0].Id)).ToList();

            try
            {


                if (ModelState.IsValid)
                {
                   
             
                    var cliente = new Clientess
                    {
                        id = int.Parse(Input.id_clientes),
                        nombre = Input.nombre,
                        cedula = Input.cedula,
                        comentario = Input.comentario,
                        fecha = Input.fecha,
                        ciudad = "",
                        correo=  Input.correo,
                        pais = "",
                        direccion = Input.direccion,
                        telefono = Int64.Parse(Input.telefono)




                    };
                    Objeto._context.Update(cliente);
                    await Objeto._context.SaveChangesAsync();





                    var log_aplicaciones = new Historial_Acciones
                    {
                        Accion = "Editar",
                        item = "Cliente",
                        fecha_registro = DateTime.Now,
                        IdRole = userList5[0].RoleId,
                        IdUser = userList[0].Id,



                    };
                    await Objeto._context.AddAsync(log_aplicaciones);
                    Objeto._context.SaveChanges();


                    valor = true;

                }
                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Debe llenar todos los campos",

                    };
                    valor = false;
                }
            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,

                };
                valor = false;
            }
            return valor;


        }

        private async Task setEditAsync(string id)
        {
            listcliente = Objeto._context.cliente.Where(c => c.id.Equals(int.Parse(id))).ToList();

            Input = new InputModel
            {
                id_clientes = listcliente[0].id.ToString(),
                nombre = listcliente[0].nombre,
                cedula = listcliente[0].cedula,
                fecha = listcliente[0].fecha,
               correo=listcliente[0].correo,
                direccion = listcliente[0].direccion,
                comentario= listcliente[0].comentario,
                telefono= listcliente[0].telefono.ToString()





            };
        }





    }
}