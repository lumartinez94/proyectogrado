﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Clientes.Models
{
    public class InputModelsClientes
    {

        public string id_clientes { get; set; }


        [Display(Name ="Nombre")]
        [Required(ErrorMessage = "<font color='red'> El campo nombre es obligatorio</font>")]
        public string nombre { get; set; }

        [Display(Name = "Cedula")]
        [Required(ErrorMessage = "<font color='red'> El campo cedula es obligatorio</font>")]
        public string cedula { get; set; }

        [Display(Name = "Correo")]
        [Required(ErrorMessage = "<font color='red'> El campo correo es obligatorio</font>")]
        public string correo { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? fecha { get; set; }
        [Required(ErrorMessage = "<font color='red'> El campo fecha es obligatorio</font>")]

        [Display(Name = "Telefono")]
        public string telefono { get; set; }
        [Required(ErrorMessage = "<font color='red'> El campo telefono es obligatorio</font>")]

        [Display(Name = "Comentario")]
        public string comentario { get; set; }

        [Display(Name = "Dirección")]
        [Required(ErrorMessage = "<font color='red'> El campo dirección es obligatorio</font>")]
        public string direccion { get; set; }

        public string pais { get; set; }
        public string ciudad { get; set; }

      
        
    }
}
