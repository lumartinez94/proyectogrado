﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Clientes.Models
{
    public class Clientess
    {

        public int id { get; set; }
        public string nombre { get; set; }

        public string cedula { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? fecha { get; set; }

        public string correo { get; set; }
        public Int64 telefono { get; set; }
        public string comentario { get; set; }
        public string direccion { get; set; }
        public string pais { get; set; }
        public string ciudad { get; set; }
    }
}
