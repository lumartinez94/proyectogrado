﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.ControlAlimentos.Models
{
    public class InputModelsAlimentos_cerdos
    {
        [Display(Name = "Linea del alimento")]
        public string id_alimento { get; set; }
        [Display(Name = "#Caravana Cerdo")]
        public string caravana_cerdo { get; set; }

        [Display(Name = "Numero del cerdo")]
        public string id_cerdo { get; set; }

       

       [Display(Name = "#Corral")]
        public string numero_corral { get; set; }

        public string id_usuario { get; set; }
        [Display(Name ="Encargado")]
        public string correoUser { get; set; }

        [Display(Name = "Categoria")]
        public string categoria_alimento { get; set; }


        public string id_alimentos_cerdo { get; set; }


        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        [Display(Name = "Fecha")]
        public DateTime? fecha { get; set; }
        [Display(Name = "Cantidad de kg al día")]
        public string cantidad_kg { get; set; }
    }
}
