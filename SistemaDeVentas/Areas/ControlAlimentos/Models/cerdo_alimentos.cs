﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.ControlAlimentos.Models
{
    public class cerdo_alimentos
    {
        public int id { get; set; }

        public int id_cerdo { get; set; }
        public int id_alimento { get; set; }
        public string cantidad_suministrada { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? fecha { get; set; }

        public string id_user { get; set; }
    }
}
