using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.ControlAlimentos.Controllers;
using SistemaDeVentas.Areas.ControlAlimentos.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.ControlAlimentos.Pages.Registrar
{
    public class RegistrarModel : PageModel
    {

        private ListObject Objeto = new ListObject();
        private static String idGet = null;
        private static List<IdentityUser> userList1;
        private static List<cerdo_alimentos> listControlCerdoAlimentos;
        private static List<IdentityRole> userList5;


        public RegistrarModel(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager, ApplicationDbContext context, IHostingEnvironment environment)
        {
            Objeto._roleManager = roleManager;
            Objeto._usuarios = new Usuarios();
            Objeto._context = context;
            Objeto._usersRoles = new UserRoles();
            Objeto._environment = environment;
            Objeto._image = new UploadImage();
            Objeto._userRoles = new List<SelectListItem>();
            Objeto._userManager = userManager;
        }
        public async Task OnGet(string id)
        {
            if (id != null)
            {
                idGet = id;
                await setEditAsync(id);
            }
            else
            {

                Input = new InputModel
                {

                    cerdosList = getCerdos(),
                    AlimentoList = getAlimentos(),

                };
            }


        }


        [BindProperty]
        public InputModel Input { get; set; }
        public class InputModel : InputModelsAlimentos_cerdos
        {

            [TempData]

            public string ErrorMessage { get; set; }

            public List<SelectListItem> AlimentoList { get; set; }

            public List<SelectListItem> cerdosList { get; set; }


        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (idGet == null)
            {


                var valor = await Save();
                if (valor)
                {
                    return RedirectToAction(nameof(ControlAlimentosController.CerdoAlimentos), "ControlAlimentos");
                }
                {
                    return Page();
                }
            }
            else
            {
                var valor = await updateUser();
                if (valor)
                {
                    idGet = null;
                    return RedirectToAction(nameof(ControlAlimentosController.CerdoAlimentos), "ControlAlimentos");
                }
                else
                {
                    return Page();
                }
            }

        }


        private async Task<bool> Save()
        {
            var valor = false;
            try
            {
                var user = HttpContext.User.Identity.Name;

                var userAsp = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                var userT = Objeto._context.Tusuarios.Where(u => u.IdUser.Equals(userAsp[0].Id)).ToList();
                var userAPP = Objeto._context.AppUsuarios.Where(u => u.IdUser.Equals(userAsp[0].Id)).ToList();


                if (ModelState.IsValid)
                {


                    var Control = new cerdo_alimentos
                    {

                        id_cerdo= int.Parse(Input.id_cerdo),
                        id_alimento= int.Parse(Input.id_alimento),
                        id_user= userAsp[0].Id,
                        cantidad_suministrada= Input.cantidad_kg,
                        fecha= Input.fecha
                        




                    };


                    await Objeto._context.AddAsync(Control);
                    Objeto._context.SaveChanges();





                    var userlist = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                    var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].Id)).ToList();


                    var log_aplicaciones = new Historial_Acciones
                    {
                        Accion = "Guardar",
                        item = "ControlAlimentos",
                        fecha_registro = DateTime.Now,
                        IdRole = userList5[0].RoleId,
                        IdUser = userlist[0].Id,



                    };

                    await Objeto._context.AddAsync(log_aplicaciones);
                    Objeto._context.SaveChanges();
                    valor = true;




                }
                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Debe completar todos los campos",
                        AlimentoList = getAlimentos(),
                        cerdosList = getCerdos(),


                    };
                }





            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                    AlimentoList = getAlimentos(),
                    cerdosList = getCerdos(),



                };
                valor = false;
            }
            return valor;
        }


        public List<SelectListItem> getCerdos(string data = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var hembras = Objeto._context.Cerdos.Where(c => c.id_estado.Equals(1)).ToList();


            if (data != null)
            {

                var busqueda = Objeto._context.Cerdos.Find(int.Parse(data));

                list.Add(new SelectListItem
                {
                    Text = busqueda.codigo,
                    Value = busqueda.ID.ToString()
                });

                hembras.ForEach(item =>
                {

                    if (!item.ID.ToString().Equals(data))
                    {
                        list.Add(new SelectListItem
                        {
                            Value = item.ID.ToString(),
                            Text = item.codigo

                        });

                    }
                });




            }
            else
            {
                hembras.ForEach(item =>
                {
                    list.Add(new SelectListItem
                    {

                        Text = item.codigo,
                        Value = item.ID.ToString()
                    }

                    );

                });
            }


            return list;
        }


        public List<SelectListItem> getAlimentos(string data = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var alimentos = Objeto._context.Alimento.ToList();


            if (data != null)
            {

                var busqueda = Objeto._context.Alimento.Find(int.Parse(data));

                list.Add(new SelectListItem
                {
                    Value = busqueda.Id.ToString(),
                    Text = busqueda.categoria,

                });

                alimentos.ForEach(item =>
                    {

                        if (!item.Id.ToString().Equals(data))
                        {
                            list.Add(new SelectListItem
                            {
                                Value = item.Id.ToString(),
                                Text = item.categoria


                            });

                        }
                    });




            }
            else
            {
                alimentos.ForEach(item =>
                    {
                        list.Add(new SelectListItem
                        {

                            Value = item.Id.ToString(),
                            Text = item.categoria,

                        }

                        );

                    });
            }


            return list;
        }



        private async Task<bool> updateUser()

        {
            var valor = false;
            var user = HttpContext.User.Identity.Name;
            var userAsp = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
            var userList = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
            var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userList[0].Id)).ToList();
         
         

            try
            {


                if (ModelState.IsValid)
                {
                    var Control = new cerdo_alimentos
                    {
                        id= int.Parse(Input.id_alimentos_cerdo),
                        id_cerdo= int.Parse(Input.id_cerdo),
                        id_alimento= int.Parse(Input.id_alimento),
                        id_user= userAsp[0].Id,
                        cantidad_suministrada=Input.cantidad_kg,
                        fecha= Input.fecha






                    };
                    Objeto._context.Update(Control);
                    await Objeto._context.SaveChangesAsync();





                    var log_aplicaciones = new Historial_Acciones
                    {
                        Accion = "Editar",
                        item = "ControlAlimentos",
                        fecha_registro = DateTime.Now,
                        IdRole = userList5[0].RoleId,
                        IdUser = userList[0].Id,



                    };
                    await Objeto._context.AddAsync(log_aplicaciones);
                    Objeto._context.SaveChanges();


                    valor = true;

                }
                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Debe llenar todos los campos",

                        AlimentoList = getAlimentos(listControlCerdoAlimentos[0].id_alimento.ToString()),
                        cerdosList = getCerdos(listControlCerdoAlimentos[0].id_cerdo.ToString())

                    };
                    valor = false;
                }
            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                   AlimentoList = getAlimentos(listControlCerdoAlimentos[0].id_alimento.ToString()),

                  
                    cerdosList = getCerdos(listControlCerdoAlimentos[0].id_cerdo.ToString())

                };
                valor = false;
            }
            return valor;


        }

        private async Task setEditAsync(string id)
        {
            listControlCerdoAlimentos = Objeto._context.cerdo_Alimentos.Where(c => c.id.Equals(int.Parse(id))).ToList();

            Input = new InputModel
            {
                id_alimentos_cerdo= listControlCerdoAlimentos[0].id.ToString(),
                cerdosList= getCerdos(listControlCerdoAlimentos[0].id_cerdo.ToString()),
                AlimentoList= getAlimentos(listControlCerdoAlimentos[0].id_alimento.ToString()),
                cantidad_kg= listControlCerdoAlimentos[0].cantidad_suministrada,
                fecha= listControlCerdoAlimentos[0].fecha








            };
        }






    }
}
