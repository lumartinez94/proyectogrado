﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SistemaDeVentas.Areas.ControlAlimentos.Models;
using SistemaDeVentas.Controllers;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.ControlAlimentos.Controllers
{
    [Area("ControlAlimentos")]
    [Authorize]
    public class ControlAlimentosController : Controller
    {

        private ListObject Objeto = new ListObject();


        public ControlAlimentosController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context)
        {
            Objeto._signInManager = signInManager;
            Objeto._usuarios = new Usuarios(userManager, signInManager, roleManager, context);
        }

        public async Task<IActionResult> CerdoAlimentos(int id, String Search)
        {


            if (Objeto._signInManager.IsSignedIn(User))
            {
                var url = Request.Scheme + "://" + Request.Host.Value;
                var objects = new Paginador<InputModelsAlimentos_cerdos>().paginador(await Objeto._usuarios.getControlAlimentosCerdosAsync(Search),
                    id, "ControlAlimentos", "ControlAlimentos", "CerdoAlimentos", url);
                var models = new DataPaginador<InputModelsAlimentos_cerdos>
                {
                    List = (List<InputModelsAlimentos_cerdos>)objects[2],
                    Pagi_info = (String)objects[0],
                    Pagi_navegacion = (String)objects[1],
                    Input = new InputModelsAlimentos_cerdos()

                };


                return View("~/Areas/ControlAlimentos/Views/CerdoAlimentos/CerdoAlimentos.cshtml",models);
            }
            else
            {

                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }
    }
}