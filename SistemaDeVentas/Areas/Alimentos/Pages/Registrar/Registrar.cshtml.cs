using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.Alimentos.Controllers;
using SistemaDeVentas.Areas.Alimentos.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Alimentos.Pages.Registrar
{
    public class RegistrarModel : PageModel
    {
        private ListObject Objeto = new ListObject();
        private static String idGet = null;
        private static List<IdentityUser> userList1;
        private static List<Alimento> alimentos;
        private static List<IdentityRole> userList5;

        public RegistrarModel(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager, ApplicationDbContext context, IHostingEnvironment environment)
        {
            Objeto._roleManager = roleManager;
            Objeto._usuarios = new Usuarios();
            Objeto._context = context;
            Objeto._usersRoles = new UserRoles();
            Objeto._environment = environment;
            Objeto._image = new UploadImage();
            Objeto._userRoles = new List<SelectListItem>();
            Objeto._userManager = userManager;
        }
        public async Task OnGet(string id)
        {
            if (id != null)
            {
                idGet = id;
                await setEditAsync(id);
            }
            else
            {
                Input = new InputModel
                {
                    alimentacionList = getAlimentacion()
                };


            }
        }


        [BindProperty]
        public InputModel Input { get; set; }
        public class InputModel : InputModelAlimentos
        {

            [TempData]

            public string ErrorMessage { get; set; }
            public List<SelectListItem> alimentacionList { get; set; }


        }

        private List<SelectListItem> getAlimentacion(string data = null)
        {
            List<SelectListItem> ListAlimento = new List<SelectListItem>();

            var fillAlimento = Objeto._context.tipo_Alimentacion.ToList();

            if (data !=null )
            {
                var searchAlimento = Objeto._context.tipo_Alimentacion.Where(al => al.nombre.Equals(data)).ToList();

                ListAlimento.Add(new SelectListItem
                {
                    Value = searchAlimento[0].id.ToString(),
                    Text = searchAlimento[0].nombre,
                });

                fillAlimento.ForEach(item =>
                {
                    if (item.nombre != data)
                    {
                        ListAlimento.Add(new SelectListItem
                        {
                            Value = item.id.ToString(),
                            Text = item.nombre
                        });
                    }

                });
            }
            else
            {


                foreach (var item in fillAlimento)
                {

                    ListAlimento.Add(new SelectListItem
                    {

                        Value = item.id.ToString(),
                        Text = item.nombre
                    });

                }
            }

            return ListAlimento;

        }
        public async Task<IActionResult> OnPostAsync()
        {
            if (idGet == null)
            {


                var valor = await Save();
                if (valor)
                {
                    return RedirectToAction(nameof(AlimentosController.Index), "Alimentos");
                }
                {
                    return Page();
                }
            }
            else
            {
                var valor = await updateUser();
                if (valor)
                {
                    return RedirectToAction(nameof(AlimentosController.Index), "Alimentos");
                }
                else
                {
                    return Page();
                }
            }

        }


       

        private async Task<bool> Save()
        {
            var valor = false;
            try
            {
                var user = HttpContext.User.Identity.Name;

                if (ModelState.IsValid)
                {





                    var Alimentos = new Alimento
                    {

                        Nombre = Input.Nombre,
                        valor_bulto= Input.valor_bulto,
                        Descripcion = Input.Descripcion,
                        Fecha_compra = Input.Fecha_compra,
                        cantidad = Input.cantidad,
                        valor_kg = Input.valor_kg,
                        fecha_registro=DateTime.Now,
                         categoria = Input.categoria,



                    };


                    await Objeto._context.AddAsync(Alimentos);
                    Objeto._context.SaveChanges();

                    var userlist = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
                    var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userlist[0].Id)).ToList();


                    var log_aplicaciones = new Historial_Acciones
                    {
                        Accion = "Guardar",
                        item = "Alimentos",
                        fecha_registro = DateTime.Now,
                        IdRole = userList5[0].RoleId,
                        IdUser = userlist[0].Id,



                    };

                    await Objeto._context.AddAsync(log_aplicaciones);
                    Objeto._context.SaveChanges();
                    valor = true;

                }


                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Debe completar todos los campos",
                        alimentacionList = getAlimentacion()

                    };
                }


            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                    alimentacionList = getAlimentacion()


            };
                valor = false;
            }
            return valor;
        }


        private async Task<bool> updateUser()

        {
            var valor = false;
            var user = HttpContext.User.Identity.Name;
            var userList = Objeto._context.Users.Where(u => u.Email.Equals(user)).ToList();
            var userList5 = Objeto._context.UserRoles.Where(u => u.UserId.Equals(userList[0].Id)).ToList();

            try
            {


                if (ModelState.IsValid)
                {
                    var alimento = new Alimento
                    {
                        
                        Id= int.Parse(Input.Id),
                        Nombre = Input.Nombre,
                        valor_bulto = Input.valor_bulto,
                        Descripcion = Input.Descripcion,
                        Fecha_compra = Input.Fecha_compra,
                        cantidad = Input.cantidad,
                        valor_kg = Input.valor_kg,
                        categoria= Input.categoria,
                        fecha_registro=alimentos[0].fecha_registro,
                        cantidad_existente= alimentos[0].cantidad_existente
                        

                    
                        
                        




                    };
                    Objeto._context.Update(alimento);
                    await Objeto._context.SaveChangesAsync();





                    var log_aplicaciones = new Historial_Acciones
                    {
                        Accion = "Editar",
                        item = "Alimentos",
                        fecha_registro = DateTime.Now,
                        IdRole = userList5[0].RoleId,
                        IdUser = userList[0].Id,



                    };
                    await Objeto._context.AddAsync(log_aplicaciones);
                    Objeto._context.SaveChanges();


                    valor = true;

                }
                else
                {
                    Input = new InputModel
                    {
                        ErrorMessage = "Debe llenar todos los campos",
                        alimentacionList = getAlimentacion()

                    };
                    valor = false;
                }
            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMessage = ex.Message,
                    alimentacionList = getAlimentacion()

                };
                valor = false;
            }
            return valor;


        }

        private async Task setEditAsync(string id)
        {
            alimentos = Objeto._context.Alimento.Where(c => c.Id.Equals(int.Parse(id))).ToList();

            Input = new InputModel
            {
                Id = alimentos[0].Id.ToString(),
                cantidad = alimentos[0].cantidad,
                Nombre = alimentos[0].Nombre,
                Fecha_compra = alimentos[0].Fecha_compra,
                valor_bulto = alimentos[0].valor_kg,
                valor_kg= alimentos[0].valor_kg,
                Descripcion = alimentos[0].Descripcion,
                alimentacionList= getAlimentacion(alimentos[0].categoria),





            };
        }

    }
}