using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SistemaDeVentas.Areas.Alimentos.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Alimentos.Pages.Eliminar
{
    public class EliminarModel : PageModel
    {
        private ListObject objeto = new ListObject();
        public static InputModel model;
        public EliminarModel(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context, IHostingEnvironment environment)

        {
            objeto._context = context;
            objeto._environment = environment;
            objeto._image = new UploadImage();
            objeto._usuarios = new Usuarios(userManager, signInManager, roleManager, context);
        }
        public async Task<IActionResult> OnGet(String id)
        {
            if (id != null)
            {
                var data = objeto._context.Alimento.Find(int.Parse(id));
                Input = new InputModel
                {
                    Id = data.Id.ToString(),
                    Nombre = data.Nombre,
                    Descripcion = data.Descripcion,
                    cantidad = data.cantidad,
                    valor_kg = data.valor_kg,
                    valor_bulto= data.valor_bulto,
                    Fecha_compra = data.Fecha_compra,
                    categoria= data.categoria
                   

                };
                model = Input;
                return Page();
            }
            else
            {

                return Redirect("/Alimentos?area=Alimentos");
            }
        }
        public InputModel Input { get; set; }
        public class InputModel : InputModelAlimentos
        {
            [TempData]

            public string ErrorMesssage { get; set; }

        }

        public IActionResult OnPost()
        {
            try
            {

                var users = HttpContext.User.Identity.Name;
                var userList = objeto._context.Users.Where(u => u.Email.Equals(users)).ToList();
                var userList5 = objeto._context.UserRoles.Where(u => u.UserId.Equals(userList[0].Id)).ToList();
                var alimentos = new Alimento
                {
                    Id= int.Parse(Input.Id)
                };

                objeto._context.Alimento.Remove(alimentos);
                objeto._context.SaveChanges();




                var log_aplicaciones = new Historial_Acciones
                {
                    Accion = "Eliminar",
                    item = "Alimentos",
                    fecha_registro = DateTime.Now,
                    IdRole = userList5[0].RoleId,
                    IdUser = userList[0].Id,



                };

                objeto._context.AddAsync(log_aplicaciones);
                objeto._context.SaveChanges();

                return Redirect("/Alimentos?area=Alimentos");
            }
            catch (Exception ex)
            {

                Input = new InputModel
                {
                    ErrorMesssage = ex.Message
                };
                return Page();
            }
        }
    }
}