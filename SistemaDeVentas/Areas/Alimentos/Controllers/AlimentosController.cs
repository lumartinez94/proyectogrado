﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SistemaDeVentas.Areas.Alimentos.Models;
using SistemaDeVentas.Controllers;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;

namespace SistemaDeVentas.Areas.Alimentos.Controllers
{
    [Area("Alimentos")]
    [Authorize]
    public class AlimentosController : Controller
    {
        
      
            private ListObject Objeto = new ListObject();


            public AlimentosController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context)
            {
                Objeto._signInManager = signInManager;
                Objeto._usuarios = new Usuarios(userManager, signInManager, roleManager, context);
            }

            public async Task<IActionResult> Index(int id, String Search)
            {


                if (Objeto._signInManager.IsSignedIn(User))
                {
                    var url = Request.Scheme + "://" + Request.Host.Value;
                    var objects = new Paginador<InputModelAlimentos>().paginador(await Objeto._usuarios.getAlimentosAsync(Search),
                        id, "Alimentos", "Alimentos", "Index", url);
                    var models = new DataPaginador<InputModelAlimentos>
                    {
                        List = (List<InputModelAlimentos>)objects[2],
                        Pagi_info = (String)objects[0],
                        Pagi_navegacion = (String)objects[1],
                        Input = new InputModelAlimentos()

                    };


                    return View(models);
                }
                else
                {

                    return RedirectToAction(nameof(HomeController.Index), "Home");
                }
            }
        }
}