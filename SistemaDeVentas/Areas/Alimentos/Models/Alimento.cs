﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Alimentos.Models
{
    public class Alimento
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

        public string Descripcion { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? Fecha_compra { get; set; }

        public string categoria { get; set; }

        public string valor_bulto { get; set; }

        public string cantidad { get; set; }
        public string valor_kg { get; set; }

        public string cantidad_existente { get; set; }
        public DateTime fecha_registro { get; set; }
    }
}
