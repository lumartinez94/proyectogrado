﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Areas.Alimentos.Models
{
    public class InputModelAlimentos
    {


        public string Id { get; set; }

        [Required(ErrorMessage = "<font color='red'> El campo Nombre es obligatorio</font>")]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        [Required(ErrorMessage = "<font color='red'> El campo Fecha es obligatorio</font>")]
        [Display(Name = "Fecha")]

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? Fecha_compra { get; set; }

        [Required(ErrorMessage = "<font color='red'> El campo Categoria es obligatorio</font>")]
        [Display(Name = "Categoria")]
        public string categoria { get; set; }

        [Required(ErrorMessage = "<font color='red'> El campo Precio es obligatorio</font>")]
        [Display(Name = "Precio")]
        public string valor_bulto { get; set; }

       
        [Display(Name = "Cantidad")]
        public string cantidad { get; set; }


      
        [Display(Name = "Valor en Kg")]
        public string valor_kg { get; set; }

        public string cantidad_existente { get; set; }
        public DateTime fecha_registro { get; set; }
    }
}
