﻿var getParameterByName = (name) => {
    
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex.exec(location.search);
    // la funcion decodeURIComponent() decodifica un URI
    return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
}