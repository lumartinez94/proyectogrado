﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SistemaDeVentas.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace SistemaDeVentas
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
            //services.AddDefaultIdentity<IdentityUser>()
            //    .AddEntityFrameworkStores<ApplicationDbContext>();
            services.AddIdentity<IdentityUser, IdentityRole>().AddEntityFrameworkStores<ApplicationDbContext>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequiredUniqueChars = 0;
                options.Password.RequiredLength = 6;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
            });
            services.ConfigureApplicationCookie(option =>
            {
                option.Cookie.HttpOnly = true;
                option.ExpireTimeSpan = TimeSpan.FromDays(1);
                option.LoginPath = "/Home/Index";
            });
            services.AddSession(option =>
            {

                option.Cookie.Name = ".SystemVentas.Session";
                option.IdleTimeout = TimeSpan.FromHours(12);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSession();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            //app.UseStatusCodePages();
            app.UseStatusCodePagesWithReExecute("/Home/Error/Error", "?statusCode={0}");
            //app.UseStatusCodePagesWithRedirects("/Home/Error");
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute
                (

                    name: "Principal",
                    template: "{controller=Principal}/{action=Index}/{id?}");


                routes.MapAreaRoute
                ("Users", "Users", "{controller=Users}/{action=Index}/{id?}");

                routes.MapAreaRoute
              ("Cerdos", "Cerdos", "{controller=Cerdos}/{action=Index}/{id?}");

                routes.MapAreaRoute
                ("Corrales", "Corrales", "{controller=Corrales}/{action=Index}/{id?}");

                routes.MapAreaRoute
           ("Medicamento", "Medicamento", "{controller=Medicamento}/{action=Index}/{id?}");

                routes.MapAreaRoute
          ("Clientes", "Clientes", "{controller=Clientes}/{action=Index}/{id?}");

                routes.MapAreaRoute
       ("Alimentos", "Alimentos", "{controller=Alimentos}/{action=Index}/{id?}");

                routes.MapAreaRoute
    ("Ventas", "Ventas", "{controller=Ventas}/{action=Index}/{id?}");

                routes.MapAreaRoute
   ("ControlAlimentos", "ControlAlimentos", "{controller=ControlAlimentos}/{action= CerdoAlimentos}/{id?}");

                routes.MapAreaRoute
 ("ControlMedicamento", "ControlMedicamento", "{controller=ControlMedicamento}/{action=Index}/{id?}");


                routes.MapAreaRoute
 ("Dashboard", "Dashboard", "{controller=Dashboard}/{action=graficoCerdostotales}/{id?}");




            });
        }
    }
}
