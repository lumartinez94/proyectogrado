﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models;
using SistemaDeVentas.Models.Dao;
using SistemaDeVentas.Models.Dao.Clases;
using SistemaDeVentas.Data;


namespace SistemaDeVentas.Controllers
{
    public class HomeController : Controller
    {

        
        private Usuarios usuario;
        private SignInManager<IdentityUser> _signInManager;

        public HomeController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager,ApplicationDbContext context)
        {
            _signInManager = signInManager;
            usuario = new Usuarios(userManager, signInManager, roleManager,context);
            
        }
        public IActionResult Index()

        {
            if (_signInManager.IsSignedIn(User))
            {
                return RedirectToAction(nameof(PrincipalController.Index), "Principal");

            }
            else
            {
                return View();
            }

            
        }

      
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Index(LoginViewModels model)


        {
           
       
            if (ModelState.IsValid)
            {

                List<Object[]> listObject = await usuario.UserLogin(model.input.Email, model.input.Password);

                object[] objects = listObject[0];
                var _identityError = (IdentityError)objects[0];
                model.ErrorMessage = _identityError.Description;
                if (model.ErrorMessage.Equals("True"))
                {
                    //var data = JsonConvert.SerializeObject(objects[1]);
                    //HttpContext.Session.SetString("User",data);
                    

                    return RedirectToAction(nameof(PrincipalController.Index), "Principal");
                    
                }
                else
                {
                    return View(model);
                }
               







            }
            return View(model);

        }
        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private async Task createRoles(IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<IdentityUser>>();
            string[] rolesName = { "Admin", "User" };
            foreach(var item in rolesName){
                var roleExist = await roleManager.RoleExistsAsync(item);
                if (!roleExist)
                {
                    await roleManager.CreateAsync(new IdentityRole(item));
                }

            }
        }
    }
}
