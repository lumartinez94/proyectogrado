﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.Cerdos.DaoCerdos;
using SistemaDeVentas.Areas.Clientes.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models.Compras.Clase;
using SistemaDeVentas.Models.Compras.Dao;
using SistemaDeVentas.Models.Nomina.Clase;
using SistemaDeVentas.Models.Nomina.Dao;
using SistemaDeVentas.Models.Ventas.Clase;
using SistemaDeVentas.Models.Ventas.Dao;

namespace SistemaDeVentas.Controllers
{

    public class data
    {

        public string FileGuid { get; set; }
      public string FileName { get; set; }
    }
    public class ReportesController : Controller
    {

        
        public ActionResult DetailVenta(int id)
        {

            VentasDao dao = new VentasDao();
            Venta clase = new Venta();
            clase.Id = id;

          
            return PartialView(dao.Read(clase));
        }

        public IActionResult Reportes()
        {
            #region ViewBag Sugerencias
            VentasDao dao = new VentasDao();
            List<Clientess> ListClientes = new List<Clientess>();
            ModelState.Clear();
            ListClientes = dao.ReadAllClientes();

            List<SelectListItem> Listac = new List<SelectListItem>();

            foreach (var item in ListClientes)
            {
                Listac.Add(
                new SelectListItem
                {
                    Text = item.cedula + " - " + item.nombre,
                    Value = item.id.ToString()
                });
            }

            ViewBag.cboClientes = Listac;
            #endregion
            return View();
        }





        public ActionResult ReporteGeneral(string f1, string f2)
        {


            #region ViewBag ventas
            VentasDao ventasdao = new VentasDao();
            Venta ventaclase = new Venta();
            List<Venta> ListVenta = new List<Venta>();
            ModelState.Clear();
            ListVenta = ventasdao.readAllCerdosVendidos(f1, f2);

            List<SelectListItem> List = new List<SelectListItem>();
            int suma = 0;
            foreach (var item in ListVenta)
            {
                suma = suma + int.Parse(item.Valor.ToString().Replace(",", ""));


                List.Add(
                new SelectListItem
                {
                    Text = item.TipoVenta,
                    Value = suma.ToString()
                });
            }

            ViewBag.ListVentas = List;

            #endregion



            #region ViewBag Compras
            CompraDao comprasdao = new CompraDao();
            Compra compraclase = new Compra();
            List<Compra> ListCompra = new List<Compra>();
            ModelState.Clear();
            ListCompra = comprasdao.ReadComprasReporte(f1, f2);

            List<SelectListItem> ListCompras = new List<SelectListItem>();
            suma = 0;
            foreach (var item in ListCompra)
            {
                suma = suma + int.Parse(item.ValorIndividual.ToString().Replace(",", ""));


                ListCompras.Add(
                new SelectListItem
                {
                    Text = item.TipoDeCompra,
                    Value = suma.ToString()
                });
            }

            ViewBag.ListCompras = ListCompras;

            #endregion

            #region ViewBag Nomina
            NominaDao nominaDao = new NominaDao();
            Pago nominaClase = new Pago();
            List<Pago> ListPago = new List<Pago>();
            ModelState.Clear();
            ListPago = nominaDao.readAllPagosReporte(f1, f2);

            List<SelectListItem> ListPagos = new List<SelectListItem>();

            foreach (var item in ListPago)
            {
                suma = suma + int.Parse(item.PagoNomina.ToString().Replace(",", ""));


                ListPagos.Add(
                new SelectListItem
                {
                    Text = item.FechaPago,
                    Value = suma.ToString()
                });
            }

            ViewBag.ListPagos = ListPagos;

            #endregion

         
            return PartialView();
        }

        public ActionResult ListadoMaestroVentas(string f1, string f2)
        {


            VentasDao dao = new VentasDao();
           

            return PartialView(dao.readAllReporte(f1,f2));
        }

        public ActionResult ExportButton(string f1, string f2, string id)
        {
            ViewBag.Idcliente = id;
            ViewBag.f1 = f1;
            ViewBag.f2 = f2;


            return PartialView();
        }
        public ActionResult ListadoMaestroVentasCliente(string f1, string f2,string id)
        {



            VentasDao dao = new VentasDao();

            return PartialView(dao.readAllReporteCliente(f1, f2,id));
        }
        public ActionResult ListadoMaestroCompras(string f1, string f2,string tipoCompra=null)
        {


            CompraDao dao = new CompraDao();


            return PartialView(dao.readAllReporte(f1, f2,tipoCompra));
        }


        public ActionResult ListarNetoCompras(string f1, string f2, string tipoCompra = null)
        {


            CompraDao dao = new CompraDao();


            return PartialView(dao.totalcompra(f1, f2, tipoCompra));
        }
        public ActionResult ListadoMaestroNomina (string f1, string f2)
        {


            NominaDao dao = new NominaDao();


            return PartialView(dao.readAllNominaReporte(f1, f2));
        }

        public ActionResult Poblacion(string f1, string f2)
        {

            DaoPoblacion poblacion = new DaoPoblacion();

            return PartialView(poblacion.getPoblation(f1,f2));


        }

        [HttpPost]
        public FileResult ExportReporteCliente(string f1, string f2, string id)
        {

            VentasDao dao = new VentasDao();
            DataTable dt = new DataTable();

            dt = dao.ExportReportByClient(f1, f2, id);


            XLWorkbook workbook = new XLWorkbook();
            var ws = workbook.Worksheets.Add("Excel_Export");

            int col = 1;
            foreach (DataColumn dc in dt.Columns)
            {
                ws.Cell(1, col).Value = dc.ColumnName;
                col = col + 1;
            }

            int row = 2;
            foreach (DataRow rows in dt.Rows)
            {
                col = 1;
                foreach (DataColumn cols in dt.Columns)
                {
                    ws.Cell(row, col).Value = rows[cols.ColumnName];
                    col = col + 1;
                }
                row = row + 1;
            }



            using (MemoryStream stream = new MemoryStream())
            {

                ws.Columns().AdjustToContents();

                workbook.SaveAs(stream);
                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Ventas_Cliente N°-" + id + ".xlsx");
            }




        }

    [HttpGet]
        public FileResult ExportCompras(string f1, string f2, string tipoCompra)
       {

            var compras = new CompraDao();
            

            
            DataTable dt = new DataTable();

            dt = compras.CompraTable(f1, f2, tipoCompra);


            XLWorkbook workbook = new XLWorkbook();
            var ws = workbook.Worksheets.Add("Excel_Export");

            int col = 1;
            foreach (DataColumn dc in dt.Columns)
            {
                ws.Cell(1, col).Value = dc.ColumnName;
                col = col + 1;
            }

            int row = 2;
            foreach (DataRow rows in dt.Rows)
            {
                col = 1;
                foreach (DataColumn cols in dt.Columns)
                {
                    ws.Cell(row, col).Value = rows[cols.ColumnName];
                    col = col + 1;
                }
                row = row + 1;
            }



            using (MemoryStream stream = new MemoryStream())
            {

                ws.Columns().AdjustToContents();

                workbook.SaveAs(stream);
                if (tipoCompra != null)
                {

                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ReporteCompra_ "+tipoCompra+"_"+ f1 + "-" + f2 + ".xlsx");
                }
                else
                {
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ReporteCompra_" + f1 + "-" + f2 + ".xlsx");
                }
            }


        }

        [HttpPost]
        public FileResult ExportReporteFactura(string id, string nombre, string fecha)
        {

            VentasDao dao = new VentasDao();
            DataTable dt = new DataTable();

            dt = dao.ExportReportFactura(id, nombre, fecha);


            XLWorkbook workbook = new XLWorkbook();
            var ws = workbook.Worksheets.Add("Excel_Export");

            int col = 1;
            foreach (DataColumn dc in dt.Columns)
            {
                ws.Cell(1, col).Value = dc.ColumnName;
                col = col + 1;
            }

            int row = 2;
            foreach (DataRow rows in dt.Rows)
            {
                col = 1;
                foreach (DataColumn cols in dt.Columns)
                {
                    ws.Cell(row, col).Value = rows[cols.ColumnName];
                    col = col + 1;
                }
                row = row + 1;
            }



            using (MemoryStream stream = new MemoryStream())
            {

                ws.Columns().AdjustToContents();

                workbook.SaveAs(stream);
                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Factura Cliente: " + nombre + " Fecha: " + fecha + ".xlsx");
            }




        }
    }
}