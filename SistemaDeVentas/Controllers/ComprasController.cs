﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Models.Compras.Clase;
using SistemaDeVentas.Models.Compras.Dao;

namespace SistemaDeVentas.Controllers
{
    public class ComprasController : Controller
    { 
        public IActionResult Compras()
        {
            return View();
        }

        public ActionResult ListadoMaestro()
        {
            

            CompraDao dao = new CompraDao();


            return PartialView(dao.readAll());
        }
        public ActionResult Detail(int id)
        {
            // CityDao sbd = new CityDao();


            CompraDao dao = new CompraDao();
            Compra clase = new Compra();
            clase.Id = id;


            #region ViewBag Sugerencias

            List<Compra> ListCompras = new List<Compra>();            
            ModelState.Clear();
            ListCompras = dao.readComprasGroup();

            List<SelectListItem> List = new List<SelectListItem>();
          
            foreach (var item in ListCompras)
            {
                List.Add(
                new SelectListItem
                {
                    Text = item.Nombre,
                    Value = item.Nombre
                });
            }

            ViewBag.cboCompras = List;

            #endregion

            return PartialView(dao.Read(clase));
        }


        [HttpPost]
        public JsonResult Salvar(string cbounidad, string cbolineamiento, string cbocategoria, string marca, int Id, string Nombre, string Descripcion, string TipoDeCompra, string ValorIndividual, int Cantidad)
        {
            string res = string.Empty;
            res = Nombre;

            CompraDao dao = new CompraDao();

            Compra clase = new Compra();
            clase.Id = Id;
            clase.Nombre = Nombre;
            clase.Descripcion = Descripcion;
            clase.TipoDeCompra = TipoDeCompra;
            clase.ValorIndividual = ValorIndividual;
            clase.Cantidad = Cantidad;
           
            try
            {

                if (clase.Id == 0)
                {
                    dao.Insert(clase);
                    if (clase.TipoDeCompra.Trim() == "Alimento")
                    {
                        dao.InsertAlimento(clase, cbolineamiento,  cbocategoria, cbounidad);
                    }

                    if (clase.TipoDeCompra.Trim() == "Medicamento")
                    {
                        dao.InsertMedicamento(clase, marca);
                    }



                }
                else
                {
                    dao.Update(clase);
                    int idcompra = Id;
                    if (clase.TipoDeCompra.Trim() == "Alimento")
                    {
                        dao.UpdateAlimento(idcompra, clase, cbolineamiento, cbocategoria, cbounidad);
                    }

                    if (clase.TipoDeCompra.Trim() == "Medicamento")
                    {
                        dao.UpdatetMedicamento(idcompra, clase, marca);
                    }


                }

                res = "Inserted";
            }
            catch (Exception)
            {
                res = "failed";
            }
            return Json(res);

        }
     


    }
}