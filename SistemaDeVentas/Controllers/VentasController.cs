﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Areas.Cerdos.Models;
using SistemaDeVentas.Areas.Clientes.Models;
using SistemaDeVentas.Areas.Corrales.Models;
using SistemaDeVentas.Models.Ventas.Clase;
using SistemaDeVentas.Models.Ventas.Dao;
using Corral = SistemaDeVentas.Models.Ventas.Clase.Corral;

namespace SistemaDeVentas.Controllers
{
    public class VentasController : Controller
    {
        public IActionResult Ventas()
        {
            return View();
        }

        public ActionResult ListadoMaestro()
        {


            VentasDao dao = new VentasDao();


            return PartialView(dao.readAll());
        }
        public JsonResult Salvar(List<Venta> pListaDetalle,string cliente,string observacion,string total)
        {
            string res = string.Empty;
            res = "";
            try
            {
                VentasDao dao = new VentasDao();
            if (dao.InsertVenta(pListaDetalle, cliente, observacion, total)) {
                res = "Inserted";

            }
            else
            {
                res = "failed";

            }


         

                
            }
            catch (Exception)
            {
                res = "failed";
            }
            return Json(res);

        }
        public ActionResult Detail(int id)
        {

            #region ViewBag Sugerencias
            VentasDao dao = new VentasDao();
            List<Corral> ListCorrals = new List<Corral>();
            ModelState.Clear();
            ListCorrals = dao.ReadAllCorrals();

            List<SelectListItem> List = new List<SelectListItem>();

            foreach (var item in ListCorrals)
            {
                List.Add(
                new SelectListItem
                {
                    Text = item.numeroCorral,
                    Value = item.ID.ToString()
                });
            }

            ViewBag.cboCorrales = List;

            #endregion



            #region ViewBag Sugerencias
         
            List<Clientess> ListClientes = new List<Clientess>();
            ModelState.Clear();
            ListClientes = dao.ReadAllClientes();

            List<SelectListItem> Listac = new List<SelectListItem>();

            foreach (var item in ListClientes)
            {
                Listac.Add(
                new SelectListItem
                {
                    Text = item.nombre,
                    Value = item.id.ToString()
                });
            }

            ViewBag.cboClientes = Listac;

            #endregion
            return PartialView();
        }
       public ActionResult DetailVenta(int id)
        {

            VentasDao dao = new VentasDao();
            Venta clase = new Venta();
            clase.Id = id;
            return PartialView(dao.Read(clase));
        }

        public JsonResult anular(int id)
        {
            string res = string.Empty;
            res = "";
            try
            {
                VentasDao dao = new VentasDao();
                if (dao.anular(id))
                {
                    res = "Inserted";

                }
                else
                {
                    res = "failed";

                }





            }
            catch (Exception)
            {
                res = "failed";
            }
            return Json(res);

        }
        public ActionResult DetailVentacerdos(int id)
        {

            VentasDao dao = new VentasDao();
            
            return PartialView(dao.readAllCerdosVendidos(id));
        }

        public ActionResult DetailCerdo(int id)
        {


            VentasDao dao = new VentasDao();
            Cerdoss clase = new Cerdoss();
            clase.ID = id;
            return PartialView(dao.DetalleCerdo(clase));
        }


        public ActionResult ListadoCerdoPorCorral(int id)
        {

            #region ViewBag Sugerencias
            VentasDao dao = new VentasDao();
            List<Cerdo> ListCerdos = new List<Cerdo>();
            ModelState.Clear();
            ListCerdos = dao.ReadCerdosByCorral(id);

            List<SelectListItem> List = new List<SelectListItem>();

            foreach (var item in ListCerdos)
            {
                List.Add(
                new SelectListItem
                {
                    Text = item.codigo,
                    Value = item.ID.ToString()
                });
            }

            ViewBag.cboCerdosPorCorral= List;

            #endregion
            return PartialView();
        }

        public ActionResult ListadoCerdoPorCorralCriterio(int id,string criterio)
        {

            #region ViewBag Sugerencias
            VentasDao dao = new VentasDao();
            List<Cerdo> ListCerdos = new List<Cerdo>();
            ModelState.Clear();
            ListCerdos = dao.ReadCerdosByCorralCriterio(id,criterio);

            List<SelectListItem> List = new List<SelectListItem>();

            foreach (var item in ListCerdos)
            {
                List.Add(
                new SelectListItem
                {
                    Text = item.codigo,
                    Value = item.ID.ToString()
                });
            }

            ViewBag.cboCerdosPorCorral = List;

            #endregion
            return PartialView();
        }

    }
}