﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SistemaDeVentas.Models.Nomina.Clase;
using SistemaDeVentas.Models.Nomina.Dao;

namespace SistemaDeVentas.Controllers
{
    public class NominaController : Controller
    {
        public IActionResult Nomina()
        {
            return View();
        }

       
           

            public ActionResult ListadoMaestro()
            {


                NominaDao dao = new NominaDao();


                return PartialView(dao.readAll());
            }

                public ActionResult PagosPorEmpleado(int id)
                {


                    NominaDao dao = new NominaDao();

            Pago clase = new Pago();
            clase.IdEmpleado = id;
                    return PartialView(dao.readAllPagos(clase));
                }
        public ActionResult Detail(int id)
            {

            NominaDao dao = new NominaDao();
            Empleado clase = new Empleado();
            clase.IdEmpleado = id;

            return PartialView(dao.Detalle(clase));
            }


            [HttpPost]
            public JsonResult Salvar(int IdEmpleado, string Identificacion, string Nombre, string Cargo, string Salario)
            {

            NominaDao dao = new NominaDao();
            string res = "";

                try
                {

                    if (IdEmpleado == 0)
                    {
                    res= dao.GuardarEmpleado( Identificacion,  Nombre,  Cargo,  Salario);
                       



                    }
                    else
                    {
                    res = dao.ActualizarEmpleado(IdEmpleado,Identificacion, Nombre, Cargo, Salario);
                       


                    }

                    
                }
                catch (Exception)
                {
                    res = "failed";
                }
                return Json(res);

            }

        [HttpPost]
        public JsonResult PagarTodosEmpleados()
        {

            NominaDao dao = new NominaDao();
            string res = "";

            try
            {
                    res = dao.PagarTodosEmpleados();


            }
            catch (Exception)
            {
                res = "failed";
            }
            return Json(res);

        }

        [HttpPost]
        public JsonResult DevolverPago()
        {

            NominaDao dao = new NominaDao();
            string res = "";

            try
            {
                res = dao.DevolverPago();


            }
            catch (Exception)
            {
                res = "failed";
            }
            return Json(res);

        }

        [HttpPost]
        public JsonResult ActivarEmpleado(int IdEmpleado)
        {

            NominaDao dao = new NominaDao();
            string res = "";

            try
            {
                res = dao.ActivarEmpleado(IdEmpleado);


            }
            catch (Exception)
            {
                res = "failed";
            }
            return Json(res);

        }

        [HttpPost]
        public JsonResult DesactivarEmpleado(int IdEmpleado)
        {

            NominaDao dao = new NominaDao();
            string res = "";

            try
            {
                res = dao.DesactivarEmpleado(IdEmpleado);


            }
            catch (Exception)
            {
                res = "failed";
            }
            return Json(res);

        }

    }
    }