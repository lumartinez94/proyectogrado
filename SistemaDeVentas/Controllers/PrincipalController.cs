﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SistemaDeVentas.Library;
using SistemaDeVentas.Models.Dao;
using SistemaDeVentas.Models.Dao.Clases;

namespace SistemaDeVentas.Controllers
{
    [Authorize]
    public class PrincipalController : Controller
    {

        private Usuarios _usuarios;
        private SignInManager<IdentityUser> _signInManager;
        public PrincipalController(SignInManager<IdentityUser> signInManager)
        {
            _signInManager = signInManager;
            _usuarios = new Usuarios();
           
        }
        public IActionResult Index()
        {
            if (_signInManager.IsSignedIn(User))
            {

                
                return View();
            }
            else { 

                return RedirectToAction(nameof(HomeController.Index), "Home");
                }
        }
    }
}