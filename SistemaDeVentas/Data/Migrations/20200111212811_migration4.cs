﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaDeVentas.Data.Migrations
{
    public partial class migration4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "IdTUsuario",
                table: "AppUsuarios",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "AppUsuarios",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IdUser",
                table: "AppUsuarios",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "AppUsuarios");

            migrationBuilder.DropColumn(
                name: "IdUser",
                table: "AppUsuarios");

            migrationBuilder.AlterColumn<string>(
                name: "IdTUsuario",
                table: "AppUsuarios",
                nullable: true,
                oldClrType: typeof(int));
        }
    }
}
