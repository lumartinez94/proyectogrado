﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaDeVentas.Data.Migrations
{
    public partial class migration6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Ventas",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    precioKg = table.Column<float>(nullable: false),
                    peso = table.Column<double>(nullable: false),
                    tipo_venta = table.Column<string>(nullable: true),
                    id_cerdo = table.Column<string>(nullable: true),
                    id_cliente = table.Column<string>(nullable: true),
                    idTusuario = table.Column<string>(nullable: true),
                    idAppUser = table.Column<string>(nullable: true),
                    precio_total = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ventas", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Ventas");
        }
    }
}
