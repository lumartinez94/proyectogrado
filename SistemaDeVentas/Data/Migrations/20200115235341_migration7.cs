﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaDeVentas.Data.Migrations
{
    public partial class migration7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IdTusuario",
                table: "Historial_Acciones",
                newName: "IdUser");

            migrationBuilder.AddColumn<DateTime>(
                name: "fecha_destete",
                table: "Cerdos",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<float>(
                name: "peso",
                table: "Cerdo_Razas",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<string>(
                name: "sexo",
                table: "Cerdo_Razas",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "fecha_destete",
                table: "Cerdos");

            migrationBuilder.DropColumn(
                name: "peso",
                table: "Cerdo_Razas");

            migrationBuilder.DropColumn(
                name: "sexo",
                table: "Cerdo_Razas");

            migrationBuilder.RenameColumn(
                name: "IdUser",
                table: "Historial_Acciones",
                newName: "IdTusuario");
        }
    }
}
