﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaDeVentas.Data.Migrations
{
    public partial class migration10 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_linea_Produccion",
                table: "linea_Produccion");

            migrationBuilder.RenameTable(
                name: "linea_Produccion",
                newName: "Linea_Produccion");

            migrationBuilder.RenameColumn(
                name: "nombre",
                table: "Cerdos",
                newName: "status");

            migrationBuilder.RenameColumn(
                name: "IdEstado",
                table: "Cerdos",
                newName: "origen");

            migrationBuilder.AddColumn<string>(
                name: "numeroCorral",
                table: "Corrals",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "caravana",
                table: "Cerdos",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "castrado",
                table: "Cerdos",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "id_alimentacion",
                table: "Cerdos",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "id_corral",
                table: "Cerdos",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "id_linea_produccion",
                table: "Cerdos",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "id_raza",
                table: "Cerdos",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "id_user",
                table: "Cerdos",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "numero_tetas",
                table: "Cerdos",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "observaciones",
                table: "Cerdos",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Linea_Produccion",
                table: "Linea_Produccion",
                column: "id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Linea_Produccion",
                table: "Linea_Produccion");

            migrationBuilder.DropColumn(
                name: "numeroCorral",
                table: "Corrals");

            migrationBuilder.DropColumn(
                name: "caravana",
                table: "Cerdos");

            migrationBuilder.DropColumn(
                name: "castrado",
                table: "Cerdos");

            migrationBuilder.DropColumn(
                name: "id_alimentacion",
                table: "Cerdos");

            migrationBuilder.DropColumn(
                name: "id_corral",
                table: "Cerdos");

            migrationBuilder.DropColumn(
                name: "id_linea_produccion",
                table: "Cerdos");

            migrationBuilder.DropColumn(
                name: "id_raza",
                table: "Cerdos");

            migrationBuilder.DropColumn(
                name: "id_user",
                table: "Cerdos");

            migrationBuilder.DropColumn(
                name: "numero_tetas",
                table: "Cerdos");

            migrationBuilder.DropColumn(
                name: "observaciones",
                table: "Cerdos");

            migrationBuilder.RenameTable(
                name: "Linea_Produccion",
                newName: "linea_Produccion");

            migrationBuilder.RenameColumn(
                name: "status",
                table: "Cerdos",
                newName: "nombre");

            migrationBuilder.RenameColumn(
                name: "origen",
                table: "Cerdos",
                newName: "IdEstado");

            migrationBuilder.AddPrimaryKey(
                name: "PK_linea_Produccion",
                table: "linea_Produccion",
                column: "id");
        }
    }
}
