﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaDeVentas.Data.Migrations
{
    public partial class migration3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AppUsuarios",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IdTUsuario = table.Column<string>(nullable: true),
                    telefono = table.Column<string>(nullable: true),
                    password = table.Column<string>(nullable: true),
                    fecha_registro = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppUsuarios", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Cerdo_Razas",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IdCerdo = table.Column<string>(nullable: true),
                    IdRaza = table.Column<string>(nullable: true),
                    fecha_registro = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cerdo_Razas", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Cerdos",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    codigo = table.Column<string>(nullable: true),
                    nombre = table.Column<string>(nullable: true),
                    fecha_registro = table.Column<DateTime>(nullable: false),
                    Peso = table.Column<string>(nullable: true),
                    Sexo = table.Column<string>(nullable: true),
                    color = table.Column<string>(nullable: true),
                    fecha_nacimiento = table.Column<string>(nullable: true),
                    IdEstado = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cerdos", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Corrals",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Descripcion = table.Column<string>(nullable: true),
                    Tipo = table.Column<string>(nullable: true),
                    Numero_Corral = table.Column<string>(nullable: true),
                    id_cerdo = table.Column<string>(nullable: true),
                    fecha_registro = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Corrals", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Estados",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: true),
                    Descripcion = table.Column<string>(nullable: true),
                    fecha_registro = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Estados", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Hembras",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NumeroTetas = table.Column<string>(nullable: true),
                    IdCerdo = table.Column<string>(nullable: true),
                    fecha_registro = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hembras", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Historial_Acciones",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    fecha_registro = table.Column<DateTime>(nullable: false),
                    Accion = table.Column<string>(nullable: true),
                    IdTusuario = table.Column<string>(nullable: true),
                    IdRole = table.Column<string>(nullable: true),
                    item = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Historial_Acciones", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Machos",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IdCerdo = table.Column<string>(nullable: true),
                    castracion = table.Column<string>(nullable: true),
                    fecha_registro = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Machos", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Razas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(nullable: true),
                    Descripcion = table.Column<string>(nullable: true),
                    fecha_registro = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Razas", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppUsuarios");

            migrationBuilder.DropTable(
                name: "Cerdo_Razas");

            migrationBuilder.DropTable(
                name: "Cerdos");

            migrationBuilder.DropTable(
                name: "Corrals");

            migrationBuilder.DropTable(
                name: "Estados");

            migrationBuilder.DropTable(
                name: "Hembras");

            migrationBuilder.DropTable(
                name: "Historial_Acciones");

            migrationBuilder.DropTable(
                name: "Machos");

            migrationBuilder.DropTable(
                name: "Razas");
        }
    }
}
