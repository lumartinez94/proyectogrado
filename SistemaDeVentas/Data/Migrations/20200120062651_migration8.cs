﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaDeVentas.Data.Migrations
{
    public partial class migration8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Cerdo_Razas");

            migrationBuilder.DropTable(
                name: "Hembras");

            migrationBuilder.DropTable(
                name: "Machos");

            migrationBuilder.DropColumn(
                name: "Descripcion",
                table: "Corrals");

            migrationBuilder.DropColumn(
                name: "Numero_Corral",
                table: "Corrals");

            migrationBuilder.DropColumn(
                name: "Tipo",
                table: "Corrals");

            migrationBuilder.DropColumn(
                name: "fecha_registro",
                table: "Corrals");

            migrationBuilder.RenameColumn(
                name: "id_cerdo",
                table: "Corrals",
                newName: "comentario");

            migrationBuilder.AddColumn<int>(
                name: "IdTipo",
                table: "Corrals",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "tipoCorrals",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tipoCorrals", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tipoCorrals");

            migrationBuilder.DropColumn(
                name: "IdTipo",
                table: "Corrals");

            migrationBuilder.RenameColumn(
                name: "comentario",
                table: "Corrals",
                newName: "id_cerdo");

            migrationBuilder.AddColumn<string>(
                name: "Descripcion",
                table: "Corrals",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Numero_Corral",
                table: "Corrals",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tipo",
                table: "Corrals",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "fecha_registro",
                table: "Corrals",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateTable(
                name: "Cerdo_Razas",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IdCerdo = table.Column<string>(nullable: true),
                    IdRaza = table.Column<string>(nullable: true),
                    fecha_registro = table.Column<DateTime>(nullable: false),
                    peso = table.Column<float>(nullable: false),
                    sexo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cerdo_Razas", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Hembras",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IdCerdo = table.Column<string>(nullable: true),
                    NumeroTetas = table.Column<string>(nullable: true),
                    fecha_registro = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hembras", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Machos",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IdCerdo = table.Column<string>(nullable: true),
                    castracion = table.Column<string>(nullable: true),
                    fecha_registro = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Machos", x => x.ID);
                });
        }
    }
}
