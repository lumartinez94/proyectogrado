﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaDeVentas.Data.Migrations
{
    public partial class migration11 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "fecha_registro",
                table: "Cerdos",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<string>(
                name: "fecha_destete",
                table: "Cerdos",
                nullable: true,
                oldClrType: typeof(DateTime));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "fecha_registro",
                table: "Cerdos",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "fecha_destete",
                table: "Cerdos",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
