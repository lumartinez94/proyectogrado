﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SistemaDeVentas.Data;

namespace SistemaDeVentas.Data.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    partial class ApplicationDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.8-servicing-32085")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("SistemaDeVentas.Areas.Cerdos.Models.Cerdo", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Peso");

                    b.Property<string>("Sexo");

                    b.Property<string>("castrado");

                    b.Property<string>("codigo");



                    b.Property<string>("fecha_destete");

                    b.Property<string>("fecha_nacimiento");

                    b.Property<string>("fecha_registro");

                    b.Property<int>("id_alimentacion");

                    b.Property<int>("id_corral");

                    b.Property<int>("id_linea_produccion");

                    b.Property<int>("id_raza");

                    b.Property<string>("id_user");

                    b.Property<string>("numero_tetas");

                    b.Property<string>("observaciones");

                    b.Property<int>("id_estado");

                    b.Property<int>("id_Venta");

                    b.HasKey("ID");

                    b.ToTable("Cerdos");
                });


            modelBuilder.Entity("SistemaDeVentas.Areas.Cerdos.Models.Estado", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Nombre");


                    b.HasKey("ID");

                    b.ToTable("Estados");
                });

            modelBuilder.Entity("SistemaDeVentas.Areas.Cerdos.Models.Traslado", b =>
            {
                b.Property<int>("id")
                    .ValueGeneratedOnAdd()
                    .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                b.Property<string>("causas");
                b.Property<DateTime>("fecha");
                b.Property<int>("id_corral");
                b.Property<int>("id_cerdo");


                b.HasKey("id");

                b.ToTable("Traslado");
            });


            modelBuilder.Entity("SistemaDeVentas.Areas.Cerdos.Models.cerdo_alimentos", b =>
            {
                b.Property<int>("id")
                    .ValueGeneratedOnAdd()
                    .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                b.Property<int>("id_alimento");

                b.Property<int>("id_cerdo");
               
                b.Property<string>("cantidad_suministrada");

                b.Property<string>("id_user");

                b.Property<DateTime>("fecha");


                b.HasKey("id");

                b.ToTable("cerdo_alimentos");
            });

            modelBuilder.Entity("SistemaDeVentas.Areas.Cerdos.Models.Cerdo_Medicamento", b =>
            {
                b.Property<int>("id")
                    .ValueGeneratedOnAdd()
                    .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                b.Property<int>("id_medicamento");

                b.Property<int>("id_cerdo");
                b.Property<int>("id_corral");
                b.Property<int>("id_lote");

                b.Property<string>("cantidad_suministrada");

                b.Property<string>("id_usuario");

                b.Property<DateTime>("fecha");

                b.HasKey("id");

                b.ToTable("Cerdo_Medicamento");
            });

            modelBuilder.Entity("SistemaDeVentas.Areas.Cerdos.Models.Gestacion", b =>
            {
                b.Property<int>("id")
                    .ValueGeneratedOnAdd()
                    .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                b.Property<int>("id_cerdo");

                b.Property<string>("estado");

                b.Property<DateTime>("fecha_aproximada");
                b.Property<DateTime>("fecha_inicio");
                b.Property<DateTime>("fecha_real");

                b.HasKey("id");

                b.ToTable("Gestacion");
            });

            modelBuilder.Entity("SistemaDeVentas.Areas.Cerdos.Models.Raza", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Descripcion");

                    b.Property<string>("Nombre");

                    b.Property<DateTime>("fecha_registro");

                    b.HasKey("Id");

                    b.ToTable("Razas");
                });

            modelBuilder.Entity("SistemaDeVentas.Areas.Corrales.Models.Corral", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("comentario");

                    b.Property<string>("numeroCorral");

                    b.HasKey("ID");

                    b.ToTable("Corrals");
                });

            modelBuilder.Entity("SistemaDeVentas.Areas.Corrales.Models.TipoCorral", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Nombre");

                    b.HasKey("ID");

                    b.ToTable("tipoCorrals");
                });

            modelBuilder.Entity("SistemaDeVentas.Areas.Users.Models.AppUsuarios", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Email");

                    b.Property<int>("IdTUsuario");

                    b.Property<string>("IdUser");

                    b.Property<DateTime>("fecha_registro");

                    b.Property<string>("password");

                    b.Property<string>("telefono");

                    b.HasKey("ID");

                    b.ToTable("AppUsuarios");
                });

            modelBuilder.Entity("SistemaDeVentas.Areas.Users.Models.TUsuarios", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Apellido");

                    b.Property<string>("Cedula");

                    b.Property<string>("IdUser");

                    b.Property<string>("Image");

                    b.Property<string>("Nombre");

                    b.HasKey("ID");

                    b.ToTable("Tusuarios");
                });

            modelBuilder.Entity("SistemaDeVentas.Areas.Ventas.Models.VentasU", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("idAppUser");

                    b.Property<int>("idTusuario");

                    b.Property<int>("id_cerdo");

                    b.Property<int>("id_cliente");

                    b.Property<string>("peso");

                    b.Property<string>("precioKg");

                    b.Property<string>("precio");

                    b.Property<string>("precio_total");

                    b.Property<string>("tipo_venta");
                    b.Property<string>("id_user");

                    b.Property<DateTime>("fecha");

                    b.Property<string>("Observacion");

                    b.HasKey("ID");

                    b.ToTable("Ventas");
                });

            modelBuilder.Entity("SistemaDeVentas.Areas.Ventas.Models.Clientess", b =>
            {
                b.Property<int>("id")
                    .ValueGeneratedOnAdd()
                    .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                b.Property<string>("nombre");

                b.Property<string>("ciudad");

                b.Property<string>("pais");

                b.Property<string>("cedula");

                b.Property<string>("correo");

                b.Property<Int64>("telefono");

                b.Property<string>("direccion");
                b.Property<string>("comentario");

                b.Property<DateTime>("fecha");



                b.HasKey("id");

                b.ToTable("cliente");
            });

            modelBuilder.Entity("SistemaDeVentas.Models.Historial_Acciones", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Accion");

                    b.Property<string>("IdRole");

                    b.Property<string>("IdUser");

                    b.Property<DateTime>("fecha_registro");

                    b.Property<string>("item");

                    b.HasKey("ID");

                    b.ToTable("Historial_Acciones");
                });

            modelBuilder.Entity("SistemaDeVentas.Models.linea_produccion", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("nombre");

                    b.HasKey("id");

                    b.ToTable("Linea_Produccion");
                });

            modelBuilder.Entity("SistemaDeVentas.Models.tipo_alimentacion", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("nombre");

                    b.HasKey("id");

                    b.ToTable("tipo_Alimentacion");
                });

            modelBuilder.Entity("SistemaDeVentas.Models.medicamento", b =>
            {
                b.Property<int>("id")
                    .ValueGeneratedOnAdd()
                    .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                b.Property<string>("nombre");
                b.Property<string>("descripcion");
                b.Property<string>("marca");
                b.Property<DateTime>("fecha_creacion");
                b.Property<int>("cantidad");
                b.Property<float>("precio_unidad");

                b.HasKey("id");

                b.ToTable("medicamento");
            });

            modelBuilder.Entity("SistemaDeVentas.Models.Parto", b =>
            {
                b.Property<int>("id")
                    .ValueGeneratedOnAdd()
                    .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                b.Property<int>("id_cerdaMadre");
                b.Property<int>("Nacidos_vivos");
                b.Property<int>("nacidos_muertos");
                b.Property<int>("numero_adoptados");
                b.Property<int>("numero_transferidos");
                b.Property<string>("peso_promedio");
                b.Property<DateTime>("fecha_nacimiento");
                b.Property<DateTime>("fecha_Destete");
                b.Property<DateTime>("fecha_ultimoParto");
                b.Property<DateTime>("fecha_ultimoDestete");
                b.Property<int>("numero_hembra");
                b.Property<int>("numero_parto");
                b.Property<int>("numero_macho");
                b.Property<string>("observacion");

                b.HasKey("id");

                b.ToTable("Parto");
            });

            modelBuilder.Entity("SistemaDeVentas.Models.servicio", b =>
            {
                b.Property<int>("id")
                    .ValueGeneratedOnAdd()
                    .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                b.Property<int>("id_cerdoMacho");
                b.Property<int>("id_cerdoHembra");
                b.Property<DateTime>("fecha");
                b.Property<string>("servicio_exitoso");
                b.Property<string>("Observacion");
                b.Property<int>("intentos");
                b.Property<string>("estado");

                b.HasKey("id");

                b.ToTable("Servicio");
            });

            modelBuilder.Entity("SistemaDeVentas.Models.Alimento", b =>
            {
                b.Property<int>("id")
                    .ValueGeneratedOnAdd()
                    .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                b.Property<string>("nombre");
                b.Property<string>("descripcion");
                b.Property<string>("precio");
                b.Property<string>("categoria");
                b.Property<DateTime>("fecha_compra");
                b.Property<string>("cantidad");
                b.Property<float>("total");
                b.Property<DateTime>("fecha_registro");
                b.Property<string>("cantidad_existente");

                b.HasKey("id");

                b.ToTable("Alimento");
            });


            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
