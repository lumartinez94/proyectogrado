﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SistemaDeVentas.Areas.Users.Models;
using SistemaDeVentas.Areas.Ventas.Models;
using SistemaDeVentas.Models;
using SistemaDeVentas.Models.Dao.Clases;
using SistemaDeVentas.Areas.Cerdos.Models;
using SistemaDeVentas.Areas.Corrales.Models;
using SistemaDeVentas.Areas.Medicamento.Models;
using SistemaDeVentas.Areas.Alimentos.Models;
using SistemaDeVentas.Areas.Clientes.Models;
using SistemaDeVentas.Areas.ControlAlimentos.Models;
using SistemaDeVentas.Areas.ControlMedicamento.Models;

namespace SistemaDeVentas.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<TUsuarios> Tusuarios { get; set; }
        public DbSet<AppUsuarios> AppUsuarios { get; set; }
        public DbSet<Cerdo> Cerdos { get; set; }
        public DbSet<Raza> Razas { get; set; }

        public DbSet<linea_produccion> Linea_Produccion { get; set; }
        public DbSet<cerdo_alimentos> cerdo_Alimentos { get; set; }

        public DbSet<Cerdo_Medicamento> cerdo_Medicamento { get; set; }
        public DbSet<tipo_alimentacion> tipo_Alimentacion { get; set; }

        public DbSet<Historial_Acciones> Historial_Acciones { get; set; }
        public DbSet<Estado> Estados { get; set; }
        public DbSet<Corral> Corrals { get; set; }
        public DbSet<TipoCorral> tipoCorrals { get; set; }
        public DbSet<VentasU> Ventas { get; set; }
      

        public DbSet<Clientess> cliente { get; set; }
        public DbSet<Alimento> Alimento { get; set; }

        public DbSet<servicio> servicio { get; set; }
        public DbSet<Gestacion> gestacion { get; set; }

        public DbSet<Parto> parto { get; set; }
        public DbSet<Traslado> traslado { get; set; }


        public DbSet<medicamento> medicamento { get; set; }
    } 
}
