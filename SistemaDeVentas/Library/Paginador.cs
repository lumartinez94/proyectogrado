﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Library
{
    public class Paginador<T>
    {
        //cantidad de resultado por paginas
        private int pagi_cuantos = 10;
        //cantidad de enlaces que se mostraran como maximo en la barra de navegacion  
        private int pagi_nav_num_enlaces = 3;
        private int pagi_actual;
        //definimos que ira en el enlace a la pagina anterior
        private String pagi_nav_anterior = " &laquo; Anterior ";
        //definimos que ira en el enlace a la pagina siguiente
        private String pagi_nav_siguiente = " Siguiente";
        //definimos que ira en el enlace a la pagina siguiente
        private String pagi_nav_primera = " &laquo; Primero ";
        private String pagi_nav_ultima = " Último ";
        private String pagi_navegacion = null;

        public object[] paginador(List<T> table, int pagina, String area, String controller, String action, String host)
        {
            if (pagina==0)
            {
                //si no se ha hecho click a ninguna pagina especificada
                //o sea si la primera vez que se ejecuta el script
                // pagi_actual es la pagina actual-->sera por defecto la primera.
                pagi_actual = 1;

            }
            else
            {
                pagi_actual = pagina;

            }
            int pagi_totalReg = table.Count;
            double valor1 = pagi_totalReg / pagi_cuantos;
            int pagi_totalPags = Convert.ToInt16(Math.Round(valor1));
            if (pagi_actual != 1)
            {
                //si no estamos en la pagina 1. ponemos el enlace "primera"
                int pagi_url = 1;//sera el numero de pagina al que enlazamos
                pagi_navegacion += "<a id='paginas1' href='" + host + "/" + controller + "/" + action + "?id=" + pagi_url + "&area=" + area + "'>" + pagi_nav_primera + "</a>";

                //si no estamos en la pagina 1. ponemos el enlace "anterior"
                pagi_url = pagi_actual - 1;//sera el numero de pagina al que enlazamos
                pagi_navegacion += "<a id='paginas1' href='" + host + "/" + controller + "/" + action + "?id=" + pagi_url + "&area=" + area + "'>" + pagi_nav_anterior + " </a>";

            }
            //si se definio la variable pagi_nav_num_enlaces
            //calculamos el intervalo para restar y sumar a partir de la pagina actual
            double valor2 = (pagi_nav_num_enlaces / 2);
            int pagi_nav_intervalo = Convert.ToInt16(Math.Round(valor2));
            //calculamos desde que numero de pagina se mostrara
            int pagi_nav_desde = pagi_actual - pagi_nav_intervalo;
            //calculamos hasta que numero de pagina se mostrara
            int pagi_nav_hasta = pagi_actual + pagi_nav_intervalo;

            // si pagi_nav_desde es un numero negativo
            if (pagi_nav_desde < 1)
            {
                //le sumamos la cantidad sobrante al final para mantener el numero de enlaces que se quiere  mostrar.
                pagi_nav_hasta -= (pagi_nav_desde - 1);
                //establecemos pagi_nav_desde como 1.
                pagi_nav_desde = 1;

            }
            if (pagi_nav_hasta >= pagi_totalPags)
            {
                //le restamos la cantidad excedida al comienzo para mantener el numero de enlaces que se quiere mostrar.
                pagi_nav_desde -= (pagi_nav_hasta - pagi_totalPags);
            //Establecemos pagi_nav_hasta como el total de paginas
                pagi_nav_hasta = pagi_totalPags;
                //hacemos el ultimo ajuste verificando que al cambiar pagi_nav_desde no haya quedado con un valor no valido
                if (pagi_nav_desde < 1)
                {
                    pagi_nav_desde = 1;
                }
            }

            for (int pagi_i = pagi_nav_desde; pagi_i <= pagi_nav_hasta; pagi_i++)
            {
                //desde pagina 1 hasta ultima pagina (pagi_totalpags)
                if (pagi_i== pagi_actual)
                {
                    pagi_navegacion += "<span id='paginas2'>" + pagi_i + " </span>";
                }
                else
                {
                    pagi_navegacion += "<a id='paginas1' href='" + host + "/" + controller + "/" + action + "?id=" + pagi_i + "&area=" + area + "'>" + pagi_i + " </a>";    
                }
            }
            if (pagi_actual <= pagi_totalPags)
            {
                int pagi_url = pagi_actual + 1;
                pagi_navegacion += "<a id='paginas1' href='" + host + "/" + controller + "/" + action + "?id=" + pagi_url + "&area=" + area + "' class='page-link'>" + pagi_nav_siguiente + "</a>";

                pagi_url = pagi_totalPags;
                pagi_navegacion += "<a  href='" + host + "/" + controller + "/" + action + "?id=" + pagi_url + "&area=" + area + "' class='page-link'>" + pagi_nav_ultima + " </a>";
            }

            //Obtencion de los registros que se mostraran en la pagina actual

            int pagi_inicial = (pagi_actual - 1) * pagi_cuantos;

            var query = table.Skip(pagi_inicial).Take(pagi_cuantos).ToList();
            //Generacion de la informacion sobre los registros mostrados.

            //Numero del primer registro de la pagina actual
            int pagi_desde = pagi_inicial + 1;

            int pagi_hasta = pagi_inicial + pagi_cuantos;
            if (pagi_hasta > pagi_totalReg)
            {
                pagi_hasta = pagi_totalReg;

            }
            String pagi_info = " del <b>" + pagi_desde + " </b> al <b>" + pagi_hasta + "</b> de " + pagi_totalReg + " </b>";
            object[] data = { pagi_info, pagi_navegacion, query };
            return data ;
        }

    }
}
