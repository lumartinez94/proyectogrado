﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using SistemaDeVentas.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using SistemaDeVentas.Data;
using SistemaDeVentas.Areas.Users.Models;
using SistemaDeVentas.Areas.Ventas.Models;
using SistemaDeVentas.Areas.Corrales.Models;
using SistemaDeVentas.Areas.Medicamento.Models;
using SistemaDeVentas.Areas.Alimentos.Models;
using SistemaDeVentas.Areas.Cerdos.Models;
using SistemaDeVentas.Areas.Clientes.Models;
using SistemaDeVentas.Areas.ControlAlimentos.Models;
using SistemaDeVentas.Areas.ControlMedicamento.Models;

namespace SistemaDeVentas.Library
{
    public class Usuarios : ListObject
    {


        public Usuarios()
        {

        }
        public Usuarios(RoleManager<IdentityRole> roleManager)
        {
            _roleManager = roleManager;
            _usersRoles = new UserRoles();
        }
        public Usuarios(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context)
        {
            this._userManager = userManager;
            this._roleManager = roleManager;
            this._signInManager = signInManager;
            this._usersRoles = new UserRoles();
            _context = context;
        }

        internal async Task<List<Object[]>> UserLogin(string email, string password)
        {
            try
            {
                var result = await this._signInManager.PasswordSignInAsync(email, password, false, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    var appUser1 = _context.Users.Where(u => u.Email.Equals(email)).ToList();
                    var appUser2 = _context.Tusuarios.Where(u => u.IdUser.Equals(appUser1[0].Id)).ToList();
                    _userRoles = await _usersRoles.getRole(_userManager,
                                                     _roleManager,
                                                       appUser1[0].Id);
                    //_userData = new UserData
                    //{

                    //    UserName = appUser2[0].Nombre+" "+appUser2[0].Apellido,
                    //    Image= appUser2[0].Image+".png"
                    //};
                    code = "0";
                    description = result.Succeeded.ToString();
                }
                else
                {
                    code = "1";
                    description = "Correo o contraseña invalidos.";
                }

            }
            catch (Exception ex)
            {
                code = "2";
                description = ex.Message;

            }
            _identityError = new IdentityError
            {
                Code = code,
                Description = description
            };
            object[] data = { _identityError, _userData };
            dataList.Add(data);
            return dataList;
        }
        public async Task<List<InputModelRegistrarcs>> getTUsuariosAsync(String Search)
        {
            List<TUsuarios> listUser;
            if (Search == null)
            {
                listUser = _context.Tusuarios.ToList();
            }
            else
            {
                listUser = _context.Tusuarios.Where(u => u.Cedula.StartsWith(Search) || u.Nombre.StartsWith(Search) || u.Apellido.StartsWith(Search) || u.Image.StartsWith(Search)).ToList();
            }
            List<InputModelRegistrarcs> userList = new List<InputModelRegistrarcs>();


            foreach (var item in listUser)
            {
                _userRoles = await _usersRoles.getRole(_userManager, _roleManager, item.IdUser);
                userList.Add(new InputModelRegistrarcs
                {
                    ID = item.IdUser,
                    Cedula = item.Cedula,
                    Nombre = item.Nombre,
                    Apellido = item.Apellido,
                    Role = _userRoles[0].Text,
                    Email = item.Image,
                    id = item.ID

                });
                _userRoles.Clear();
            }

            return userList;
        }

        public async Task<List<InputModelCorral>> getCorralAsync(String Search)
        {

            List<Corral> listCorral = _context.Corrals.ToList();
            List<InputModelCorral> userList = new List<InputModelCorral>();



           

            if (Search == null)
            {
               

                foreach (var item in listCorral)
                {
                    userList.Add(new InputModelCorral
                    {
                        ID = item.ID,
                        numeroCorral = item.numeroCorral,
                        total=_context.Cerdos.Where(u=>u.id_corral==item.ID && (u.id_estado==1 || u.id_estado==5)).Count().ToString(),
                    });


                }
            }
            else
            {

                foreach (var item in listCorral)
                {
                    if (item.ID.Equals(Search) || item.numeroCorral.StartsWith(Search))
                    {

                        userList.Add(new InputModelCorral
                        {
                            ID = item.ID,
                            numeroCorral = item.numeroCorral,
                            total = _context.Cerdos.Where(u => u.id_corral == item.ID && u.id_estado == 1).Count().ToString(),
                        });
                    }
                }


            }



            return userList;
        }

        //obtencion lista de control medicamentos-cerdos
        public async Task<List<InputModelsCerdosMedicamentos>> getControlMedicamentosCerdosAsync(String Search)
        {

            List<Corral> listCorral = _context.Corrals.ToList();
            List<Cerdo_Medicamento> cerdo_Medicamentos = _context.cerdo_Medicamento.ToList();
            List<medicamento> medicamento = _context.medicamento.ToList();
            List<Cerdo> cerdos = _context.Cerdos.ToList();
            List<IdentityUser> usuario = _context.Users.ToList();
            List<InputModelsCerdosMedicamentos> userList = new List<InputModelsCerdosMedicamentos>();



            var lista = cerdo_Medicamentos.Join(
                cerdos,
                ca => ca.id_cerdo,
                cerd => cerd.ID,
                (ca, cerd) => new
                {

                    ca,
                    Caravana = cerd.codigo,
                    sexo = cerd.Sexo,
                    idCerdo = cerd.ID,
                    id_corral = cerd.id_corral

                }).Join(medicamento,
                caa => caa.ca.id_medicamento,
                alim => alim.ID,
                (caa, alim) => new

                {
                    caa,
                    NombreMedicamento = alim.Nombre
                }).Join(listCorral,
                cas => cas.caa.id_corral,
                corral => corral.ID,
                (cas, corral) => new
                {
                    cas,
                    ncorral = corral.numeroCorral

                }).Join(
                usuario,
                cerdo_alim => cerdo_alim.cas.caa.ca.id_usuario,
                user => user.Id,
                (cerdo_alim, user) =>
                new
                {
                    cerdo_alim,
                    user
                }
                ).OrderByDescending(c => c.cerdo_alim.cas.caa.ca.id).ToList();

            if (Search == null)
            {


                foreach (var item in lista)
                {
                    userList.Add(new InputModelsCerdosMedicamentos
                    {

                        id_cerdo_medicamento = item.cerdo_alim.cas.caa.ca.id.ToString(),
                        id_medicamento = item.cerdo_alim.cas.caa.ca.id_medicamento.ToString(),
                        id_cerdo = item.cerdo_alim.cas.caa.idCerdo.ToString(),
                        nombre_medicamento = item.cerdo_alim.cas.NombreMedicamento,
                        numero_corral = item.cerdo_alim.ncorral,
                        cantidad_suministrada = item.cerdo_alim.cas.caa.ca.cantidad_suministrada,
                        fecha = item.cerdo_alim.cas.caa.ca.fecha,
                        numero_caravana = item.cerdo_alim.cas.caa.Caravana,
                        correo_user = item.user.Email


                    });


                }
            }
            else
            {

                foreach (var item in lista)
                {
                    if (item.cerdo_alim.cas.caa.Caravana.StartsWith(Search) || item.cerdo_alim.cas.NombreMedicamento.StartsWith(Search) || item.user.Email.StartsWith(Search) || item.cerdo_alim.cas.caa.ca.cantidad_suministrada.StartsWith(Search)
                      || item.cerdo_alim.cas.caa.Caravana.StartsWith(Search) || item.cerdo_alim.ncorral.Equals(Search) || item.user.Email.StartsWith(Search))
                    {

                        userList.Add(new InputModelsCerdosMedicamentos
                        {
                            id_cerdo_medicamento = item.cerdo_alim.cas.caa.ca.id.ToString(),
                            id_medicamento = item.cerdo_alim.cas.caa.ca.id_medicamento.ToString(),
                            id_cerdo = item.cerdo_alim.cas.caa.idCerdo.ToString(),
                            nombre_medicamento = item.cerdo_alim.cas.NombreMedicamento,
                            numero_corral = item.cerdo_alim.ncorral,
                            cantidad_suministrada = item.cerdo_alim.cas.caa.ca.cantidad_suministrada,
                            fecha = item.cerdo_alim.cas.caa.ca.fecha,
                            numero_caravana = item.cerdo_alim.cas.caa.Caravana,
                            correo_user = item.user.Email
                        });
                    }
                }


            }



            return userList;
        }

        //obtencion lista de control alimentos-cerdos
        public async Task<List<InputModelsAlimentos_cerdos>> getControlAlimentosCerdosAsync(String Search)
        {

            List<Corral> listCorral = _context.Corrals.ToList();
            List<cerdo_alimentos> cerdo_Alimentos = _context.cerdo_Alimentos.ToList();
            List<Alimento> alimentos = _context.Alimento.ToList();
            List<Cerdo> cerdos = _context.Cerdos.ToList();
            List<IdentityUser> usuario = _context.Users.ToList();
            List<InputModelsAlimentos_cerdos> userList = new List<InputModelsAlimentos_cerdos>();



            var lista = cerdo_Alimentos.Join(
                cerdos,
                ca => ca.id_cerdo,
                cerd => cerd.ID,
                (ca, cerd) => new
                {

                    ca,
                    Caravana = cerd.codigo,
                    sexo = cerd.Sexo,
                    idCerdo = cerd.ID,
                    id_corral = cerd.id_corral

                }).Join(alimentos,
                caa => caa.ca.id_alimento,
                alim => alim.Id,
                (caa, alim) => new

                {
                    caa,
                    categoria = alim.categoria
                }).Join(listCorral,
                cas => cas.caa.id_corral,
                corral => corral.ID,
                (cas, corral) => new
                {
                    cas,
                    ncorral = corral.numeroCorral

                }).Join(
                usuario,
                cerdo_alim=>cerdo_alim.cas.caa.ca.id_user,
                user=>user.Id,
                (cerdo_alim,user)=>
                new
                {
                    cerdo_alim,
                    user
                }
                ).OrderByDescending(c=>c.cerdo_alim.cas.caa.ca.id).ToList();

            if (Search == null)
            {


                foreach (var item in lista)
                {
                    userList.Add(new InputModelsAlimentos_cerdos
                    {
                        
                        id_alimentos_cerdo= item.cerdo_alim.cas.caa.ca.id.ToString(),
                        id_alimento= item.cerdo_alim.cas.caa.ca.id_alimento.ToString(),
                        id_cerdo= item.cerdo_alim.cas.caa.idCerdo.ToString(),
                        categoria_alimento= item.cerdo_alim.cas.categoria,
                        numero_corral= item.cerdo_alim.ncorral,
                        cantidad_kg= item.cerdo_alim.cas.caa.ca.cantidad_suministrada,
                        fecha= item.cerdo_alim.cas.caa.ca.fecha,
                        caravana_cerdo= item.cerdo_alim.cas.caa.Caravana,
                        correoUser= item.user.Email

                       
                    });


                }
            }
            else
            {

                foreach (var item in lista)
                {
                    if (item.cerdo_alim.cas.caa.Caravana.StartsWith(Search) || item.cerdo_alim.cas.categoria.StartsWith(Search) || item.user.Email.StartsWith(Search) || item.cerdo_alim.cas.caa.ca.cantidad_suministrada.StartsWith(Search)
                      ||  item.cerdo_alim.cas.caa.Caravana.StartsWith(Search))
                    {

                        userList.Add(new InputModelsAlimentos_cerdos
                        {
                            id_alimentos_cerdo = item.cerdo_alim.cas.caa.ca.id.ToString(),
                            id_alimento = item.cerdo_alim.cas.caa.ca.id_alimento.ToString(),
                            id_cerdo = item.cerdo_alim.cas.caa.idCerdo.ToString(),
                            numero_corral = item.cerdo_alim.ncorral,
                            cantidad_kg = item.cerdo_alim.cas.caa.ca.cantidad_suministrada,
                            fecha = item.cerdo_alim.cas.caa.ca.fecha,
                            categoria_alimento = item.cerdo_alim.cas.categoria,
                            caravana_cerdo = item.cerdo_alim.cas.caa.Caravana,
                            correoUser = item.user.Email
                        });
                    }
                }


            }



            return userList;
        }


        //obtencion listas de ventas
        public async Task<List<InputModelVentas>> getVentassync(String Search)
        {
            List<InputModelVentas> List = new List<InputModelVentas>();

            List<Cerdo> cerdos = _context.Cerdos.ToList();
            List<Clientess> clientes = _context.cliente.ToList();
            List<VentasU> ventas = _context.Ventas.ToList();

            var list = ventas.Join(
                cerdos,
                vent => vent.id_cerdo,
                cerd => cerd.ID,
                (vent, cerd) => new
                {
                    vent,
                    Caravana = cerd.codigo,
                    IdCerdo = cerd.ID

                }
                ).Join(
                clientes,
                v => v.vent.id_cliente,
                cli => cli.id,
                (v, cli) => new
                {
                    v,
                    IdCliente = cli.id,
                    NombreCli = cli.nombre
                }

                ).Join(
                _context.Users,
                vn => vn.v.vent.id_user,
                user => user.Id,
                (vn, user) => new
                {
                    vn,
                    email = user.Email
                }

                ).ToList();



            if (Search == null)
            {

                foreach (var item in list)
                {

                    List.Add(new InputModelVentas
                    {
                        id_cerdo = item.vn.v.IdCerdo.ToString(),
                        caravana_cerdo = item.vn.v.Caravana,
                        id_venta = item.vn.v.vent.ID.ToString(),
                        Nombre_Cliente = item.vn.NombreCli,
                        correo_empleado = item.email,
                        Precio = item.vn.v.vent.precio,
                        precio_total = item.vn.v.vent.precio_total,
                        Peso = item.vn.v.vent.peso,
                        TipoVenta = item.vn.v.vent.tipo_venta,
                        PrecioKg = item.vn.v.vent.precioKg,
                        Fecha = item.vn.v.vent.fecha




                    });

                }

            }
            else
            {


                foreach (var item in list)
                {

                    if (item.vn.NombreCli.StartsWith(Search) ||
                        item.email.StartsWith(Search) || item.vn.v.Caravana.StartsWith(Search) || item.vn.v.vent.tipo_venta.StartsWith(Search))
                    {

                        List.Add(new InputModelVentas
                        {
                            id_cerdo = item.vn.v.IdCerdo.ToString(),
                            caravana_cerdo = item.vn.v.Caravana,
                            id_venta = item.vn.v.vent.ID.ToString(),
                            Nombre_Cliente = item.vn.NombreCli,
                            correo_empleado = item.email,
                            Precio = item.vn.v.vent.precio,
                            precio_total = item.vn.v.vent.precio_total,
                            Peso = item.vn.v.vent.peso,
                            TipoVenta = item.vn.v.vent.tipo_venta,
                            PrecioKg = item.vn.v.vent.precioKg,
                            Fecha = item.vn.v.vent.fecha

                        });

                    }
                }

            }



            return List;
        }

        public async Task<List<InputModelMedicamentos>> getMedicamentoAsync(String Search)
        {

            List<medicamento> listMedicamento = _context.medicamento.ToList();
            List<InputModelMedicamentos> Medicamentos = new List<InputModelMedicamentos>();

            if (Search == null)
            {


                foreach (var item in listMedicamento)
                {
                    Medicamentos.Add(new InputModelMedicamentos
                    {
                        ID = item.ID.ToString(),
                        Nombre = item.Nombre,
                        Marca = item.Marca,
                        Precio_unidad = item.Precio_unidad,
                        cantidad = item.cantidad,
                        Fecha = item.fecha_creacion

                    });


                }
            }
            else
            {

                foreach (var item in listMedicamento)
                {
                    if (item.Nombre.ToLower().StartsWith(Search.ToLower()) || item.Marca.ToLower().StartsWith(Search.ToLower()) || item.cantidad.Equals(int.Parse(Search)))
                    {

                        Medicamentos.Add(new InputModelMedicamentos
                        {
                            ID = item.ID.ToString(),
                            Nombre = item.Nombre,
                            Marca = item.Marca,
                            Fecha = item.fecha_creacion,
                            Precio_unidad = item.Precio_unidad,
                            cantidad = item.cantidad,

                        });
                    }
                }


            }



            return Medicamentos;
        }


        public async Task<List<InputModelAlimentos>> getAlimentosAsync(String Search)
        {

            List<Alimento> listAlimento = _context.Alimento.ToList();
            List<InputModelAlimentos> Alimentos = new List<InputModelAlimentos>();

            if (Search == null)
            {


                foreach (var item in listAlimento)
                {
                    Alimentos.Add(new InputModelAlimentos
                    {
                        Id = item.Id.ToString(),
                        Nombre = item.Nombre,
                        Descripcion = item.Descripcion,
                        valor_bulto = item.valor_bulto,
                        valor_kg = item.valor_kg,
                        cantidad = item.cantidad,
                        Fecha_compra = item.Fecha_compra

                    });


                }
            }
            else
            {

                foreach (var item in listAlimento)
                {
                    if (item.Nombre.ToLower().StartsWith(Search.ToLower()) || item.valor_bulto.ToLower().StartsWith(Search.ToLower()) || item.cantidad.Equals(int.Parse(Search)) || item.valor_bulto.StartsWith(Search))
                    {

                        Alimentos.Add(new InputModelAlimentos
                        {
                            Id = item.Id.ToString(),
                            Nombre = item.Nombre,
                            Descripcion = item.Descripcion,
                            valor_bulto = item.valor_bulto,
                            valor_kg = item.valor_kg,
                            cantidad = item.cantidad,
                            Fecha_compra = item.Fecha_compra

                        });
                    }
                }


            }



            return Alimentos;
        }

        //obtencion de listas de clientes
        public async Task<List<InputModelsClientes>> getClientesAsync(String Search)
        {
            List<Clientess> list;
            if (Search == null)
            {
                list = _context.cliente.ToList();
            }
            else
            {
                list = _context.cliente.Where(u => u.nombre.StartsWith(Search) || u.fecha.Equals(Search) || u.direccion.StartsWith(Search) || u.cedula.Equals(int.Parse(Search)) || u.telefono.Equals(int.Parse(Search))).ToList();
            }
            List<InputModelsClientes> userList = new List<InputModelsClientes>();


            foreach (var item in list)
            {

                userList.Add(new InputModelsClientes
                {
                    id_clientes = item.id.ToString(),
                    nombre = item.nombre,
                    telefono = item.telefono.ToString(),
                    direccion = item.direccion,
                    fecha = item.fecha,
                    cedula = item.cedula

                });


            }

            return userList;
        }

        public String userData(HttpContext http)
        {
            String role = null;
            var user = http.Session.GetString("User");
            if (user != null)
            {
                UserData dataItem = JsonConvert.DeserializeObject<UserData>(user.ToString());
                role = dataItem.Role;

            }
            else
            {
                role = "No data";
            }
            return role;
        }
    }
}
