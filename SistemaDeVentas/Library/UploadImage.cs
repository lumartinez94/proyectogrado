﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Library
{
    public class UploadImage
    {
        public async Task CopyImageAsync (IFormFile AvatarImage, String fileName, IHostingEnvironment environment,String carpeta,String image)
        {
            if(null == AvatarImage)
            {
                String fileOrigen;
               
                
                var destFileName = environment.ContentRootPath + "/wwwroot/images/photos/"+carpeta+"/" + fileName;
                if (image == null)
                {
                     fileOrigen = environment.ContentRootPath + "/wwwroot/images/photos/" + carpeta + "/default.jpg";
                    File.Copy(fileOrigen, destFileName, true);
                }
                else
                {
                    fileOrigen = environment.ContentRootPath + "/wwwroot/images/photos/" + carpeta + "/"+image+".png";
                    if (fileName != image + ".png")
                    {
                        File.Copy(fileOrigen, destFileName, true);
                        File.Delete(fileOrigen);
                    }

                }
              

            }
            else
            {
                var filePath = Path.Combine(environment.ContentRootPath, "wwwroot/images/photos/"+carpeta, fileName );
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await AvatarImage.CopyToAsync(stream);
                }

            }
        }

        public void deleteImage(IHostingEnvironment environment,string carpeta, String image)
        {
            var fileOrigen = environment.ContentRootPath + "/wwwroot/images/photos/" + carpeta + "/" + image + ".png";
            File.Delete(fileOrigen);
        }
    }
}
