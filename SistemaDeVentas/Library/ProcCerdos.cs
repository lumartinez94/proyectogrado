﻿using Microsoft.AspNetCore.Identity;
using SistemaDeVentas.Areas.Cerdos.Models;
using SistemaDeVentas.Areas.Corrales.Models;
using SistemaDeVentas.Data;
using SistemaDeVentas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Library
{
    public class ProcCerdos:ListObject
    {
        public ProcCerdos()
        {

        }

        public ProcCerdos(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context)
        {
            this._userManager = userManager;
            this._roleManager = roleManager;
            this._signInManager = signInManager;
            this._usersRoles = new UserRoles();
            _context = context;
        }

        public async Task<List<InputModelsCerdos>> getRazasAsync(String Search)
        {
            List<Raza> listUser;
            if (Search == null)
            {
                listUser = _context.Razas.ToList();
            }
            else
            {
                listUser = _context.Razas.Where(u => u.Nombre.StartsWith(Search) || u.Descripcion.StartsWith(Search)).ToList();
            }
            List<InputModelsCerdos> userList = new List<InputModelsCerdos>();


            foreach (var item in listUser)
            {
                
                userList.Add(new InputModelsCerdos
                {
                    ID = item.Id,
                    Nombre = item.Nombre,
                    Descripcion = item.Descripcion
                   

                });
              
            }

            return userList;
        }

        //obtencion de listas de partos de cerdas

        public async Task<List<InputModelsPartos>> getPartosAsync(String Search)
        {
            List<InputModelsPartos> List = new List<InputModelsPartos>();

            var list = _context.parto.Join(


                _context.Cerdos,
                service => service.id_cerdaMadre,
                cerdos => cerdos.ID,
                (service, cerdos) => new
                {
                    service,
                    CaravanaHembra = cerdos.codigo
                }



                ).OrderByDescending(x=>x.service.id).ToList();


            if (Search == null)
            {

                foreach (var item in list)
                {

                    List.Add(new InputModelsPartos
                    {
                        id_cerdaMadre = item.service.id_cerdaMadre.ToString(),
                        id_parto = item.service.id.ToString(),
                        fecha_nacimiento = item.service.fecha_nacimiento,
                        fecha_ultimoParto = item.service.fecha_nacimiento,
                        fecha_Destete = item.service.fecha_Destete,
                        fecha_ultimoDestete = item.service.fecha_ultimoDestete,
                        nacidos_muertos = item.service.nacidos_muertos.ToString()!=null ? item.service.nacidos_muertos.ToString():"0",
                        nacidos_vivos= item.service.nacidos_vivos.ToString()!=null ? item.service.nacidos_vivos.ToString():"0",
                        numero_adoptados= item.service.numero_adoptados.ToString(),
                        numero_hembra= item.service.numero_hembra.ToString(),
                        numero_macho= item.service.numero_macho.ToString(),
                        numero_parto= item.service.numero_parto.ToString()!=null ? item.service.numero_parto.ToString():"0",
                        numero_transferidos= item.service.numero_transferidos.ToString(),
                        caravana_cerda= item.CaravanaHembra,
                        peso_promedio= item.service.peso_promedio
                        



                    });

                }

            }
            else
            {


                foreach (var item in list)
                {

                    if (item.service.id_cerdaMadre.Equals(Search) ||
                        item.service.id.Equals(Search) || item.CaravanaHembra.StartsWith(Search) || item.service.nacidos_vivos.Equals(Search) || item.service.nacidos_muertos.Equals(Search))
                    {

                        List.Add(new InputModelsPartos
                        {
                            id_cerdaMadre = item.service.id_cerdaMadre.ToString(),
                            id_parto = item.service.id.ToString(),
                            fecha_nacimiento = item.service.fecha_nacimiento,
                            fecha_ultimoParto = item.service.fecha_nacimiento,
                            fecha_Destete = item.service.fecha_Destete,
                            fecha_ultimoDestete = item.service.fecha_ultimoDestete,
                            nacidos_muertos = item.service.nacidos_muertos.ToString(),
                            nacidos_vivos = item.service.nacidos_vivos.ToString(),
                            numero_adoptados = item.service.numero_adoptados.ToString(),
                            numero_hembra = item.service.numero_hembra.ToString(),
                            numero_macho = item.service.numero_macho.ToString(),
                            numero_parto = item.service.numero_parto.ToString(),
                            numero_transferidos = item.service.numero_transferidos.ToString(),
                            caravana_cerda = item.CaravanaHembra,
                            peso_promedio = item.service.peso_promedio

                        });

                    }
                }

            }



            return List;
        }

        //obtencion lista de cerdas en gestacion
        public async Task<List<InputModelsGestacion>> getGestacionAsync(String Search)
        {
            List<InputModelsGestacion> List = new List<InputModelsGestacion>();

            var list = _context.gestacion.Join(


                _context.Cerdos,
                service => service.id_cerdo,
                cerdos => cerdos.ID,
                (service, cerdos) => new
                {
                    service,
                    CaravanaHembra = cerdos.codigo
                }



                ).ToList();


            if (Search == null)
            {

                foreach (var item in list)
                {

                    List.Add(new InputModelsGestacion
                    {
                        id_cerdo= item.service.id_cerdo.ToString(),
                        id_gestacion= item.service.id.ToString(),
                        fecha_inicio= item.service.fecha_inicio,
                        fecha_aproximada= item.service.fecha_aproximada,
                        estado= item.service.estado,
                        caravana_hembra= item.CaravanaHembra,
                        fecha_real= item.service.fecha_real !=null ? item.service.fecha_real : null,

                        

                    });

                }

            }
            else
            {


                foreach (var item in list)
                {

                    if (item.service.id_cerdo.Equals(Search) ||
                        item.service.id.Equals(Search) || item.CaravanaHembra.StartsWith(Search) || item.service.estado.StartsWith(Search))
                    {

                        List.Add(new InputModelsGestacion
                        {
                            id_cerdo = item.service.id_cerdo.ToString(),
                            id_gestacion = item.service.id.ToString(),
                            fecha_inicio = item.service.fecha_inicio,
                            fecha_aproximada = item.service.fecha_aproximada,
                            estado = item.service.estado,
                            caravana_hembra = item.CaravanaHembra,
                            fecha_real = item.service.fecha_real != null ? item.service.fecha_real : null,

                        });

                    }
                }

            }



            return List;
        }

        //obtencion de servicio
        public async Task<List<InputModelsServicio>> getServicioAsync(String Search)
        {
            List<InputModelsServicio> List = new List<InputModelsServicio>();

            var list = _context.servicio.Join(


                _context.Cerdos,
                service=>service.id_cerdoMacho,
                cerdos=>cerdos.ID,
                (service,cerdos)=> new
                {
                    service,
                    CaravanaMacho= cerdos.codigo
                }

                
                
                ).Join(
                _context.Cerdos,
                service=>service.service.id_cerdoHembra,
                cerdos=>cerdos.ID,
                (service,cerdos)=> new
                {
                    service,
                    CaravanaHembra= cerdos.codigo,
                    IdHembra= cerdos.ID
                    
                }
                ).ToList();


            if (Search == null)
            {
         
                foreach (var item in list)
                {

                    List.Add(new InputModelsServicio
                    {
                        id_Servicio = item.service.service.id,
                        id_macho = item.service.service.id_cerdoMacho.ToString(),
                        caravana_macho = item.service.CaravanaMacho,
                        id_hembra = item.IdHembra.ToString(),
                        caravana_hembra = item.CaravanaHembra,
                        fecha_servicio = item.service.service.fecha,
                        servicio_exitoso = item.service.service.servicio_exitoso

                    });

                }

            }
            else
            {


                foreach (var item in list)
                {

                    if (item.IdHembra.Equals(Search) || 
                        item.service.service.id_cerdoMacho.Equals(Search) ||item.CaravanaHembra.Equals(Search) || item.service.CaravanaMacho.Equals(Search) || item.service.service.servicio_exitoso.Equals(Search))
                    {

                        List.Add(new InputModelsServicio
                        {
                            id_Servicio= item.service.service.id,
                            id_macho = item.service.service.id_cerdoMacho.ToString(),
                            caravana_macho = item.service.CaravanaMacho,
                            id_hembra = item.IdHembra.ToString(),
                            caravana_hembra = item.CaravanaHembra,
                            fecha_servicio = item.service.service.fecha,
                            servicio_exitoso = item.service.service.servicio_exitoso

                        });

                    }
                }

            }
        


            return List;
        }

        public async Task<List<InputModelsCerdos>> getCerdosAsync(String Search)
        {

            var listCorral = _context.Corrals.ToList();
            var tipoCorrals = _context.tipoCorrals.ToList();
            var listProduccion = _context.Linea_Produccion.ToList();
            var listRaza = _context.Razas.ToList();
            var alimentacionList = _context.tipo_Alimentacion.ToList();
            var CerdoList = _context.Cerdos.ToList();
            var estadoList = _context.Estados.ToList();
            List<InputModelsCerdos> userList = new List<InputModelsCerdos>();



            var lista = CerdoList.Join(
                listCorral,
                cerdos => cerdos.id_corral,
                corrales => corrales.ID,
                (cerdos, corrales) =>
                new
                {
                    cerdos,
                    corrales

                }).Join(
                listProduccion,
                combCorrales => combCorrales.cerdos.id_lineaProduccion,
                produccion => produccion.id,
                (combCorrales, produccion) =>
                new
                {
                    combCorrales,
                    produccion
                }).Join(
                listRaza,
                cmb => cmb.combCorrales.cerdos.id_raza,
                razas => razas.Id,
                (cmb, razas) =>
                new
                {
                    cmb,
                    razas
                }
                ).Join(alimentacionList,
                cmmb => cmmb.cmb.combCorrales.cerdos.id_alimentacion,
                alimentacion => alimentacion.id,
                (cmmb, alimentacion) => new
                {
                    cmmb,
                    alimentacion
                }
                ).Join(estadoList,
                cb=>cb.cmmb.cmb.combCorrales.cerdos.id_estado,
                estados=>estados.ID,
                (cb, estados) => new
                {
                    cb,
                    estados
                }
                ).ToList();
                
                

            if (Search == null)
            {
                

                foreach (var item in lista)
                {


                    userList.Add(new InputModelsCerdos
                    {
                        ID = item.cb.cmmb.cmb.combCorrales.cerdos.ID,

                        Sexo = item.cb.cmmb.cmb.combCorrales.cerdos.Sexo,
                        numeroCorral = item.cb.cmmb.cmb.combCorrales.corrales.numeroCorral,
                        NombreRaza= item.cb.cmmb.razas.Nombre,
                        Nombre_alimentacion=item.cb.alimentacion.nombre,
                        codigo=item.cb.cmmb.cmb.combCorrales.cerdos.codigo,
                        nombre_produccion=item.cb.cmmb.cmb.produccion.nombre,
                        fecha_destete = item.cb.cmmb.cmb.combCorrales.cerdos.fecha_destete,
                        Peso = item.cb.cmmb.cmb.combCorrales.cerdos.Peso,
                        status= item.estados.Nombre,
                    


                    });


                }
            }
            else
            {

                foreach (var item in lista)
                {
                    if (item.cb.cmmb.cmb.combCorrales.cerdos.Sexo.ToLower().StartsWith(Search.ToLower()) 

                        || item.estados.Nombre.Equals(Search) ||

                         item.cb.cmmb.razas.Nombre.ToLower().Equals(Search) || item.cb.alimentacion.nombre.ToLower().StartsWith(Search.ToLower()) || item.cb.cmmb.cmb.produccion.nombre.ToLower().StartsWith(Search.ToLower()))
                    {

                        userList.Add(new InputModelsCerdos
                        {
                            ID = item.cb.cmmb.cmb.combCorrales.cerdos.ID,

                            Sexo = item.cb.cmmb.cmb.combCorrales.cerdos.Sexo,
                            numeroCorral = item.cb.cmmb.cmb.combCorrales.corrales.numeroCorral,
                            NombreRaza = item.cb.cmmb.razas.Nombre,
                            Nombre_alimentacion = item.cb.alimentacion.nombre,
                            nombre_produccion = item.cb.cmmb.cmb.produccion.nombre,
                            codigo=item.cb.cmmb.cmb.combCorrales.cerdos.codigo,
                            fecha_destete = item.cb.cmmb.cmb.combCorrales.cerdos.fecha_destete,
                            Peso = item.cb.cmmb.cmb.combCorrales.cerdos.Peso,
                            status = item.estados.Nombre,
                         
                        });
                    }
                }


            }

            return userList;
        }
    }
}
