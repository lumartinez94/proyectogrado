﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaDeVentas.Data;
using SistemaDeVentas.Models;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;


namespace SistemaDeVentas.Library
{
    public class ListObject
    {
        public String description, code;
        public UserRoles _usersRoles;
        public UserData _userData;
        public Usuarios _usuarios;
        public IdentityError _identityError;
        public ProcCerdos _procCerdos;
        public List<SelectListItem> _userRoles;

        public ApplicationDbContext _context;
        public IHostingEnvironment _environment;
        public UploadImage _image;

        public RoleManager<IdentityRole> _roleManager;
        public UserManager<IdentityUser> _userManager;
        public SignInManager<IdentityUser> _signInManager;

        public List<Object[]> dataList = new List<object[] >();
        
     
    }
}
