﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace SistemaDeVentas.Models
{
    public class LoginViewModels
    {
        [BindProperty]
        public InputModels input{get;set;}
        [TempData]
        public string ErrorMessage { get; set; }
        public class InputModels
        {
            [Required(ErrorMessage ="<font color='red'>El campo correo electronico es obligatorio.</font>")]
            [EmailAddress(ErrorMessage = "<font color='red'>El correo electronico no es una dirección de correo electronico valida.</font>")]

            public string Email { get; set; }

            [Required(ErrorMessage = "<font color='red'>El campo contraseña es obligatorio.</font>")]
            [DataType(DataType.Password)]
            [StringLength(100,ErrorMessage ="<font color='red'>El numero de caracteres de {0} debe ser al menos {2}.</font>",MinimumLength =6)] 
            public string Password { get; set; }

        }
    }
}
