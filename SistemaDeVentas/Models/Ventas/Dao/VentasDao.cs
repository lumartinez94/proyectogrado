﻿using SistemaDeVentas.Areas.Cerdos.Models;
using SistemaDeVentas.Areas.Clientes.Models;
using SistemaDeVentas.Areas.Corrales.Models;
using SistemaDeVentas.Models.Ventas.Clase;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Corral = SistemaDeVentas.Models.Ventas.Clase.Corral;

namespace SistemaDeVentas.Models.Ventas.Dao
{
    public class VentasDao
    {
        private SqlConnection con;
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["GanadoPorcino"].ToString();
            con = new SqlConnection(constring);
        }

        public List<Venta> readAllCerdosVendidos(string f1, string f2)
        {
            connection();
            List<Venta> lista = new List<Venta>();
            SqlCommand cmd = new SqlCommand("select vc.ValorTotal,vc.TipoVenta from venta_cerdo vc inner join ventas v on v.ID = vc.idventa where Estado='EXITOSO' and (cast (v.Fecha as date) >=  '" + f1 + "'  and cast (v.Fecha as date) <=  '" + f2 + "')", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Venta clase = new Venta();


                clase.TipoVenta = (String.IsNullOrEmpty(dr["TipoVenta"].ToString())) ? "" : Convert.ToString(dr["TipoVenta"]);
                clase.Valor = (String.IsNullOrEmpty(dr["ValorTotal"].ToString())) ? "" : Convert.ToString(dr["ValorTotal"]);






                lista.Add(clase);
            }

            return lista;
        }
        public List<Venta> readAllCerdosVendidos(int id)
        {
            connection();
            List<Venta> lista = new List<Venta>();
            SqlCommand cmd = new SqlCommand("select c.codigo, cv.TipoVenta,cv.ValorTotal,cv.ValorKg,c.peso,c.sexo,r.nombre as raza from cerdos c inner join razas r on r.id = c.id_raza inner join Venta_Cerdo cv on cv.IdCerdo = c.ID where cv.IdVenta ='" + id + "'", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Venta clase = new Venta();

                clase.IdCerdo = (String.IsNullOrEmpty(dr["codigo"].ToString())) ? "" : Convert.ToString(dr["codigo"]);
                clase.TipoVenta = (String.IsNullOrEmpty(dr["TipoVenta"].ToString())) ? "" : Convert.ToString(dr["TipoVenta"]);
                clase.sexo = (String.IsNullOrEmpty(dr["sexo"].ToString())) ? "" : Convert.ToString(dr["sexo"]);
                clase.Valor = (String.IsNullOrEmpty(dr["ValorTotal"].ToString())) ? "" : Convert.ToString(dr["ValorTotal"]);
                clase.ValorKG = (String.IsNullOrEmpty(dr["ValorKg"].ToString())) ? "" : Convert.ToString(dr["ValorKg"]);
                clase.Peso = (String.IsNullOrEmpty(dr["peso"].ToString())) ? "" : Convert.ToString(dr["peso"]);
                clase.raza = (String.IsNullOrEmpty(dr["raza"].ToString())) ? "" : Convert.ToString(dr["raza"]);





                lista.Add(clase);
            }

            return lista;
        }
        public Venta Read(Venta clase)
        {
            connection();

            SqlCommand cmd = new SqlCommand("select v.Observacion,v.ID,upper(c.nombre) as cliente," +
                "c.cedula,v.estado,v.[Fecha],(select count(*) from Venta_Cerdo vc where vc.IdVenta=v.ID) as Vendidos," +
                "v.[precio_total] from Ventas v inner join Cliente c on c.id = v.id_cliente where v.ID='" + clase.Id + "' ", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {

                clase.Id = (String.IsNullOrEmpty(dr["ID"].ToString())) ? 0 : int.Parse(dr["ID"].ToString());
                clase.cliente.nombre = (String.IsNullOrEmpty(dr["cliente"].ToString())) ? "" : Convert.ToString(dr["cliente"]);
                clase.cliente.cedula = (String.IsNullOrEmpty(dr["cedula"].ToString())) ? "" : Convert.ToString(dr["cedula"]);
                clase.unidadesvendidas = (String.IsNullOrEmpty(dr["Vendidos"].ToString())) ? "" : Convert.ToString(dr["Vendidos"]);
                clase.Valor = (String.IsNullOrEmpty(dr["precio_total"].ToString())) ? "" : Convert.ToString(dr["precio_total"]);
                clase.fechaventa = (String.IsNullOrEmpty(dr["Fecha"].ToString())) ? "" : Convert.ToString(dr["Fecha"]);
                clase.estado = (String.IsNullOrEmpty(dr["estado"].ToString())) ? "" : Convert.ToString(dr["estado"]);
                clase.observacion = (String.IsNullOrEmpty(dr["Observacion"].ToString())) ? "" : Convert.ToString(dr["Observacion"]);


            }

            return clase;
        }
        public List<Venta> readAll()
        {
            connection();
            List<Venta> lista = new List<Venta>();
            SqlCommand cmd = new SqlCommand("select v.ID,upper(c.nombre) as cliente," +
                "c.cedula,v.[Fecha],(select count(*) from Venta_Cerdo vc where vc.IdVenta=v.ID) " +
                "as Vendidos,v.estado,v.[precio_total] from Ventas v inner join Cliente c on c.id = v.id_cliente order by v.ID desc", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Venta clase = new Venta();
                clase.Id = (String.IsNullOrEmpty(dr["ID"].ToString())) ? 0 : int.Parse(dr["ID"].ToString());
                clase.cliente.nombre = (String.IsNullOrEmpty(dr["cliente"].ToString())) ? "" : Convert.ToString(dr["cliente"]);
                clase.cliente.cedula = (String.IsNullOrEmpty(dr["cedula"].ToString())) ? "" : Convert.ToString(dr["cedula"]);
                clase.unidadesvendidas = (String.IsNullOrEmpty(dr["Vendidos"].ToString())) ? "" : Convert.ToString(dr["Vendidos"]);
                clase.Valor = (String.IsNullOrEmpty(dr["precio_total"].ToString())) ? "" : Convert.ToString(dr["precio_total"]);
                clase.fechaventa = (String.IsNullOrEmpty(dr["Fecha"].ToString())) ? "" : Convert.ToString(dr["Fecha"]);
                clase.estado = (String.IsNullOrEmpty(dr["estado"].ToString())) ? "" : Convert.ToString(dr["estado"]);


                lista.Add(clase);
            }

            return lista;
        }
        public List<Venta> readAllReporte(string f1, string f2)
        {
            connection();
            List<Venta> lista = new List<Venta>();
            SqlCommand cmd = new SqlCommand("select v.ID,upper(c.nombre) as cliente," +
                "c.cedula,v.[Fecha],(select count(*) from Venta_Cerdo vc where vc.IdVenta=v.ID) " +
                "as Vendidos,v.estado,v.[precio_total] from Ventas v inner join Cliente c on c.id = v.id_cliente " +
                "where Estado ='EXITOSO' and (cast (v.Fecha as date) >=  '" + f1 + "'  and cast(v.Fecha as date) <= '" + f2 + "' ) order by v.ID desc", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Venta clase = new Venta();
                clase.Id = (String.IsNullOrEmpty(dr["ID"].ToString())) ? 0 : int.Parse(dr["ID"].ToString());
                clase.cliente.nombre = (String.IsNullOrEmpty(dr["cliente"].ToString())) ? "" : Convert.ToString(dr["cliente"]);
                clase.cliente.cedula = (String.IsNullOrEmpty(dr["cedula"].ToString())) ? "" : Convert.ToString(dr["cedula"]);
                clase.unidadesvendidas = (String.IsNullOrEmpty(dr["Vendidos"].ToString())) ? "" : Convert.ToString(dr["Vendidos"]);
                clase.Valor = (String.IsNullOrEmpty(dr["precio_total"].ToString())) ? "" : Convert.ToString(dr["precio_total"]);
                clase.fechaventa = (String.IsNullOrEmpty(dr["Fecha"].ToString())) ? "" : Convert.ToString(dr["Fecha"]);
                clase.estado = (String.IsNullOrEmpty(dr["estado"].ToString())) ? "" : Convert.ToString(dr["estado"]);


                lista.Add(clase);
            }

            return lista;
        }

        public List<Venta> readAllReporteCliente (string f1, string f2,string id)
        {
            connection();
            string sql = "";
            List<Venta> lista = new List<Venta>();
            if (id ==null)
            {
                sql = "select v.ID,upper(c.nombre) as cliente," +
                "c.cedula,v.[Fecha],(select count(*) from Venta_Cerdo vc where vc.IdVenta=v.ID) " +
                "as Vendidos,v.estado,v.[precio_total] from Ventas v inner join Cliente c on c.id = v.id_cliente " +
                "where Estado ='EXITOSO' and (cast (v.Fecha as date) >=  '" + f1 + "'  and cast(v.Fecha as date) <= '" + f2 + "' )  order by v.ID desc";
            }
            else
            {
                sql = "select v.ID,upper(c.nombre) as cliente," +
                "c.cedula,v.[Fecha],(select count(*) from Venta_Cerdo vc where vc.IdVenta=v.ID) " +
                "as Vendidos,v.estado,v.[precio_total] from Ventas v inner join Cliente c on c.id = v.id_cliente " +
                "where Estado ='EXITOSO' and (cast (v.Fecha as date) >=  '" + f1 + "'  and cast(v.Fecha as date) <= '" + f2 + "' ) and c.id='" + id + "'  order by v.ID desc";

            }
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Venta clase = new Venta();
                clase.Id = (String.IsNullOrEmpty(dr["ID"].ToString())) ? 0 : int.Parse(dr["ID"].ToString());
                clase.cliente.nombre = (String.IsNullOrEmpty(dr["cliente"].ToString())) ? "" : Convert.ToString(dr["cliente"]);
                clase.cliente.cedula = (String.IsNullOrEmpty(dr["cedula"].ToString())) ? "" : Convert.ToString(dr["cedula"]);
                clase.unidadesvendidas = (String.IsNullOrEmpty(dr["Vendidos"].ToString())) ? "" : Convert.ToString(dr["Vendidos"]);
                clase.Valor = (String.IsNullOrEmpty(dr["precio_total"].ToString())) ? "" : Convert.ToString(dr["precio_total"]);
                clase.fechaventa = (String.IsNullOrEmpty(dr["Fecha"].ToString())) ? "" : Convert.ToString(dr["Fecha"]);
                clase.estado = (String.IsNullOrEmpty(dr["estado"].ToString())) ? "" : Convert.ToString(dr["estado"]);


                lista.Add(clase);
            }

            return lista;
        }
        public Boolean InsertVenta(List<Venta> pListaDetalle, string cliente, string observacion, string total)
        {

            connection();

            SqlTransaction tx;
            con.Open();
            tx = con.BeginTransaction("SampleTransaction");

            try
            {

                string usuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace("AIB\\", "");


                string sql = "INSERT INTO [dbo].[Ventas] ([id_cliente]," +
                    "[precio_total],[id_user],[Observacion],[Fecha]) " +
                    "VALUES('"+cliente+"','"+ total + "','"+usuario+"','"+observacion+"',getdate())";
                SqlCommand cmd = new SqlCommand(sql, con, tx);
                cmd.ExecuteNonQuery();


                int idVenta = 0;

                SqlCommand cmd1 = new SqlCommand("SELECT top 1 [Id] FROM [dbo].[Ventas] order by Id desc", con, tx);
                cmd1.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd1);
                DataTable dt = new DataTable();


                sd.Fill(dt);


                foreach (DataRow dr in dt.Rows)
                {

                    idVenta = (String.IsNullOrEmpty(dr["Id"].ToString())) ? 0 : int.Parse(dr["Id"].ToString());

                }
                string sqllist = "";
                foreach (var x in pListaDetalle)
                {
                    sqllist += " INSERT INTO [dbo].[Venta_Cerdo] ([IdVenta],[IdCerdo],[ValorKg]," +
                    "[Peso],[ValorTotal],[TipoVenta]) " +
                    "VALUES('"+idVenta+"','"+x.IdCerdo+"','"+x.ValorKG+ "','" + x.Peso + "','" + x.Valor + "','" + x.TipoVenta + "' ); update cerdos set id_estado='2',id_Venta='"+idVenta+"' where id='" + x.IdCerdo+"';  ";

                }


               
                SqlCommand cmd2 = new SqlCommand(sqllist, con, tx);






                cmd2.ExecuteNonQuery();






                tx.Commit();

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                tx.Rollback();
                return false;

                // throw;

            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public Boolean anular(int id)
        {

            connection();

            SqlTransaction tx;
            con.Open();
            tx = con.BeginTransaction("SampleTransaction");

            try
            {

                string usuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace("AIB\\", "");


                string sql = "update Ventas set Estado='ANULADO' where ID='" + id+ "'; update Cerdos set id_estado ='1' where id_Venta='"+id+"'";
                SqlCommand cmd = new SqlCommand(sql, con, tx);
                cmd.ExecuteNonQuery();


               





                tx.Commit();

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                tx.Rollback();
                return false;

                // throw;

            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public Cerdoss DetalleCerdo(Cerdoss clase)
        {

            connection();
            SqlCommand cmd = new SqlCommand("select c.peso,c.sexo,r.nombre as raza,c.fecha_nacimiento,c.fecha_destete,c.numero_tetas,c.castrado " +
                "from cerdos c inner join razas r" +
                " on r.id = c.id_raza where c.id= '"+clase.ID+"'", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                clase.Peso = (String.IsNullOrEmpty(dr["peso"].ToString())) ? "" : Convert.ToString(dr["peso"]);
                clase.Sexo = (String.IsNullOrEmpty(dr["sexo"].ToString())) ? "" : Convert.ToString(dr["sexo"]);
                clase.raza.Nombre = (String.IsNullOrEmpty(dr["raza"].ToString())) ? "" : Convert.ToString(dr["raza"]);
                clase.fecha_nacimiento = (String.IsNullOrEmpty(dr["fecha_nacimiento"].ToString())) ? "" : Convert.ToString(dr["fecha_nacimiento"]);
                clase.fecha_destete = (String.IsNullOrEmpty(dr["fecha_destete"].ToString())) ? "" : Convert.ToString(dr["fecha_destete"]);
                clase.numero_tetas = (String.IsNullOrEmpty(dr["numero_tetas"].ToString())) ? "" : Convert.ToString(dr["numero_tetas"]);
                clase.castrado = (String.IsNullOrEmpty(dr["castrado"].ToString())) ? "" : Convert.ToString(dr["castrado"]);

                
            }

            return clase;

        }

        public List<Corral> ReadAllCorrals()
        {
            connection();
            List<Corral> lista = new List<Corral>();
            SqlCommand cmd = new SqlCommand("select ID, numeroCorral from Corrals order by numeroCorral asc", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Corral clase = new Corral();
                clase.ID = (String.IsNullOrEmpty(dr["ID"].ToString())) ? 0 : int.Parse(dr["ID"].ToString());
                clase.numeroCorral = (String.IsNullOrEmpty(dr["numeroCorral"].ToString())) ? "" : Convert.ToString(dr["numeroCorral"]);
              
                lista.Add(clase);
            }

            return lista;
        }

        public List<Clientess> ReadAllClientes()
        {
            connection();
            List<Clientess> lista = new List<Clientess>();
            SqlCommand cmd = new SqlCommand("select id, cedula, UPPER(nombre) as nombre from Cliente order by nombre asc", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Clientess clase = new Clientess();
                clase.id = (String.IsNullOrEmpty(dr["id"].ToString())) ? 0 : int.Parse(dr["id"].ToString());
                clase.cedula = (String.IsNullOrEmpty(dr["cedula"].ToString())) ? "" : Convert.ToString(dr["cedula"]);
                clase.nombre = (String.IsNullOrEmpty(dr["nombre"].ToString())) ? "" : Convert.ToString(dr["nombre"]);

                lista.Add(clase);
            }

            return lista;
        }

        public List<Cerdo> ReadCerdosByCorral(int idcorral)
        {
            connection();
            List<Cerdo> lista = new List<Cerdo>();
            SqlCommand cmd = new SqlCommand("select ID,codigo from Cerdos where id_Corral='"+idcorral+ "' and id_estado !='2' order by codigo asc", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Cerdo clase = new Cerdo();
                clase.ID = (String.IsNullOrEmpty(dr["ID"].ToString())) ? 0 : int.Parse(dr["ID"].ToString());
                clase.codigo = (String.IsNullOrEmpty(dr["codigo"].ToString())) ? "" : Convert.ToString(dr["codigo"]);

                lista.Add(clase);
            }

            return lista;
        }

        public List<Cerdo> ReadCerdosByCorralCriterio(int idcorral,string criterio)
        {
            connection();
            List<Cerdo> lista = new List<Cerdo>();
            SqlCommand cmd = new SqlCommand("select ID,codigo from Cerdos where id_Corral='" + idcorral + "' and codigo like '"+criterio+ "%' and id_estado !='2' order by codigo asc", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Cerdo clase = new Cerdo();
                clase.ID = (String.IsNullOrEmpty(dr["ID"].ToString())) ? 0 : int.Parse(dr["ID"].ToString());
                clase.codigo = (String.IsNullOrEmpty(dr["codigo"].ToString())) ? "" : Convert.ToString(dr["codigo"]);

                lista.Add(clase);
            }

            return lista;
        }

        public DataTable ExportReportByClient(string f1, string f2, string id)
        {
            

            connection();
            List<Cerdo> lista = new List<Cerdo>();

            string sql = "";
            if (id == null)
            {
                sql = "select v.ID,upper(c.nombre) as cliente," +
                "c.cedula,v.[Fecha],(select count(*) from Venta_Cerdo vc where vc.IdVenta=v.ID) " +
                "as Vendidos,v.estado,v.[precio_total] from Ventas v inner join Cliente c on c.id = v.id_cliente " +
                "where Estado ='EXITOSO' and (cast (v.Fecha as date) >=  '" + f1 + "'  and cast(v.Fecha as date) <= '" + f2 + "' )  order by v.ID desc";
            }
            else
            {
                sql = "select v.ID,upper(c.nombre) as cliente," +
                "c.cedula,v.[Fecha],(select count(*) from Venta_Cerdo vc where vc.IdVenta=v.ID) " +
                "as Vendidos,v.estado,v.[precio_total] from Ventas v inner join Cliente c on c.id = v.id_cliente " +
                "where Estado ='EXITOSO' and (cast (v.Fecha as date) >=  '" + f1 + "'  and cast(v.Fecha as date) <= '" + f2 + "' ) and c.id='" + id + "'  order by v.ID desc";

            }
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            return dt;

        }

        public DataTable ExportReportFactura(string id, string nombre, string fecha)
        {


            connection();
            List<Cerdo> lista = new List<Cerdo>();

            string sql = "";
           
                sql = "select '_' + c.codigo  as COD, cv.TipoVenta,cv.ValorTotal,cv.ValorKg,c.peso,c.sexo,r.nombre as raza from cerdos c inner join razas r on r.id = c.id_raza inner join Venta_Cerdo cv on cv.IdCerdo = c.ID where cv.IdVenta ='" + id + "'";

            
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            return dt;

        }
    }
}
