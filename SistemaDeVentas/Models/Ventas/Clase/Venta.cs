﻿using SistemaDeVentas.Areas.Clientes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Models.Ventas.Clase
{
    public class Venta
    { 
        public int Id { get; set; }
        public string IdCerdo { get; set; }
        public string Item { get; set; }
        public string Valor { get; set; }
        public string Peso { get; set; }
        public string TipoVenta { get; set; }
        public string ValorKG { get; set; }

        public Clientess cliente { get; set; } = new Clientess();

        public string observacion { get; set; }
        public string unidadesvendidas { get; set; }
        public string fechaventa { get; set; }
        public string estado { get; set; }
        public string raza { get; set; }
        public string sexo { get; set; }
    }
}
