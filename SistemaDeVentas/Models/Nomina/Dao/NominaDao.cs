﻿using SistemaDeVentas.Models.Nomina.Clase;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Models.Nomina.Dao
{
    public class NominaDao
    {
        private SqlConnection con;
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["GanadoPorcino"].ToString();
            con = new SqlConnection(constring);
        }

        public List<Pago> readAllPagosReporte(string f1, string f2)
        {
            connection();
            List<Pago> lista = new List<Pago>();
            SqlCommand cmd = new SqlCommand("select PagoNomina,FechaPago from Nom_Pago  where (cast (FechaPago as date) >=  '"+f1+"'  and cast(FechaPago as date) <= '"+f2+"' )", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Pago clase = new Pago();

                clase.PagoNomina = (String.IsNullOrEmpty(dr["PagoNomina"].ToString())) ? "" : Convert.ToString(dr["PagoNomina"]);
                clase.FechaPago = (String.IsNullOrEmpty(dr["FechaPago"].ToString())) ? "" : Convert.ToString(dr["FechaPago"]);


                lista.Add(clase);
            }

            return lista;
        }
        public List<Pago> readAllPagos(Pago p)
        {
            connection();
            List<Pago> lista = new List<Pago>();
            SqlCommand cmd = new SqlCommand("select PagoNomina, FechaPago from Nom_Pago where idempleado='"+p.IdEmpleado+ "'  order by IdPago desc", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Pago clase = new Pago();
               
                clase.PagoNomina = (String.IsNullOrEmpty(dr["PagoNomina"].ToString())) ? "" : Convert.ToString(dr["PagoNomina"]);
                clase.FechaPago = (String.IsNullOrEmpty(dr["FechaPago"].ToString())) ? "" : Convert.ToString(dr["FechaPago"]);
               

                lista.Add(clase);
            }

            return lista;
        }
        public List<Empleado> readAll()
        {
            connection();
            List<Empleado> lista = new List<Empleado>();
            SqlCommand cmd = new SqlCommand("SELECT [IdEmpleado],[Identificacion],[Nombre],[FechaCreado],[Cargo],[Salario],[FechaUltimoPago],[ValorUltimoPago],[Estado] FROM [dbo].[Nom_Empleado]", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Empleado clase = new Empleado();
                clase.IdEmpleado = (String.IsNullOrEmpty(dr["IdEmpleado"].ToString())) ? 0 : int.Parse(dr["IdEmpleado"].ToString());
                clase.Identificacion = (String.IsNullOrEmpty(dr["Identificacion"].ToString())) ? "" : Convert.ToString(dr["Identificacion"]);
                clase.Nombre = (String.IsNullOrEmpty(dr["Nombre"].ToString())) ? "" : Convert.ToString(dr["Nombre"]);
                clase.FechaCreado = (String.IsNullOrEmpty(dr["FechaCreado"].ToString())) ? "" : Convert.ToString(dr["FechaCreado"]);
                clase.Cargo = (String.IsNullOrEmpty(dr["Cargo"].ToString())) ? "" : Convert.ToString(dr["Cargo"]);
                clase.Salario = (String.IsNullOrEmpty(dr["Salario"].ToString())) ? "" : Convert.ToString(dr["Salario"]);
                clase.FechaUltimoPago = (String.IsNullOrEmpty(dr["FechaUltimoPago"].ToString())) ? "" : Convert.ToString(dr["FechaUltimoPago"]);
                clase.ValorUltimoPago = (String.IsNullOrEmpty(dr["ValorUltimoPago"].ToString())) ? "" : Convert.ToString(dr["ValorUltimoPago"]);
                clase.Estado = (String.IsNullOrEmpty(dr["Estado"].ToString())) ? "" : Convert.ToString(dr["Estado"]);
              
                lista.Add(clase);
            }

            return lista;
        }


        public List<Empleado> readAllNominaReporte(string f1, string f2)
        {
            connection();
            List<Empleado> lista = new List<Empleado>();
            SqlCommand cmd = new SqlCommand("SELECT [IdEmpleado],[Identificacion],[Nombre],[FechaCreado],[Cargo],[Salario],[FechaUltimoPago],[ValorUltimoPago],[Estado] FROM [dbo].[Nom_Empleado]   WHERE  (cast (FechaUltimoPago as date) >=  '"+f1+"'  and cast(FechaUltimoPago as date) <= '"+f2+"' )", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Empleado clase = new Empleado();
                clase.IdEmpleado = (String.IsNullOrEmpty(dr["IdEmpleado"].ToString())) ? 0 : int.Parse(dr["IdEmpleado"].ToString());
                clase.Identificacion = (String.IsNullOrEmpty(dr["Identificacion"].ToString())) ? "" : Convert.ToString(dr["Identificacion"]);
                clase.Nombre = (String.IsNullOrEmpty(dr["Nombre"].ToString())) ? "" : Convert.ToString(dr["Nombre"]);
                clase.FechaCreado = (String.IsNullOrEmpty(dr["FechaCreado"].ToString())) ? "" : Convert.ToString(dr["FechaCreado"]);
                clase.Cargo = (String.IsNullOrEmpty(dr["Cargo"].ToString())) ? "" : Convert.ToString(dr["Cargo"]);
                clase.Salario = (String.IsNullOrEmpty(dr["Salario"].ToString())) ? "" : Convert.ToString(dr["Salario"]);
                clase.FechaUltimoPago = (String.IsNullOrEmpty(dr["FechaUltimoPago"].ToString())) ? "" : Convert.ToString(dr["FechaUltimoPago"]);
                clase.ValorUltimoPago = (String.IsNullOrEmpty(dr["ValorUltimoPago"].ToString())) ? "" : Convert.ToString(dr["ValorUltimoPago"]);
                clase.Estado = (String.IsNullOrEmpty(dr["Estado"].ToString())) ? "" : Convert.ToString(dr["Estado"]);

                lista.Add(clase);
            }

            return lista;
        }
        public Empleado Detalle(Empleado e)
        {
            connection();
            List<Empleado> lista = new List<Empleado>();
            SqlCommand cmd = new SqlCommand("SELECT [IdEmpleado],[Identificacion],[Nombre],[FechaCreado],[Cargo],[Salario],[FechaUltimoPago],[ValorUltimoPago],[Estado] FROM [dbo].[Nom_Empleado] where  [IdEmpleado] ='" + e.IdEmpleado + "'", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();
            Empleado clase = new Empleado();
            foreach (DataRow dr in dt.Rows)
            {
               
                clase.IdEmpleado = (String.IsNullOrEmpty(dr["IdEmpleado"].ToString())) ? 0 : int.Parse(dr["IdEmpleado"].ToString());
                clase.Identificacion = (String.IsNullOrEmpty(dr["Identificacion"].ToString())) ? "" : Convert.ToString(dr["Identificacion"]);
                clase.Nombre = (String.IsNullOrEmpty(dr["Nombre"].ToString())) ? "" : Convert.ToString(dr["Nombre"]);
                clase.FechaCreado = (String.IsNullOrEmpty(dr["FechaCreado"].ToString())) ? "" : Convert.ToString(dr["FechaCreado"]);
                clase.Cargo = (String.IsNullOrEmpty(dr["Cargo"].ToString())) ? "" : Convert.ToString(dr["Cargo"]);
                clase.Salario = (String.IsNullOrEmpty(dr["Salario"].ToString())) ? "" : Convert.ToString(dr["Salario"]);
                clase.FechaUltimoPago = (String.IsNullOrEmpty(dr["FechaUltimoPago"].ToString())) ? "" : Convert.ToString(dr["FechaUltimoPago"]);
                clase.ValorUltimoPago = (String.IsNullOrEmpty(dr["ValorUltimoPago"].ToString())) ? "" : Convert.ToString(dr["ValorUltimoPago"]);
                clase.Estado = (String.IsNullOrEmpty(dr["Estado"].ToString())) ? "" : Convert.ToString(dr["Estado"]);

                lista.Add(clase);
            }

            return clase;
        }

        public string GuardarEmpleado(string Identificacion, string Nombre, string Cargo, string Salario)
        {
            connection();

            SqlTransaction tx;
            con.Open();
            tx = con.BeginTransaction("SampleTransaction");
            string resultado = "";
            try
            {

                string usuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace("AIB\\", "");


                string sql = "INSERT INTO [dbo].[Nom_Empleado]([Identificacion],[Nombre],[FechaCreado],[Cargo],[Salario]," +
                    "[Estado])" +
                    " VALUES('"+Identificacion+ "','" + Nombre + "',getdate(),'" + Cargo + "','" + Salario + "','ACTIVO')";
                SqlCommand cmd = new SqlCommand(sql, con, tx);
                cmd.ExecuteNonQuery();



                tx.Commit();

                resultado = "INSERTADO";
                return resultado;
            }
            catch (Exception e)
            {
                Console.Write(e);
                tx.Rollback();
                resultado = "NOINSERTADO";
                return resultado;
                // throw;

            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }


        public string ActualizarEmpleado(int IdEmpleado, string Identificacion, string Nombre, string Cargo, string Salario)
        {
            connection();

            SqlTransaction tx;
            con.Open();
            tx = con.BeginTransaction("SampleTransaction");
            string resultado = "";
            try
            {

                string usuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace("AIB\\", "");


                string sql = "UPDATE [dbo].[Nom_Empleado] SET [Identificacion] ='"+Identificacion+"'" +
                    ",[Nombre] = '"+Nombre+"',[Cargo] = '"+Cargo+"',[Salario] = '"+Salario+ "',[Estado] = 'ACTIVO' WHERE IdEmpleado='"+IdEmpleado+"';";
                SqlCommand cmd = new SqlCommand(sql, con, tx);
                cmd.ExecuteNonQuery();



                tx.Commit();

                resultado = "INSERTADO";
                return resultado;
            }
            catch (Exception e)
            {
                Console.Write(e);
                tx.Rollback();
                resultado = "NOINSERTADO";
                return resultado;
                // throw;

            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        public string PagarEmpleado(int IdEmpleado, string Identificacion, string Nombre, string Cargo, string Salario)
        {
            connection();

            SqlTransaction tx;
            con.Open();
            tx = con.BeginTransaction("SampleTransaction");
            string resultado = "";
            try
            {

                string usuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace("AIB\\", "");


                string sql = "UPDATE [dbo].[Nom_Empleado] SET [Identificacion] ='" + Identificacion + "'" +
                    ",[Nombre] = '" + Nombre + "',[Cargo] = '" + Cargo + "',[Salario] = '" + Salario + "',[Estado] = 'ACTIVO'" +
                    "[FechaUltimoPago] = getdate(),[ValorUltimoPago]='"+Salario+"' WHERE IdEmpleado='" + IdEmpleado + "'; " +
                    "INSERT INTO [dbo].[Nom_Pago] ([IdEmpleado],[PagoNomina],[FechaPago]) VALUES ('"+IdEmpleado+"','"+Salario+"',getdate())";
                SqlCommand cmd = new SqlCommand(sql, con, tx);
                cmd.ExecuteNonQuery();



                tx.Commit();

                resultado = "INSERTADO";
                return resultado;
            }
            catch (Exception e)
            {
                Console.Write(e);
                tx.Rollback();
                resultado = "NOINSERTADO";
                return resultado;
                // throw;

            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        public string PagarTodosEmpleados()
        {
            connection();

            SqlTransaction tx;
            con.Open();
            tx = con.BeginTransaction("SampleTransaction");
            string resultado = "";
            try
            {

                string usuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace("AIB\\", "");


                //string sql = "UPDATE [dbo].[Nom_Empleado] SET [Identificacion] ='" + Identificacion + "'" +
                //    ",[Nombre] = '" + Nombre + "',[Cargo] = '" + Cargo + "',[Salario] = '" + Salario + "',[Estado] = 'ACTIVO'" +
                //    "[FechaUltimoPago] = getdate(),[ValorUltimoPago]='" + Salario + "' WHERE IdEmpleado='" + IdEmpleado + "'; " +
                //    "INSERT INTO [dbo].[Nom_Pago] ([IdEmpleado],[PagoNomina],[FechaPago]) VALUES ('" + IdEmpleado + "','" + Salario + "',getdate()); " +
                //    "update Nom_Empleado  set FechaUltimoPago = GETDATE(),ValorUltimoPago=Salario where Estado='ACTIVO' ";

                string sql = "INSERT INTO [dbo].[Nom_Pago] ([IdEmpleado],[PagoNomina],[FechaPago]) select IdEmpleado,Salario,GETDATE() from Nom_Empleado where Estado='ACTIVO'; " +
                    "update Nom_Empleado  set FechaUltimoPago = GETDATE(), ValorUltimoPago = Salario where Estado = 'ACTIVO'; ";
                SqlCommand cmd = new SqlCommand(sql, con, tx);
                cmd.ExecuteNonQuery();



                tx.Commit();

                resultado = "INSERTADO";
                return resultado;
            }
            catch (Exception e)
            {
                Console.Write(e);
                tx.Rollback();
                resultado = "NOINSERTADO";
                return resultado;
                // throw;

            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        public string DevolverPago()
        {
            connection();

            SqlTransaction tx;
            con.Open();
            tx = con.BeginTransaction("SampleTransaction");
            string resultado = "";
            try
            {

                string usuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace("AIB\\", "");


                //string sql = "UPDATE [dbo].[Nom_Empleado] SET [Identificacion] ='" + Identificacion + "'" +
                //    ",[Nombre] = '" + Nombre + "',[Cargo] = '" + Cargo + "',[Salario] = '" + Salario + "',[Estado] = 'ACTIVO'" +
                //    "[FechaUltimoPago] = getdate(),[ValorUltimoPago]='" + Salario + "' WHERE IdEmpleado='" + IdEmpleado + "'; " +
                //    "INSERT INTO [dbo].[Nom_Pago] ([IdEmpleado],[PagoNomina],[FechaPago]) VALUES ('" + IdEmpleado + "','" + Salario + "',getdate()); " +
                //    "update Nom_Empleado  set FechaUltimoPago = GETDATE(),ValorUltimoPago=Salario where Estado='ACTIVO' ";

                string sql = "delete from [dbo].[Nom_Pago] where [FechaPago] = CONVERT (date, GETDATE()); update Nom_Empleado  set FechaUltimoPago = null, ValorUltimoPago = null where Estado = 'ACTIVO';  " +
                    "UPDATE      Nom_Empleado SET Nom_Empleado.ValorUltimoPago = Nom_Pago.PagoNomina, Nom_Empleado.FechaUltimoPago = Nom_Pago.FechaPago FROM   Nom_Empleado INNER JOIN  Nom_Pago ON    Nom_Empleado.IdEmpleado = Nom_Pago.IdEmpleado ;";
                SqlCommand cmd = new SqlCommand(sql, con, tx);
                cmd.ExecuteNonQuery();



                tx.Commit();

                resultado = "INSERTADO";
                return resultado;
            }
            catch (Exception e)
            {
                Console.Write(e);
                tx.Rollback();
                resultado = "NOINSERTADO";
                return resultado;
                // throw;

            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        public string ActivarEmpleado(int IdEmpleado)
        {
            connection();

            SqlTransaction tx;
            con.Open();
            tx = con.BeginTransaction("SampleTransaction");
            string resultado = "";
            try
            {

                string usuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace("AIB\\", "");


                //string sql = "UPDATE [dbo].[Nom_Empleado] SET [Identificacion] ='" + Identificacion + "'" +
                //    ",[Nombre] = '" + Nombre + "',[Cargo] = '" + Cargo + "',[Salario] = '" + Salario + "',[Estado] = 'ACTIVO'" +
                //    "[FechaUltimoPago] = getdate(),[ValorUltimoPago]='" + Salario + "' WHERE IdEmpleado='" + IdEmpleado + "'; " +
                //    "INSERT INTO [dbo].[Nom_Pago] ([IdEmpleado],[PagoNomina],[FechaPago]) VALUES ('" + IdEmpleado + "','" + Salario + "',getdate()); " +
                //    "update Nom_Empleado  set FechaUltimoPago = GETDATE(),ValorUltimoPago=Salario where Estado='ACTIVO' ";

                string sql = "update Nom_Empleado  set Estado ='ACTIVO' where IdEmpleado ='"+ IdEmpleado + "'";
                SqlCommand cmd = new SqlCommand(sql, con, tx);
                cmd.ExecuteNonQuery();



                tx.Commit();

                resultado = "INSERTADO";
                return resultado;
            }
            catch (Exception e)
            {
                Console.Write(e);
                tx.Rollback();
                resultado = "NOINSERTADO";
                return resultado;
                // throw;

            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }
        public string DesactivarEmpleado(int IdEmpleado)
        {
            connection();

            SqlTransaction tx;
            con.Open();
            tx = con.BeginTransaction("SampleTransaction");
            string resultado = "";
            try
            {

                string usuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace("AIB\\", "");


                //string sql = "UPDATE [dbo].[Nom_Empleado] SET [Identificacion] ='" + Identificacion + "'" +
                //    ",[Nombre] = '" + Nombre + "',[Cargo] = '" + Cargo + "',[Salario] = '" + Salario + "',[Estado] = 'ACTIVO'" +
                //    "[FechaUltimoPago] = getdate(),[ValorUltimoPago]='" + Salario + "' WHERE IdEmpleado='" + IdEmpleado + "'; " +
                //    "INSERT INTO [dbo].[Nom_Pago] ([IdEmpleado],[PagoNomina],[FechaPago]) VALUES ('" + IdEmpleado + "','" + Salario + "',getdate()); " +
                //    "update Nom_Empleado  set FechaUltimoPago = GETDATE(),ValorUltimoPago=Salario where Estado='ACTIVO' ";

                string sql = "update Nom_Empleado  set Estado ='INACTIVO' where IdEmpleado ='" + IdEmpleado + "'";
                SqlCommand cmd = new SqlCommand(sql, con, tx);
                cmd.ExecuteNonQuery();



                tx.Commit();

                resultado = "INSERTADO";
                return resultado;
            }
            catch (Exception e)
            {
                Console.Write(e);
                tx.Rollback();
                resultado = "NOINSERTADO";
                return resultado;
                // throw;

            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }
    }
}
