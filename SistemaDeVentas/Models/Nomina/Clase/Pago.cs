﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Models.Nomina.Clase
{
    public class Pago
    {
        public int IdPago { get; set; }
        public int IdEmpleado { get; set; }
        public string PagoNomina { get; set; }
        public string FechaPago { get; set; }
    }
}
