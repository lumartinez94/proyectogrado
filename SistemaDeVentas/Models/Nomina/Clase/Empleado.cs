﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Models.Nomina.Clase
{
    public class Empleado
    {
        public int IdEmpleado { get; set; }
        public string Identificacion { get; set; }
        public string Nombre { get; set; }
        public string FechaCreado { get; set; }
        public string Cargo { get; set; }
        public string Salario { get; set; }
        public string FechaUltimoPago { get; set; }
        public string ValorUltimoPago { get; set; }
        public string Estado { get; set; }
      
    }
}
