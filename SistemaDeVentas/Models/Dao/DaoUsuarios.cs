﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using SistemaDeVentas.Models.Dao.Clases;
namespace SistemaDeVentas.Models.Dao
{
    public class DaoUsuarios
    {

        public SqlConnection con;
        public string cadenaConexion = "Data Source=ASUS-H110-A7M-2;Initial catalog=Ganado_Porcino;Trusted_Connection=True;Integrated Security=True";
        public List<Usuario> User(string email,string password)
        {
            try
            {
                List<Usuario> usuarios = new List<Usuario>();
                Usuario usuario = new Usuario();
                string Query = "Select email,password from  usuarios where email=@email and password=@password";
                con = new SqlConnection(cadenaConexion);
            
                SqlCommand cmd = new SqlCommand(Query, con);
                con.Open();
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@password", password);
                cmd.CommandType = CommandType.Text;
                
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
                con.Close();
                foreach ( DataRow Row in dt.Rows)
                {
                  
                    usuario.Email = (String.IsNullOrEmpty(Row["email"].ToString()) ? "":Convert.ToString(Row["email"]));
                  usuario.Password= (String.IsNullOrEmpty(Row["password"].ToString()) ? "" : Convert.ToString(Row["password"]));
                    usuarios.Add(usuario);
                }
                return usuarios;
            }
            
            catch(Exception)
            {
                throw;
            }
           
        }

        public bool ComprobarUser(string email, string password)
        {
            try
            {
                Boolean result = false;
                Usuario usuario = new Usuario();
                string Query = "Select email,password from  usuarios where email=@email and password=@password";
                con = new SqlConnection(cadenaConexion);
               
                SqlCommand cmd = new SqlCommand(Query, con);
                con.Open();
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@password", password);
                cmd.CommandType = CommandType.Text;
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                
                da.Fill(dt);
                con.Close();
                foreach (DataRow Row in dt.Rows)
                {

                    usuario.Email = (String.IsNullOrEmpty(Row["email"].ToString()) ? "" : Convert.ToString(Row["email"]));
                    usuario.Password = (String.IsNullOrEmpty(Row["password"].ToString()) ? "" : Convert.ToString(Row["password"]));
                    result = true;
                }
                return result;
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}
