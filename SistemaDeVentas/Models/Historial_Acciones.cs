﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Models
{
    public class Historial_Acciones
    {

        public int ID { get; set; }
        public DateTime fecha_registro { get; set; }

        public string Accion { get; set; }

        public string IdUser { get; set; }

        public string IdRole { get; set; }

        public string item { get; set; }
    }
}
