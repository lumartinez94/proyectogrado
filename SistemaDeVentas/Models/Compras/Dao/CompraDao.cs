﻿using SistemaDeVentas.Models.Compras.Clase;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Models.Compras.Dao
{
    public class CompraDao
    {
         

        private SqlConnection con;
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["GanadoPorcino"].ToString();
            con = new SqlConnection(constring);
        }

        public List<Compra> ReadComprasReporte(string f1, string f2)
        {
            connection();
            List<Compra> lista = new List<Compra>();
            string sql = "SELECT TipoDeCompra,[ValorIndividual],[Cantidad] FROM[dbo].[Compras] where (cast (FechaCreado as date) >=  '" + f1 + "'  and cast(FechaCreado as date) <= '" + f2 + "' )";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();
            int valor = 0;
            int cantidad = 0;
            foreach (DataRow dr in dt.Rows)
            {
                Compra clase = new Compra();

                valor = int.Parse((String.IsNullOrEmpty(dr["ValorIndividual"].ToString())) ? "" : Convert.ToString(dr["ValorIndividual"]).Replace(",", ""));
                cantidad = (String.IsNullOrEmpty(dr["Cantidad"].ToString())) ? 0 : int.Parse(dr["Cantidad"].ToString());
                clase.TipoDeCompra = (String.IsNullOrEmpty(dr["TipoDeCompra"].ToString())) ? "" : Convert.ToString(dr["TipoDeCompra"]);
                clase.ValorIndividual = (valor * cantidad).ToString();
               
                clase.Cantidad = (String.IsNullOrEmpty(dr["Cantidad"].ToString())) ? 0 : int.Parse(dr["Cantidad"].ToString());

                lista.Add(clase);
            }

            return lista;
        }
        public Compra totalcompra(string f1,string f2,string tipoCompra)
        {
            Compra compra = null;
            float total = 0;
            try
            {
                if (tipoCompra != null)
                {

                    connection();
                    List<Compra> lista = new List<Compra>();
                    SqlCommand cmd = new SqlCommand("SELECT [Id],[Nombre],[Descripcion],[FechaCreado],[TipoDeCompra],[ValorIndividual],[Cantidad],[UsuarioCreado] FROM [dbo].[Compras] WHERE (cast (FechaCreado as date) >=  '" + f1 + "'  and cast(FechaCreado as date) <=  '" + f2 + "' ) and TipoDeCompra='" + tipoCompra + "'", con);
                    cmd.CommandType = CommandType.Text;
                    SqlDataAdapter sd = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();

                    con.Open();
                    sd.Fill(dt);
                    con.Close();


                    foreach (DataRow dr in dt.Rows)
                    {
                        var valor = dr["ValorIndividual"].ToString().Replace(",", "");
                        total += float.Parse(dr["Cantidad"].ToString()) * float.Parse(valor);
                    }

                    compra = new Compra
                    {

                        total = total.ToString()
                    };

                }
                else
                {

                    connection();
                    List<Compra> lista = new List<Compra>();
                    SqlCommand cmd = new SqlCommand("SELECT [Id],[Nombre],[Descripcion],[FechaCreado],[TipoDeCompra],[ValorIndividual],[Cantidad],[UsuarioCreado] FROM [dbo].[Compras] WHERE (cast (FechaCreado as date) >=  '" + f1 + "'  and cast(FechaCreado as date) <=  '" + f2 + "' )", con);
                    cmd.CommandType = CommandType.Text;
                    SqlDataAdapter sd = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();

                    con.Open();
                    sd.Fill(dt);
                    con.Close();

                    foreach (DataRow dr in dt.Rows)
                    {
                        var valor = dr["ValorIndividual"].ToString().Replace(",", "");
                        total += float.Parse(dr["Cantidad"].ToString()) * float.Parse(valor);
                    }

                    compra = new Compra
                    {

                        total = total.ToString()
                    };


                }


            }


            catch (Exception ex)
            {

                throw ex;
            }

            return compra;

        }

        public DataTable CompraTable(string f1, string f2, string tipoCompra)



        {
            DataTable dt = new DataTable();

            if (tipoCompra != null)
            {

                connection();

                SqlCommand cmd = new SqlCommand("SELECT [Id],[Nombre],[Descripcion],[FechaCreado],[TipoDeCompra],[ValorIndividual],[Cantidad],[UsuarioCreado] FROM [dbo].[Compras] WHERE (cast (FechaCreado as date) >=  '" + f1 + "'  and cast(FechaCreado as date) <=  '" + f2 + "' ) and TipoDeCompra='" + tipoCompra + "'", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
              

                con.Open();
                sd.Fill(dt);
                con.Close();





            }
            else
            {
                connection();

                SqlCommand cmd = new SqlCommand("SELECT [Id],[Nombre],[Descripcion],[FechaCreado],[TipoDeCompra],[ValorIndividual],[Cantidad],[UsuarioCreado] FROM [dbo].[Compras] WHERE (cast (FechaCreado as date) >=  '" + f1 + "'  and cast(FechaCreado as date) <=  '" + f2 + "' )", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
            

                con.Open();
                sd.Fill(dt);
                con.Close();

               
                
            }

            return dt;
        }
        public List<Compra> readAllReporte(string f1, string f2, string tipoCompra)



        {
            List<Compra> lista = new List<Compra>();

            if (tipoCompra != null)
            {

                connection();
                
                SqlCommand cmd = new SqlCommand("SELECT [Id],[Nombre],[Descripcion],[FechaCreado],[TipoDeCompra],[ValorIndividual],[Cantidad],[UsuarioCreado] FROM [dbo].[Compras] WHERE (cast (FechaCreado as date) >=  '" + f1 + "'  and cast(FechaCreado as date) <=  '" + f2 + "' ) and TipoDeCompra='" + tipoCompra + "'", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();


                foreach (DataRow dr in dt.Rows)
                {
                    Compra clase = new Compra();
                    clase.Id = (String.IsNullOrEmpty(dr["Id"].ToString())) ? 0 : int.Parse(dr["Id"].ToString());
                    clase.Nombre = (String.IsNullOrEmpty(dr["Nombre"].ToString())) ? "" : Convert.ToString(dr["Nombre"]);
                    clase.FechaCreado = (String.IsNullOrEmpty(dr["FechaCreado"].ToString())) ? "" : Convert.ToString(dr["FechaCreado"]);
                    clase.TipoDeCompra = (String.IsNullOrEmpty(dr["TipoDeCompra"].ToString())) ? "" : Convert.ToString(dr["TipoDeCompra"]);
                    clase.ValorIndividual = (String.IsNullOrEmpty(dr["ValorIndividual"].ToString())) ? "" : Convert.ToString(dr["ValorIndividual"]);
                    clase.Nombre = (String.IsNullOrEmpty(dr["Nombre"].ToString())) ? "" : Convert.ToString(dr["Nombre"]);
                    clase.Cantidad = (String.IsNullOrEmpty(dr["Cantidad"].ToString())) ? 0 : int.Parse(dr["Cantidad"].ToString());
                    //clase.total = clase.total + Int16.Parse(clase.ValorIndividual.ToString().Replace(",", "")); 
                    lista.Add(clase);
                }



            }
            else
            {
                connection();
             
                SqlCommand cmd = new SqlCommand("SELECT [Id],[Nombre],[Descripcion],[FechaCreado],[TipoDeCompra],[ValorIndividual],[Cantidad],[UsuarioCreado] FROM [dbo].[Compras] WHERE (cast (FechaCreado as date) >=  '" + f1 + "'  and cast(FechaCreado as date) <=  '" + f2 + "' )", con);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                sd.Fill(dt);
                con.Close();

                foreach (DataRow dr in dt.Rows)
                {
                    Compra clase = new Compra();
                    clase.Id = (String.IsNullOrEmpty(dr["Id"].ToString())) ? 0 : int.Parse(dr["Id"].ToString());
                    clase.Nombre = (String.IsNullOrEmpty(dr["Nombre"].ToString())) ? "" : Convert.ToString(dr["Nombre"]);
                    clase.FechaCreado = (String.IsNullOrEmpty(dr["FechaCreado"].ToString())) ? "" : Convert.ToString(dr["FechaCreado"]);
                    clase.TipoDeCompra = (String.IsNullOrEmpty(dr["TipoDeCompra"].ToString())) ? "" : Convert.ToString(dr["TipoDeCompra"]);
                    clase.ValorIndividual = (String.IsNullOrEmpty(dr["ValorIndividual"].ToString())) ? "" : Convert.ToString(dr["ValorIndividual"]);
                    clase.Nombre = (String.IsNullOrEmpty(dr["Nombre"].ToString())) ? "" : Convert.ToString(dr["Nombre"]);
                    clase.Cantidad = (String.IsNullOrEmpty(dr["Cantidad"].ToString())) ? 0 : int.Parse(dr["Cantidad"].ToString());

                    lista.Add(clase);
                }
            }

            return lista;
        }
        public List<Compra> readAll()
        {
            connection();
            List<Compra> lista = new List<Compra>();
            SqlCommand cmd = new SqlCommand("SELECT [Id],[Nombre],[Descripcion],[FechaCreado],[TipoDeCompra],[ValorIndividual],[Cantidad],[UsuarioCreado] FROM [dbo].[Compras]", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Compra clase = new Compra();
                clase.Id = (String.IsNullOrEmpty(dr["Id"].ToString())) ? 0 : int.Parse(dr["Id"].ToString());
                clase.Nombre  = (String.IsNullOrEmpty(dr["Nombre"].ToString())) ? "" : Convert.ToString(dr["Nombre"]);
                clase.FechaCreado = (String.IsNullOrEmpty(dr["FechaCreado"].ToString())) ? "" : Convert.ToString(dr["FechaCreado"]);
                clase.TipoDeCompra = (String.IsNullOrEmpty(dr["TipoDeCompra"].ToString())) ? "" : Convert.ToString(dr["TipoDeCompra"]);
                clase.ValorIndividual = (String.IsNullOrEmpty(dr["ValorIndividual"].ToString())) ? "" : Convert.ToString(dr["ValorIndividual"]);
                clase.Nombre  = (String.IsNullOrEmpty(dr["Nombre"].ToString())) ? "" : Convert.ToString(dr["Nombre"]);
                clase.Cantidad = (String.IsNullOrEmpty(dr["Cantidad"].ToString())) ? 0 : int.Parse(dr["Cantidad"].ToString());

                lista.Add(clase);
            }

            return lista;
        }


        public List<Compra> readComprasGroup()
        {
            connection();
            List<Compra> lista = new List<Compra>();
            SqlCommand cmd = new SqlCommand("select nombre from Compras group by nombre order by nombre asc", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Compra clase = new Compra();
                clase.Nombre = (String.IsNullOrEmpty(dr["Nombre"].ToString())) ? "" : Convert.ToString(dr["Nombre"]);
                
                lista.Add(clase);
            }

            return lista;
        }



        public Boolean Insert(Compra clase)
        {

            connection();

            SqlTransaction tx;
            con.Open();
            tx = con.BeginTransaction("SampleTransaction");

            try
            {

                string usuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace("AIB\\", "");


                string sql = "INSERT INTO [Compras] ([Nombre],[Descripcion],[FechaCreado]," +
                    "[TipoDeCompra],[ValorIndividual]," +
                    "[Cantidad],[UsuarioCreado]) VALUES('"+clase.Nombre+ "','" + clase.Descripcion + "'," +
                    " getdate(),'" + clase.TipoDeCompra + "','" + clase.ValorIndividual + "','" + clase.Cantidad + "'," +
                    "'" + usuario + "')";
                SqlCommand cmd = new SqlCommand(sql, con, tx);
                cmd.ExecuteNonQuery();

              

                tx.Commit();

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                tx.Rollback();
                return false;

               // throw;

            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public Boolean InsertMedicamento(Compra clase, string marca)
        {

            connection();

            SqlTransaction tx;
            con.Open();
            tx = con.BeginTransaction("SampleTransaction");

            try
            {


                int idcompra = 0;

                SqlCommand cmd1 = new SqlCommand("SELECT top 1 [Id] FROM [dbo].[Compras] order by Id desc", con, tx);
                cmd1.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd1);
                DataTable dt = new DataTable();

                
                sd.Fill(dt);
                

                foreach (DataRow dr in dt.Rows)
                {

                    idcompra = (String.IsNullOrEmpty(dr["Id"].ToString())) ? 0 : int.Parse(dr["Id"].ToString());

                }

                string usuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace("AIB\\", "");


                string sql = "INSERT INTO[dbo].[medicamento] ([nombre],[fecha_creacion]," +
                    "[cantidad],[precio_unidad],[descripcion],[marca],IdCompra)" +
                    " VALUES('" + clase.Nombre + "',getdate(),'" + clase.Cantidad + "','" + clase.ValorIndividual.Replace(",", "") + "','" + clase.Descripcion + "','" + marca + "','"+idcompra+"')";
                SqlCommand cmd = new SqlCommand(sql, con, tx);
                cmd.ExecuteNonQuery();



                tx.Commit();

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                tx.Rollback();
                return false;

                // throw;

            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public Boolean InsertAlimento(Compra clase, string cbolineamiento, string cbocategoria, string cbounidad)
        {

            connection();

            SqlTransaction tx;
            con.Open();
            tx = con.BeginTransaction("SampleTransaction");

            try
            {

                int idcompra = 0;

                SqlCommand cmd1 = new SqlCommand("SELECT top 1 [Id] FROM [dbo].[Compras] order by Id desc", con, tx);
                cmd1.CommandType = CommandType.Text;
                SqlDataAdapter sd = new SqlDataAdapter(cmd1);
                DataTable dt = new DataTable();

               sd.Fill(dt);
                 foreach (DataRow dr in dt.Rows)
                {

                     idcompra = (String.IsNullOrEmpty(dr["Id"].ToString())) ? 0 : int.Parse(dr["Id"].ToString());

                }


               

                string sql = "INSERT INTO [dbo].[Alimento] ([nombre] ,[descripcion]," +
                    "[fecha_compra],[categoria],[precio],[cantidad],[fecha_registro],IdCompra,linaje_alimento,Unidad)" +
                    " VALUES  ('"+clase.Nombre+ "','" + clase.Descripcion + "',getdate(),'"+ cbocategoria + "','" + clase.ValorIndividual.Replace(",","") + "'," +
                    "'" + clase.Cantidad + "',getdate(),'"+ idcompra + "','"+ cbolineamiento + "','"+ cbounidad + "')";
               SqlCommand cmd = new SqlCommand(sql, con, tx);
                cmd.ExecuteNonQuery();



                tx.Commit();

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                tx.Rollback();
                return false;

                // throw;

            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public Boolean Update(Compra clase)
        {

            connection();

            SqlTransaction tx;
            con.Open();
            tx = con.BeginTransaction("SampleTransaction");

            try
            {

                string usuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace("AIB\\", "");


                string sql = "UPDATE Compras SET [Nombre] = '"+clase.Nombre+ "',[Descripcion] = '"+clase.Descripcion+"'," +
                    " [TipoDeCompra] = '" + clase.TipoDeCompra+ "',[ValorIndividual] = '"+clase.ValorIndividual.Replace(",","")+"'" +
                    ", [Cantidad]='"+clase.Cantidad+ "',[UsuarioCreado]='"+usuario+"'  where Id='"+clase.Id+"' ";
                SqlCommand cmd = new SqlCommand(sql, con, tx);
                cmd.ExecuteNonQuery();



                tx.Commit();

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                tx.Rollback();
                return false;

                // throw;

            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public Compra Read(Compra clase)
        {
            connection();
           
            SqlCommand cmd = new SqlCommand("SELECT a.Unidad,a.categoria,a.linaje_alimento,m.marca,c.[Id],c.[Nombre],c.[Descripcion],c.[FechaCreado],c.[TipoDeCompra],c.[ValorIndividual],c.[Cantidad],[UsuarioCreado] FROM [dbo].[Compras] c left join Alimento a on c.Id = a.IdCompra left join medicamento m on c.Id = m.IdCompra where c.Id ='" + clase.Id+"' ", con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
               
                clase.Id = (String.IsNullOrEmpty(dr["Id"].ToString())) ? 0 : int.Parse(dr["Id"].ToString());
                clase.Nombre = (String.IsNullOrEmpty(dr["Nombre"].ToString())) ? "" : Convert.ToString(dr["Nombre"]);
                clase.Descripcion = (String.IsNullOrEmpty(dr["Descripcion"].ToString())) ? "" : Convert.ToString(dr["Descripcion"]);
                clase.FechaCreado = (String.IsNullOrEmpty(dr["FechaCreado"].ToString())) ? "" : Convert.ToString(dr["FechaCreado"]);
                clase.TipoDeCompra = (String.IsNullOrEmpty(dr["TipoDeCompra"].ToString())) ? "" : Convert.ToString(dr["TipoDeCompra"]);
                clase.ValorIndividual = (String.IsNullOrEmpty(dr["ValorIndividual"].ToString())) ? "" : Convert.ToString(dr["ValorIndividual"]);
                clase.Nombre = (String.IsNullOrEmpty(dr["Nombre"].ToString())) ? "" : Convert.ToString(dr["Nombre"]);
                clase.Cantidad = (String.IsNullOrEmpty(dr["Cantidad"].ToString())) ? 0 : int.Parse(dr["Cantidad"].ToString());

                clase.lineamiento = (String.IsNullOrEmpty(dr["linaje_alimento"].ToString())) ? "" : Convert.ToString(dr["linaje_alimento"]);
                clase.categoria = (String.IsNullOrEmpty(dr["categoria"].ToString())) ? "" : Convert.ToString(dr["categoria"]);
                clase.marca = (String.IsNullOrEmpty(dr["marca"].ToString())) ? "" : Convert.ToString(dr["marca"]);
                clase.Unidad = (String.IsNullOrEmpty(dr["Unidad"].ToString())) ? "" : Convert.ToString(dr["Unidad"]);


            }

            return clase;
        }


        public Boolean UpdatetMedicamento(int idcompra, Compra clase, string marca)
        {

            connection();

            SqlTransaction tx;
            con.Open();
            tx = con.BeginTransaction("SampleTransaction");

            try
            {


                

           

                string usuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace("AIB\\", "");


                string sql = " delete from medicamento where IdCompra='" + idcompra + "'; INSERT INTO[dbo].[medicamento] ([nombre],[fecha_creacion]," +
                    "[cantidad],[precio_unidad],[descripcion],[marca],IdCompra)" +
                    " VALUES('" + clase.Nombre + "',getdate(),'" + clase.Cantidad + "','" + clase.ValorIndividual.Replace(",", "") + "','" + clase.Descripcion + "','" + marca + "','" + idcompra + "')";
                SqlCommand cmd = new SqlCommand(sql, con, tx);
                cmd.ExecuteNonQuery();



                tx.Commit();

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                tx.Rollback();
                return false;

                // throw;

            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public Boolean UpdateAlimento(int idcompra, Compra clase, string cbolineamiento, string cbocategoria, string cbounidad)
        {

            connection();

            SqlTransaction tx;
            con.Open();
            tx = con.BeginTransaction("SampleTransaction");

            try
            {





                string sql = " delete from alimento where IdCompra='"+ idcompra + "';INSERT INTO [dbo].[Alimento] ([nombre] ,[descripcion]," +
                    "[fecha_compra],[categoria],[precio],[cantidad],[fecha_registro],IdCompra,linaje_alimento,Unidad)" +
                    " VALUES  ('" + clase.Nombre + "','" + clase.Descripcion + "',getdate(),'" + cbocategoria + "','" + clase.ValorIndividual.Replace(",", "") + "'," +
                    "'" + clase.Cantidad + "',getdate(),'" + idcompra + "','" + cbolineamiento + "','" + cbounidad + "')";
                SqlCommand cmd = new SqlCommand(sql, con, tx);
                cmd.ExecuteNonQuery();



                tx.Commit();

                return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                tx.Rollback();
                return false;

                // throw;

            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        //public Boolean Update(Area area)
        //{

        //    connection();

        //    SqlTransaction tx;
        //    con.Open();
        //    tx = con.BeginTransaction("SampleTransaction");

        //    try
        //    {
        //        string usuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Replace("AIB\\", "");
        //        SqlCommand cmd = new SqlCommand("sp_Area", con, tx);
        //        cmd.Parameters.AddWithValue("@Oper", "U");
        //        cmd.Parameters.AddWithValue("@Description", area.description);
        //        cmd.Parameters.AddWithValue("@idArea", area.id);
        //        cmd.Parameters.AddWithValue("@User", usuario);
        //        cmd.CommandType = CommandType.StoredProcedure;

        //        cmd.ExecuteNonQuery();

        //        tx.Commit();

        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        Console.Write(e);
        //        tx.Rollback();
        //        return false;

        //        throw;

        //    }
        //    finally
        //    {
        //        con.Close();
        //        con.Dispose();
        //    }
        //}
        //public Boolean Delete(Area area)
        //{

        //    connection();

        //    SqlTransaction tx;
        //    con.Open();
        //    tx = con.BeginTransaction("SampleTransaction");

        //    try
        //    {
        //        string sQuerySolicitud = "delete from area Where IdArea='" + area.id + "'";

        //        SqlCommand cmd = new SqlCommand(sQuerySolicitud, con, tx);
        //        cmd.CommandType = CommandType.Text;

        //        cmd.ExecuteNonQuery();

        //        tx.Commit();


        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        Console.Write(e);
        //        tx.Rollback();
        //        return false;

        //        throw;

        //    }
        //    finally
        //    {
        //        con.Close();
        //        con.Dispose();
        //    }
        //}
    }
}
