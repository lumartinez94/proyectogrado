﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaDeVentas.Models.Compras.Clase
{
    public class Compra
    { 
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string FechaCreado { get; set; }
        public string TipoDeCompra { get; set; }
        public string ValorIndividual { get; set; }
        public int Cantidad { get; set; }
        public string UsuarioCreado { get; set; }


        public string marca { get; set; }
        public string categoria { get; set; }
        public string lineamiento { get; set; }
        public string Unidad { get; set; }
       
        public string total { get; set; }
    }
}
